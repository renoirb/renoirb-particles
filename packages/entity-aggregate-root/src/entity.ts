/**
 * Entity.
 *
 * In JavaScript, data transfer objects ("Entities")
 * are just "bags of data", they don't have methods.
 *
 * In order to help categorize what collection
 * a particular member of a collection is member of
 * we typically add an identifier of some sort.
 */
export interface EntityInterface {
  /**
   * Entity type.
   *
   * Depending of the conventions in your project,
   * some use the key: ObjectClass,
   * or className or ClassName, or class, or type.
   */
  readonly ENTITY: string
}
