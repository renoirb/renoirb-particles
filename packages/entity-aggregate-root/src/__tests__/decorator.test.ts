import { Entity } from '../decorator'

@Entity
export class Client {
  constructor(readonly name: string) {}
}

describe('decorator', () => {
  test('Sanity check', () => {
    const subject = new Client('ACME')
    expect(subject).toMatchSnapshot()
    expect(subject).toHaveProperty('ENTITY', 'Client')
  })
})
