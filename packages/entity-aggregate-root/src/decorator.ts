import { EntityInterface } from './entity'

/**
 * Entity decorator.
 */
export const Entity = <T extends { new (...args: any[]): {} }>(
  constructor: T,
) => {
  return class extends constructor implements EntityInterface {
    readonly ENTITY = constructor.name
  }
}
