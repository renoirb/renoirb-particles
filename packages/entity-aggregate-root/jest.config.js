/* eslint-disable @typescript-eslint/no-var-requires */
const base = require('@renoirb/conventions-use-jest')

module.exports = {
  ...base,
  prettierPath:
    '../../common/temp/node_modules/@renoirb/conventions-use-prettier/bin/prettier',
}
