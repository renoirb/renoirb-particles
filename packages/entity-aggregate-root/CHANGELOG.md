# Change Log - @renoirb/entity-aggregate-root

This log was last generated on Tue, 24 Nov 2020 09:11:31 GMT and should not be manually modified.

## 0.3.2
Tue, 24 Nov 2020 09:11:31 GMT

### Patches

- Node.js exports must start by dot slash

## 0.3.1
Mon, 23 Nov 2020 07:47:01 GMT

### Patches

- Upgrade @types/node

## 0.3.0
Wed, 15 Jul 2020 02:59:19 GMT

### Minor changes

- Packaging and bundling normalization and deps update

## 0.2.3
Thu, 04 Jun 2020 22:34:20 GMT

### Patches

- Attempt at publishing to see if import works

## 0.2.2
Thu, 04 Jun 2020 13:55:57 GMT

### Patches

- Un needed peerDependency

## 0.2.1
Thu, 04 Jun 2020 00:51:48 GMT

### Patches

- Dependencies update

## 0.2.0
Wed, 03 Jun 2020 23:37:39 GMT

### Minor changes

- New package

