[repo-url]: https://gitlab.com/renoirb/renoirb-particles/-/tree/v1.x/packages/data-source
[npmjs-badge]: https://img.shields.io/npm/v/%40renoirb%2Fdata-source?style=flat-square&logo=appveyor&label=npm&logo=npm 'NPMjs.com Package Badge'
[npmjs-url]: https://www.npmjs.com/package/%40renoirb%2Fdata-source
[size-badge]: https://img.shields.io/bundlephobia/min/%40renoirb%2Fdata-source?style=flat-square
[deps-status]: https://img.shields.io/librariesio/release/npm/%40renoirb%2Fdata-source?style=flat-square&logo=appveyor&logo=dependabot

# [**@renoirb/data-source**][repo-url]

Use JSON or YAML as way of configuring HTTP calls, get an HTTP RequestConfig manager with preconfigured parameters for your Dependency Injection Container stack.

| Version                          | Size                           | Dependencies                                                      |
| -------------------------------- | ------------------------------ | ----------------------------------------------------------------- |
| [![npm][npmjs-badge]][npmjs-url] | ![npm bundle size][size-badge] | ![Libraries.io dependency status for latest release][deps-status] |
