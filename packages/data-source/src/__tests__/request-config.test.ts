import { AxiosInstance, default as axios } from 'axios'
import { URLSearchParams } from 'url'

import {
  Endpoint,
  DataSource,
  DataSourceEndpointRequestHelper,
  MutableRequestConfig,
} from '..'

// @ts-ignore
import MockAdapter from 'axios-mock-adapter'

const originConfig = Object.freeze({
  hostname: 'secret.corp.example.org',
  port: 8080,
})

const originConfigBaseURL = `http://${originConfig.hostname}:${originConfig.port}`

/**
 * For example, we might want to use a Koa route as an alias
 * for an Endpoint
 */
const endpointPrivateServicePath = '/backend/FooNamespace/Foo'
const publicMethod = 'GET'
const publicPath = '/user/:id'
const endpointAlias = [publicMethod, publicPath].join(' ')

describe('DataSourceEndpointRequestHelper', () => {
  let dataSource: DataSource
  let serviceInstance: AxiosInstance
  let mock: MockAdapter

  const createHelper = (
    endpointAlias: string,
  ): DataSourceEndpointRequestHelper =>
    new DataSourceEndpointRequestHelper(dataSource, endpointAlias)

  beforeEach(() => {
    serviceInstance = axios.create()
    mock = new MockAdapter(serviceInstance)
    dataSource = new DataSource(originConfig)
    dataSource.addHeader('X-Backend-Id', 'foo-bar')

    const endpoint = new Endpoint('GET', endpointPrivateServicePath)
    endpoint.addHeader('From', 'jdoe@example.org')
    endpoint.addHeader('content-type', 'application/json')
    dataSource.set(endpointAlias, endpoint)
  })

  test('Happy-path', () => {
    expect(dataSource.endpoints).toHaveProperty(endpointAlias, {
      headers: {
        'content-type': 'application/json',
        From: 'jdoe@example.org',
      },
      method: 'GET',
      path: '/backend/FooNamespace/Foo' /* endpointPrivateServicePath */,
    })
    const helper = createHelper(endpointAlias)
    expect(helper).toMatchSnapshot()
    expect(helper).toBeInstanceOf(DataSourceEndpointRequestHelper)
    expect(helper.endpoint).toBeInstanceOf(Endpoint)
    expect(helper).toHaveProperty('method', 'GET')
    expect(helper).toHaveProperty(
      'url',
      originConfigBaseURL +
        '/backend/FooNamespace/Foo' /* endpointPrivateServicePath */,
    )
    expect(helper).toHaveProperty(
      'path',
      '/backend/FooNamespace/Foo' /* endpointPrivateServicePath */,
    )
    expect(helper).toHaveProperty('baseURL', originConfigBaseURL)
    expect(helper).toHaveProperty('hasURITemplateData', false)
    expect(helper).toHaveProperty('hasURITemplateParts', false)

    const requestConfig = helper.prepare()
    expect(requestConfig).toBeInstanceOf(MutableRequestConfig)
    expect(requestConfig).toHaveProperty(
      'decodedURL',
      originConfigBaseURL +
        '/backend/FooNamespace/Foo' /* endpointPrivateServicePath */,
    )
    expect(requestConfig).toMatchSnapshot()
  })

  test('By value sanity check', () => {
    const endpoint = dataSource.get(endpointAlias)
    // We are making sure that the request config only takes input
    // from endpoint, that we aren't manipulating afterwards
    // If we need a different path, add another endpoint to that origin.
    endpoint.path = '/edited/private/foo/bar'
    expect(endpoint).toHaveProperty('path', '/edited/private/foo/bar')
    const helper = createHelper(endpointAlias)
    // Because dataSource.get returns a copy, our helper hasn't been tampered.
    expect(helper).toHaveProperty(
      'path',
      '/backend/FooNamespace/Foo' /* endpointPrivateServicePath */,
    )
  })

  test('addURLSearchParam', () => {
    const endpoint = new Endpoint('GET', '/backend/i18n/v2/messages{?locale}')
    dataSource.set('locale', endpoint)

    const assertions = [
      [
        { TenantName: 'ACMECORP' },
        [['page', 10]],
        '/backend/i18n/v2/messages?page=10',
      ],
      [{}, [['locale', 'en-CA']], '/backend/i18n/v2/messages?locale=en-CA'],
      [
        {},
        [
          ['locale', 'en-CA'],
          ['page', '5'],
        ],
        '/backend/i18n/v2/messages?locale=en-CA&page=5',
      ],
      [
        {},
        [
          ['locale', 'en-CA'],
          ['page', '5'],
          ['output', 'array'],
        ],
        '/backend/i18n/v2/messages?locale=en-CA&output=array&page=5',
      ],
    ]
    // @ts-ignore
    for (const [params, urlSearchParams, expectedSubjectUrl] of assertions) {
      const helper = createHelper('locale')
      helper.setURITemplateData(params)
      const requestConfig = helper.prepare()
      expect(requestConfig).toMatchSnapshot()
      // @ts-ignore
      for (const [k, v] of urlSearchParams) {
        requestConfig.addURLSearchParam(k, v)
      }
      const expectedUrl = requestConfig.baseURL + expectedSubjectUrl
      expect(requestConfig.url).toBe(expectedUrl)
    }
  })

  test('setURITemplateData fills URL template parameters', () => {
    const endpoint = new Endpoint(
      'GET',
      // URITemplate support optioal URL paths by doing {/foo*}, which is useful
      // when we have endpoints that can be exactly the same, except to optionally
      // add them in the URL.
      '/backend/AlphaNamespace/{TenantName}{/BarID*}{?fields,q,limit,page}',
    )
    endpoint.addHeader('TenantName', 'BAZZ')
    dataSource.set('AlphaNamespace', endpoint)

    // Typical URL use-cases, the reason this is setting in place use of URITemplate.
    const assertions = [
      // In the fixtures above, we already have a header TenantName "BAZZ", but we can change it
      [{ TenantName: 'ACMECORP' }, '/backend/AlphaNamespace/ACMECORP'],
      // We can optionally set an BarID in the URL, (Not as URL Query parameter!)
      [{ BarID: '111-222-333' }, '/backend/AlphaNamespace/BAZZ/111-222-333'],
      // Notice the ID isn't present, but only q URL Query parameter is present, nothing else
      // Notice that $ is escaped as %24
      [
        { q: 'Status_$eq_$Alive' },
        '/backend/AlphaNamespace/BAZZ?q=Status_%24eq_%24Alive',
      ],
      [
        { q: 'Status_$eq_$Alive', limit: 100 },
        '/backend/AlphaNamespace/BAZZ?limit=100&q=Status_%24eq_%24Alive',
      ],
      [
        { q: 'Status_$eq_$Alive', limit: 100, page: 10 },
        '/backend/AlphaNamespace/BAZZ?limit=100&page=10&q=Status_%24eq_%24Alive',
      ],
    ]

    // @ts-ignore
    for (const [params, expectedSubjectUrl] of assertions) {
      const helper = createHelper('AlphaNamespace')
      helper.setURITemplateData(params)
      const requestConfig = helper.prepare()
      expect(requestConfig).toMatchSnapshot()
      const expectedUrl = requestConfig.baseURL + expectedSubjectUrl
      expect(requestConfig.url).toBe(expectedUrl)
      expect(String(requestConfig)).toBe(
        `GET ${decodeURIComponent(expectedUrl)}`,
      )
      expect(requestConfig.decodedURL).toBe(decodeURIComponent(expectedUrl))
    }

    /**
     * What about our endpoint has no URITemplate mention of an URL Query parameter
     */
    const helper = createHelper('AlphaNamespace')
    // Our url property do have hasURITemplateParts, but we've filled it.
    helper.setURITemplateData({})
    expect(helper).toHaveProperty('hasURITemplateData', true)
    expect(helper).toHaveProperty('hasURITemplateParts', true)
    const requestConfig = helper.prepare()
    requestConfig.addURLSearchParam('s', 'windows')
    requestConfig.addURLSearchParam('s', 'linux')
    expect(String(requestConfig)).toBe(
      `GET http://secret.corp.example.org:8080/backend/AlphaNamespace/BAZZ?s=linux`,
    )
    const searchParams: Record<string, string> = { s: 'linux' }
    expect(requestConfig).toHaveProperty(
      'params',
      new URLSearchParams(
        searchParams as any /* problem with TypeScript lib.dom.d.ts? */,
      ),
    )
    expect(String(requestConfig.params)).toBe('s=linux')
    expect(requestConfig).toMatchSnapshot()
  })

  /**
   * To avoid issuing a request with missing parameters in the URL
   * We need a way to make sure we haven't forgotten them.
   */
  test('Handling with URITemplate when URL has them', () => {
    /**
     * When no URITemplate
     * Let's not fail.
     */
    dataSource.set(
      'with-no-uri-template',
      new Endpoint('POST', '/foo', { 'Content-Type': 'application/json' }),
    )
    expect(() =>
      createHelper('with-no-uri-template').getExpandedPath(),
    ).not.toThrow()

    /**
     * When WE do HAVE an URITemplate
     * Let's make sure we enforce it
     */
    const endpoint = new Endpoint(
      'GET',
      // Notice the order: fields,q,limit
      '/backend/BazNamespace/{TenantName}{?fields,q,limit}',
    )
    dataSource.set('BazNamespace', endpoint)
    dataSource.addHeader('TenantName', 'BAZZ')

    /**
     * We should not let prepare work if we have URITemplate
     * and no values for them.
     * Regardless whether or not use use URLTemplate for adding to the URL Query
     */
    expect(() => createHelper('BazNamespace').getExpandedPath()).toThrow()
    expect(() => {
      // prepare throws, because getExpandedPath failed.
      createHelper('BazNamespace').prepare()
    }).toThrow()

    /**
     * We should have a way to check if we have what we need.
     * i.e. what make sure the tests above throws an Exception.
     */
    const subject1 = new DataSourceEndpointRequestHelper(
      dataSource,
      'BazNamespace',
    )
    expect(subject1).toHaveProperty('hasURITemplateData', false)
    expect(subject1).toHaveProperty('hasURITemplateParts', true)
    expect(subject1).toHaveProperty('uriTemplateData', {})

    const localeFixtureAlpha = {
      TenantName: 'BAZZ',
      fields: 'foo,bar',
      limit: 10,
      q: 'Foo_$eq_$Bar|Bazz',
    }

    const subject2 = new DataSourceEndpointRequestHelper(
      dataSource,
      'BazNamespace',
    )
    subject2.setURITemplateData(localeFixtureAlpha)
    expect(subject2).toHaveProperty('hasURITemplateData', true)
    expect(subject2).toHaveProperty('hasURITemplateParts', true)
    expect(subject2).toHaveProperty('uriTemplateData', {
      'X-Backend-Id': 'foo-bar',
      ...localeFixtureAlpha,
    })
    const subject2Config = subject2.prepare()
    expect(subject2Config).toMatchSnapshot()
    subject2Config.addURLSearchParam('page', 20)
    subject2Config.addURLSearchParam('q', 'Foo_$eq_$Bar')
    expect(subject2Config.params).toMatchSnapshot()
    expect(subject2Config).toHaveProperty(
      'decodedURL',
      'http://secret.corp.example.org:8080/backend/BazNamespace/BAZZ?fields=foo,bar&limit=10&page=20&q=Foo_$eq_$Bar',
    )
    expect(subject2Config).toHaveProperty(
      'url',
      // URL property is URLEncoded, useful for HTTP client, if you want full string for logging or debugging, see above
      'http://secret.corp.example.org:8080/backend/BazNamespace/BAZZ?fields=foo%2Cbar&limit=10&page=20&q=Foo_%24eq_%24Bar',
    )
    expect(String(subject2Config)).toBe(
      'GET http://secret.corp.example.org:8080/backend/BazNamespace/BAZZ?fields=foo,bar&limit=10&page=20&q=Foo_$eq_$Bar',
    )
    const subject2SearchParams: Record<string, string> = {
      fields: 'foo,bar',
      limit: '10',
      page: '20',
      q: 'Foo_$eq_$Bar',
    }
    expect(subject2Config).toHaveProperty(
      'params',
      new URLSearchParams(
        subject2SearchParams as any /* problem with TypeScript lib.dom.d.ts? */,
      ),
    )
    const subject3 = new DataSourceEndpointRequestHelper(
      dataSource,
      'BazNamespace',
    )
    // We're testing that if we set from URLSearchParams argument, we have a value set.
    subject3.setURITemplateData({ limit: subject2SearchParams.limit })
    expect(subject3).toMatchSnapshot()
    const subject3Config = subject3.prepare()
    expect(subject3Config).toMatchSnapshot()
    expect(String(subject3Config)).toBe(
      // OK, we got a value set.
      'GET http://secret.corp.example.org:8080/backend/BazNamespace/BAZZ?limit=10',
    )
    // Let's change that value
    subject3Config.addURLSearchParam('limit', 40)
    expect(subject3Config.params).toMatchSnapshot()
    expect(String(subject3Config)).toBe(
      // Cool. As expected.
      // So we can either add parameters and/or change them and the state is reflected.
      'GET http://secret.corp.example.org:8080/backend/BazNamespace/BAZZ?limit=40',
    )
    // How about adding more unexpected, that wasn't in URITemplate
    // ... say, for pagination.
    subject3Config.addURLSearchParam('page', 2)
    expect(String(subject3Config)).toBe(
      'GET http://secret.corp.example.org:8080/backend/BazNamespace/BAZZ?limit=40&page=2',
    )
  })

  test('Integration', async () => {
    // Let's create another endpoint for this test.
    // It'll be a UI endpoint.
    const endpointPrivateServicePathForUi = '/backend/FooNamespace/Foo/ui'
    const endpoint = new Endpoint('POST', endpointPrivateServicePathForUi, {
      'Content-Type': 'application/json',
    })
    endpoint.addHeader('Authorization', 'Bearer 111.222.333')
    expect(endpoint).toMatchSnapshot()

    dataSource.set('ui--request', endpoint)
    const helper = createHelper('ui--request')
    const requestConfig = helper.prepare()
    expect(requestConfig).toMatchSnapshot()
    expect(requestConfig).toBeInstanceOf(MutableRequestConfig)

    requestConfig.addHeader('From', 'jdoe@example.org')
    expect(requestConfig).toMatchSnapshot()

    expect(requestConfig).toHaveProperty(
      'path',
      endpointPrivateServicePathForUi,
    )
    expect(requestConfig).toHaveProperty(
      'url',
      originConfigBaseURL + endpointPrivateServicePathForUi,
    )

    // @todo: simplify this test, its getting long.
    // More mocking tests here https://github.com/ctimmerm/axios-mock-adapter/blob/master/test/basics.spec.js
    mock.onPost(endpointPrivateServicePathForUi).reply(200, {
      data: {
        users: [
          {
            username: 'jdoe',
            email: 'jdoe@example.org',
          },
        ],
      },
    })

    /**
     * All this setup to see a full request-response cycle.
     * AND IS using requestConfig helper.
     */
    const { data, status } = await serviceInstance.request(requestConfig)
    expect(data).toMatchSnapshot()
    expect(data).toHaveProperty('data.users.0.username', 'jdoe')
    expect(data).toHaveProperty('data.users.0.email', 'jdoe@example.org')
    expect(status).toBe(200)
    expect(JSON.parse(JSON.stringify(mock.history.post[0]))).toMatchSnapshot()
    expect(mock.history.post[0]).toHaveProperty('headers', {
      Accept: 'application/json, text/plain, */*',
      'X-Backend-Id': 'foo-bar',
      Authorization: 'Bearer 111.222.333',
      'Content-Type': 'application/json',
      From: 'jdoe@example.org',
    })
    expect(mock.history.post[0]).toHaveProperty(
      'url',
      originConfigBaseURL + endpointPrivateServicePathForUi,
    )
  })

  test('Exception handling', () => {
    expect(
      () =>
        // What happens with new DataSourceEndpointRequestHelper when it doesn'find the endpointAlias?
        new DataSourceEndpointRequestHelper(dataSource, 'Use-The-Force-Harry'),
    ).toThrow()
  })
})
