import {
  slugifyPath,
  stringifyParams,
  isValidLookingHttpHeaderPair,
  hasAlreadyHttpHeader,
  HttpHeaders,
  appropriateHttpHeaderPicker,
} from '..'

describe('slugifyPath', () => {
  test('constructor', () => {
    const assertions = [
      ['/foo/bar/baz', 'foo-bar-baz'],
      ['/foo/bar/', 'foo-bar'],
      ['/Foo/BaAr//', 'foo-baar'],
    ]
    // @ts-ignore
    for (const [target, expected] of assertions) {
      const run = slugifyPath(target)
      expect(run).toBe(expected)
    }
  })
})

describe('stringifyParams', () => {
  test('escaping', () => {
    const assertions = [
      [
        { user: 'Alice', PassWord: 'mypass' },
        'user=Alice&PassWord=mypass',
        'user=Alice&PassWord=mypass',
      ],
      [
        { Token: '111.222.333', userinfo: 'foo,bar,bazz' },
        'Token=111.222.333&userinfo=foo%2Cbar%2Cbazz',
        'Token=111.222.333&userinfo=foo,bar,bazz',
      ],
      [
        { q: 'Status_$eq_$Active|Alive' },
        'q=Status_%24eq_%24Active%7CAlive',
        'q=Status_$eq_$Active|Alive',
      ],
    ]
    // @ts-ignore
    for (const [target, expected, unescaped] of assertions) {
      let run = stringifyParams(target)
      expect(run).toBe(expected)
      run = stringifyParams(target, false)
      expect(run).toBe(unescaped)
    }
  })
})

/**
 * Alternate testing formatting using Tagged Templates
 * https://jestjs.io/docs/en/api#2--describeeachtablename-fn-timeout-
 * https://jestjs.io/docs/en/api#2--testeachtablename-fn-timeout-
 */
describe('isValidLookingHttpHeaderPair', () => {
  test.each`
    a                    | b                     | expected
    ${'Accept'}          | ${'application/json'} | ${true}
    ${'X-Forwarded-For'} | ${'10.10.10.1'}       | ${true}
  `(
    'returns $expected when we use $a as header name, and $b as header value',
    ({ a, b, expected }) => {
      expect(isValidLookingHttpHeaderPair(a, b)).toBe(expected)
    },
  )
})

describe('hasAlreadyHttpHeader', () => {
  const headers: HttpHeaders = {
    Accept: 'text/yaml',
    'Content-Type': 'application/vnd.api+json',
    Authorization: 'Bearer 111.222.333',
    NonStandardHeader: 'aaaaaaa-b-ccccccc',
  }
  test.each`
    input                | expected | because
    ${'Accept'}          | ${true}  | ${'Is not CaSe sensitive'}
    ${'X-Forwarded-For'} | ${false} | ${'Can tell if not found'}
    ${'conteNt-tYpE'}    | ${true}  | ${'Is not CAsE sensitive'}
    ${'accept'}          | ${true}  | ${'Is not CAsE sensitive'}
  `('returns $expected because $because', ({ input, expected }) => {
    expect(hasAlreadyHttpHeader(headers, input)).toBe(expected)
  })
})

describe('appropriateHttpHeaderPicker', () => {
  const headers: HttpHeaders = {
    Accept: 'text/yaml',
    'Content-Type': 'application/vnd.api+json',
    Authorization: 'Bearer 111.222.333',
    NonStandardHeader: 'aaaaaaa-b-ccccccc',
  }
  const because1 =
    'when the key is already present, in the same casing, we return the same as the input'
  const because2 = 'when the key is absent, we return the same as the input'
  const because3 = 'the key already existed with different StRiNg CaSiNg'
  test.each`
    input                | expected             | because
    ${'Accept'}          | ${'Accept'}          | ${because1}
    ${'X-Forwarded-For'} | ${'X-Forwarded-For'} | ${because2}
    ${'conteNt-tYpE'}    | ${'Content-Type'}    | ${because3}
    ${'accept'}          | ${'Accept'}          | ${because3}
  `(
    'Using $input returned $expected because $because',
    ({ input, expected }) => {
      expect(appropriateHttpHeaderPicker(headers, input)).toBe(expected)
    },
  )
})
