import { Endpoint } from '..'

describe('endpoint', () => {
  test('constructor', () => {
    const ep = new Endpoint('GET', '/foo/bar')
    expect(ep).toBeInstanceOf(Endpoint)
    expect(ep).toHaveProperty('method', 'GET')
    expect(ep).toHaveProperty('path', '/foo/bar')
    ep.path = '/buzz/bizz'
    expect(ep).toHaveProperty('path', '/buzz/bizz')
  })

  test('POST must declare Content-Type', () => {
    expect(() => {
      // eslint-disable-next-line no-new
      new Endpoint('POST', '/backend/IdentityNamespace/v3/token')
    }).toThrow(
      'Missing required Content-Type HTTP Request header, POST request types must have it declared explicitly',
    )
  })
})
