import { factory, DataSource, DataSourceEndpointRequestHelper } from '..'

/**
 * Here is how we can validate our own JSONSchema for our configuration system.
 */

const originJsonString =
  '{"hostname":"secret.corp.example.org","port": 8080,"endpoints":{"hello-world":{"path":"/backend/AlphaNamespace/{TenantName}{?fields,q,limit}","timeout":11}}}'

describe('factory', () => {
  let dataSource: DataSource

  test('Happy-path', () => {
    dataSource = factory(originJsonString)
    // At factory time, we would set a few headers.
    // That would be done at Koa WebAPI startup.
    dataSource.addHeader('TenantName', 'ACMECORP')
    expect(dataSource).toMatchSnapshot()
    // Assuming we're
    const helper = new DataSourceEndpointRequestHelper(
      dataSource,
      'hello-world',
    )
    const searchParams: Record<string, string> = {
      // Remember that TenantName is merged here for setURITemplateData
      fields: 'foo,bar',
      q: 'Foo_$eq_$Bar',
    }
    helper.setURITemplateData(searchParams)
    const requestConfig = helper.toRequestConfig()
    expect(requestConfig).toMatchSnapshot()
    const expectedUrl =
      '/backend/AlphaNamespace/ACMECORP?fields=foo,bar&q=Foo_$eq_$Bar'
    expect(String(requestConfig)).toBe(
      `GET ${requestConfig.baseURL + expectedUrl}`,
    )
    expect(requestConfig.params).toMatchSnapshot()
  })

  test('Throws with meaningful errors', () => {
    expect(() => {
      const originJsonString =
        '{"hostname":"secret.corp.example.org","port": 8080}'
      dataSource = factory(originJsonString)
      /**
       * JSON validation failed:
       *   Error: #/endpoints
       *     Missing required property: endpoints
       */
    }).toThrow('Missing required property: endpoints')

    expect(() => {
      const originJsonString =
        '{"hostname":"secret.corp.example.org","endpoints":[]}'
      dataSource = factory(originJsonString)
      /**
       * JSON validation failed:
       *   Error: #/endpoints
       *     Expected type object but found type array
       */
    }).toThrow('Expected type object but found type array')
  })
})
