import { httpHeadersNormalizer, HttpHeadersMapContainer, HttpHeaders } from '..'

describe('httpHeadersNormalizer', () => {
  const createFixture = (): HttpHeadersMapContainer => {
    const headers: HttpHeaders = {
      accept: 'text/yaml',
      'conTent-Type': 'application/vnd.api+json',
      'Non-standard-hEader': 'aaaaaaa-b-ccccccc',
      UserToken: '111.222.333-444',
      // Content-MD5 is CaseSenSiTiVe
      'conteNt-md5': 'Q2hlY2sgSW50ZWdyaXR5IQ==',
    }

    const out: HttpHeadersMapContainer = { headers }

    return out
  }

  test('Normalized', () => {
    const normalizer = httpHeadersNormalizer()
    const fixture = createFixture()
    const subject = normalizer(fixture)
    expect(subject).toMatchSnapshot()
    expect(subject).toHaveProperty('Accept')
    expect(subject).toHaveProperty('Content-Type')
    expect(subject).toHaveProperty('Content-MD5')
  })

  test('When empty', () => {
    // #TODO STUB
    const normalizer = httpHeadersNormalizer()
    const subject = normalizer({})
    expect(subject).toBe(null)
  })
})
