import { UrlTemplateHelper } from '..'

const templates1 = '/backend-foo/v2/{TenantName}{/BazID*}{?fields,q,limit}'

const prependLocalhost = (template: string): string =>
  `http://localhost${template}`

type LocalTestsAssertions = [
  string,
  Record<string, string>,
  Record<string, string | number | boolean>,
  string,
][]

describe('UrlTemplateHelper', () => {
  test('Happy-Paths', () => {
    const assertions: LocalTestsAssertions = [
      [
        templates1,
        { TenantName: 'ACMECORP' },
        { page: 10 },
        '/backend-foo/v2/ACMECORP?page=10',
      ],
      [
        templates1,
        { TenantName: 'ACMECORP', fields: 'BazName,buzzId' },
        { limit: 100 },
        '/backend-foo/v2/ACMECORP?fields=BazName%2CbuzzId&limit=100',
      ],
      [templates1, { TenantName: 'ACMECORP' }, {}, '/backend-foo/v2/ACMECORP'],
      [
        templates1,
        { TenantName: 'ACMECORP', BazID: '11111-2222-33-4' },
        { output: 'array' },
        '/backend-foo/v2/ACMECORP/11111-2222-33-4?output=array',
      ],
    ]
    for (const [
      template,
      uriTemplateData,
      urlSearchParams,
      expected,
    ] of assertions) {
      const url = prependLocalhost(template)
      const helper = new UrlTemplateHelper(url)
      // @ts-ignore
      const subject = helper.expand(uriTemplateData, urlSearchParams)
      expect(String(subject)).toBe(prependLocalhost(expected))
    }
  })
})
