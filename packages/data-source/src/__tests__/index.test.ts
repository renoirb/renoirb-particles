import * as exportables from '..'

describe('exportables', () => {
  test('endpoint', () => {
    expect(exportables).toHaveProperty('Endpoint')
  })
  test('DataSource', () => {
    expect(exportables).toHaveProperty('DataSource')
  })
  test('SchemaValidator', () => {
    const subject = new exportables.SchemaValidator()
    expect(subject.toJSON()).toMatchSnapshot()
  })
})
