import {
  Endpoint,
  DataSource,
  hasOptionsAndIsLoopback,
  mightHaveOptionsHostnameAsLocalhost,
  mustBeLoopbackIfLocalhost,
  DataSourceConstructorOptions,
  factory,
  DataSourceEndpointRequestHelper,
} from '..'

const originConfig = Object.freeze({
  hostname: 'secret.corp.example.org',
  port: 8080,
})

const endpointPrivateServicePath = '/backend/UserNamespace/v2/profile'

const reInitializeDataSourceFixture = (opts: any): DataSource => {
  const origin = new DataSource({ ...opts })
  const endpoint = new Endpoint('POST', endpointPrivateServicePath, {
    // Make obvious what is the content-type for POST requests
    'Content-Type': 'application/json',
  })
  origin.set('ui--detail-user', endpoint)

  return origin
}

describe('validators', () => {
  test('hasOptionsAndIsLoopback', () => {
    expect(hasOptionsAndIsLoopback({ ip: '10.0.0.1' })).toBe(false)
    expect(hasOptionsAndIsLoopback({ ip: '127.0.0.1' })).toBe(true)
    expect(hasOptionsAndIsLoopback({ ip: '127.0.1.1' })).toBe(true)
    expect(hasOptionsAndIsLoopback({ ip: '127.0.14.1' })).toBe(true)
  })
  test('mightHaveOptionsHostnameAsLocalhost', () => {
    expect(mightHaveOptionsHostnameAsLocalhost({ bogus: true } as any)).toBe(
      true,
    )
    expect(mightHaveOptionsHostnameAsLocalhost({ hostname: 'LOCALHOST' })).toBe(
      true,
    )
  })
  test('mustBeLoopbackIfLocalhost', () => {
    expect(() => mustBeLoopbackIfLocalhost({ ip: 'LOCALHOST' })).toThrow()
    expect(() => mustBeLoopbackIfLocalhost({ ip: '10.0.0.1' })).toThrow()
    expect(() =>
      mustBeLoopbackIfLocalhost({ ip: '10.0.0.1', hostname: 'LocalHost' }),
    ).toThrow()
    expect(mustBeLoopbackIfLocalhost({} as any))
  })
})

describe('origin options', () => {
  let origin: DataSource

  test('Fully qualified Happy-Path', () => {
    origin = new DataSource({
      ip: '10.10.1.4',
      hostname: 'internal.project.corp.example.org',
      port: 2020,
    })
    expect(origin).toHaveProperty('ip', '10.10.1.4')
    expect(origin).toHaveProperty(
      'hostname',
      'internal.project.corp.example.org',
    )
    expect(origin).toHaveProperty('port', 2020)
    expect(origin).toHaveProperty(
      'host',
      'internal.project.corp.example.org:2020',
    )
    expect(origin).toHaveProperty(
      'baseURL',
      'http://internal.project.corp.example.org:2020',
    )
    expect(origin).toHaveProperty('headers', {})
    expect(origin).toMatchSnapshot()
    origin.toggleUseIpAndAddHostHeader()
    expect(origin).toHaveProperty(
      'host',
      'internal.project.corp.example.org:2020',
    )
    expect(origin).toHaveProperty('baseURL', 'http://10.10.1.4:2020')
    expect(origin).toMatchSnapshot()
    expect(origin).toHaveProperty('headers', {
      Host: 'internal.project.corp.example.org:2020',
    })
  })

  test('toggleUseIpAndAddHostHeader', () => {
    origin = new DataSource({
      hostname: 'internal.project.corp.example.org',
      port: 2020,
    })
    expect(() => origin.toggleUseIpAndAddHostHeader()).toThrow()
  })

  test('No hostname, is localhost', () => {
    origin = new DataSource({ ip: '127.0.1.1' })
    expect(origin).toHaveProperty('ip', '127.0.1.1')
    expect(origin).toHaveProperty('hostname', 'localhost')
    expect(origin).toHaveProperty('port', 80)
    expect(origin).toHaveProperty('host', 'localhost')
    expect(origin).toHaveProperty('baseURL', 'http://localhost')
    expect(origin).toMatchSnapshot()
  })

  test('No hostname, IP is not loopback, should fail', () => {
    expect(() => new DataSource({ ip: '10.0.1.1' })).toThrow()
    expect(() => new DataSource({ ip: '127.0.1.1' })).not.toThrow()
    expect(() => new DataSource({} as any)).not.toThrow()
  })

  test('No options at all, defaults to localhost', () => {
    expect(() => new DataSource({} as any)).not.toThrow()
    // Voluntarily missing arguments for runtime forgivitiveness
    expect(() => new DataSource({} as any)).not.toThrow()
    origin = new DataSource()
    expect(origin).toMatchSnapshot()
    expect(origin).toHaveProperty('hostname', 'localhost')
    expect(origin).not.toHaveProperty('ip')
  })
})

describe('DataSource', () => {
  let dataSource: DataSource

  beforeEach(() => {
    // Bootstrap data sources, add config.
    dataSource = reInitializeDataSourceFixture(originConfig)
  })

  test('constructor', () => {
    expect(dataSource).toBeInstanceOf(DataSource)
    expect(dataSource).toMatchSnapshot()
    expect(dataSource).toHaveProperty('endpoints.ui--detail-user', {
      headers: { 'Content-Type': 'application/json' },
      method: 'POST',
      path: endpointPrivateServicePath,
    })
    expect(dataSource).toHaveProperty('hostname', originConfig.hostname)
    expect(dataSource).toHaveProperty('port', originConfig.port)
    expect(dataSource.toJSON()).toHaveProperty(
      'host',
      `${originConfig.hostname}:${originConfig.port}`,
    )
    dataSource.port = 9000
    expect(dataSource).toHaveProperty('port', 9000)
    expect(dataSource.toJSON()).toHaveProperty(
      'host',
      `${originConfig.hostname}:9000`,
    )
  })

  test('constructor with headers', () => {
    const headers = { authorization: 'Bearer 111.222.333' }
    const testGroupOriginConfig = {
      ...originConfig,
      headers,
    }
    dataSource = reInitializeDataSourceFixture(testGroupOriginConfig)
    expect(dataSource).toHaveProperty('hostname', originConfig.hostname)
    expect(dataSource).toHaveProperty('headers', headers)
    expect(dataSource).toMatchSnapshot()
  })

  test('addHeader', () => {
    dataSource.addHeader('From', 'root@localhost')
    expect(dataSource).toMatchSnapshot()
  })

  test('constructor when no Hostname provided', () => {
    expect(() => {
      reInitializeDataSourceFixture({
        // We are deliberately NOT seting hostname
        ip: '172.22.60.6',
        port: 2020,
      })
    }).toThrow(
      'When no hostname, or hostname localhost, and there is an IP, it MUST be a loopback but with IP staring by 127',
    )
    const testGroupOriginConfig = {
      ...originConfig,
      ip: '127.1.1.1',
    }
    delete testGroupOriginConfig.hostname
    dataSource = reInitializeDataSourceFixture(testGroupOriginConfig)
    expect(dataSource).toHaveProperty('hostname', 'localhost')
    expect(dataSource).toHaveProperty('port', originConfig.port)
    expect(dataSource.toJSON()).toHaveProperty(
      'host',
      `localhost:${originConfig.port}`,
    )
    expect(dataSource).toMatchSnapshot()
    expect(dataSource).toMatchObject({
      endpoints: {
        'ui--detail-user': {
          headers: { 'Content-Type': 'application/json' },
          method: 'POST',
          path: '/backend/UserNamespace/v2/profile',
        },
      },
      headers: {},
      host: 'localhost:8080',
      hostname: 'localhost',
      port: 8080,
      protocol: 'http',
      ip: '127.1.1.1',
    })
  })

  test('toString', () => {
    const stringified = String(dataSource)
    expect(stringified).toBe('http://secret.corp.example.org:8080')
    dataSource.port = 9090
    expect(String(dataSource)).toBe('http://secret.corp.example.org:9090')
    dataSource.protocol = 'https'
    expect(String(dataSource)).toBe('https://secret.corp.example.org:9090')
    // dataSource.port = 443
    // expect(String(dataSource)).toBe('https://secret.corp.example.org')
    // ^ This above can't work, because toString() does not do if-clauses from constructor.
    //   Should not be an issue, we do not plan to mutate Data Source at runtime.
  })

  test('get: Throws an exception', () => {
    expect(() => dataSource.get('Use-The-Force-Harry')).toThrow()
  })

  /**
   * Making sure dataSource.get(alias) RETURNS A **COPY**.
   * And not a reference.
   */
  test('get: By value sanity check; Returns a copy of an Endpoint object.', () => {
    const endpoint = dataSource.get('ui--detail-user')
    expect(endpoint).toBeInstanceOf(Endpoint)
    expect(endpoint).toMatchSnapshot()
    expect(endpoint).toHaveProperty('path', endpointPrivateServicePath)
    const beforeChange = dataSource.get('ui--detail-user')
    expect(beforeChange).toBeInstanceOf(Endpoint)
    expect(beforeChange).toMatchSnapshot()
    expect(beforeChange).toHaveProperty('path', endpointPrivateServicePath)

    // Changing endpoint data, hopefully not affecting
    const changedPropertyValue = '/bar/foo'
    endpoint.path = changedPropertyValue
    expect(endpoint).toMatchSnapshot()
    expect(endpoint).toHaveProperty('path', changedPropertyValue)

    // Sanity checking
    const afterChange = dataSource.get('ui--detail-user')
    expect(beforeChange).toHaveProperty('path', endpointPrivateServicePath)
    expect(afterChange).toHaveProperty('path', endpointPrivateServicePath)
    expect(afterChange).not.toHaveProperty('path', changedPropertyValue)
    expect(endpoint).toHaveProperty('path', changedPropertyValue)
  })

  test('toJSON', () => {
    const exported = JSON.parse(JSON.stringify(dataSource))
    expect(exported).toMatchSnapshot()
  })

  test('set: Happy path', () => {
    const alias = 'user-id'
    const internalMethod = 'POST'
    const internalPath = '/backend/UserNamespace/v432/profile'
    const ep = new Endpoint(internalMethod, internalPath, {
      'Content-Type': 'application/json',
    })
    dataSource.set(alias, ep)

    const endpoint = dataSource.get(alias)
    expect(endpoint).toHaveProperty('method', internalMethod)
    expect(endpoint).toHaveProperty('path', internalPath)
  })

  test('set: But use an URL Path Koa would use', () => {
    const publicMethod = 'GET'
    const publicPath = '/user/:id'
    const alias = [publicMethod, publicPath].join(' ')

    const internalMethod = 'GET'
    const internalPath = '/backend/UserNamespace/v432/profile'
    const endpoint = new Endpoint(internalMethod, internalPath)
    endpoint.headers = {
      ...endpoint.headers,
      'content-type': 'application/json',
    }

    dataSource.set(alias, endpoint)

    expect(alias).toBe('GET /user/:id')
    expect(Object.keys(dataSource.endpoints)).toMatchObject([
      'ui--detail-user',
      'GET /user/:id',
    ])
    expect(dataSource.endpoints).toHaveProperty(alias)
    expect(dataSource).toMatchSnapshot()
  })

  test('Sample service configuration fixture', () => {
    const hostname = 'secret.corp.example.org'
    const port = 8080
    const exampleOriginsData: DataSourceConstructorOptions = {
      endpoints: {
        'collection--accounts': {
          headers: {},
          method: 'GET',
          path: '/backend/IdentityNamespace/v1/{TenantName}/accounts{?limit}',
        },
        'collection--user': {
          headers: {},
          method: 'GET',
          path: '/backend/IdentityNamespace/v3/user',
        },
        'detail--user': {
          headers: {},
          method: 'GET',
          path: '/backend/IdentityNamespace/v3/user/{UUID}',
        },
        'ui--detail-request': {
          headers: {
            'Content-Type': 'application/json',
          },
          method: 'POST',
          path: '/backend/FooNamespace/v2',
        },
      },
    }

    /**
     * As an example, here is how we would programatically generate
     * API proxy configuration for a Koa proxy.
     *
     * (This is just to see if that model works, TBD.)
     *
     * Endpoint Goal Hint:
     * - ui: Is a POST API which returns rows, see QueryBuilder -- Ideally we should have GraphQL server for data.
     * - detail: Returns only one entity
     * - collection: Returns an array of entities
     * - create: Creating something, e.g. to get a session UserToken
     *
     * We'll use this and see if that'll be good convention enough.
     */
    dataSource.endpoints = {}
    dataSource.set(
      'example-urlencoded-post',
      new Endpoint('POST', '/backend/FooNamespace/example', {
        'Content-Type': 'application/json',
      }),
    )
    dataSource.set(
      'ui--detail-request',
      new Endpoint('POST', '/backend/FooNamespace/v2', {
        'Content-Type': 'application/json',
      }),
    )
    dataSource.set(
      'collection--user',
      new Endpoint('GET', '/backend/IdentityNamespace/v3/user'),
    )
    dataSource.set(
      'detail--user',
      new Endpoint('GET', '/backend/IdentityNamespace/v3/user/{UUID}'),
    )
    // This make me realize that we might need to configure filtering
    // so we could use the same endpoint, but Expose only ones related to user.
    // Maybe? Not sure. TBD.
    dataSource.set(
      'collection--accounts',
      new Endpoint(
        'GET',
        '/backend/IdentityNamespace/v1/{TenantName}/accounts{?limit}',
      ),
    )
    expect(dataSource).toMatchSnapshot()
    expect(dataSource).toMatchObject({
      ...exampleOriginsData,
      headers: {},
      host: 'secret.corp.example.org:8080',
      hostname,
      port,
      protocol: 'http',
    })
    // Which should be the same below as doing manually above
    const dataSourceUsingCreateDataSource = factory(
      JSON.stringify(exampleOriginsData),
      { port, hostname },
    )
    // Sanity check, if we add header at dataSource.
    dataSourceUsingCreateDataSource.addHeader('TenantName', 'ACMECORP')
    expect(dataSourceUsingCreateDataSource).toMatchSnapshot()
    expect(dataSourceUsingCreateDataSource).toMatchObject({
      ...exampleOriginsData,
      headers: { TenantName: 'ACMECORP' },
      host: 'secret.corp.example.org:8080',
      hostname,
      port,
      protocol: 'http',
    })

    expect(dataSourceUsingCreateDataSource.has('collection--accounts')).toBe(
      true,
    )
    const endpoint = dataSourceUsingCreateDataSource.get('collection--accounts')
    expect(endpoint).toMatchSnapshot()
    const helper = new DataSourceEndpointRequestHelper(
      dataSourceUsingCreateDataSource,
      'collection--accounts',
    )
    expect(helper).toMatchSnapshot()
    expect(() => {
      helper.prepare()
    }).toThrow(
      'URL "/backend/IdentityNamespace/v1/{TenantName}/accounts{?limit}" contains URITemplate',
    )
    helper.setURITemplateData({ limit: 100 })
    const mutable = helper.prepare()
    expect(mutable).toMatchSnapshot()
    const requestConfig = mutable.export()
    expect(requestConfig).toMatchObject({
      baseURL: 'http://secret.corp.example.org:8080',
      headers: {
        TenantName: 'ACMECORP',
      },
      method: 'GET',
      path: '/backend/IdentityNamespace/v1/ACMECORP/accounts?limit=100',
      url:
        'http://secret.corp.example.org:8080/backend/IdentityNamespace/v1/ACMECORP/accounts?limit=100',
    })
  })
})
