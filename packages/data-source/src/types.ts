export type HttpRequestMethod = 'GET' | 'PUT' | 'POST'

/**
 * HTTP Headers
 *
 * This is a limited list of all available list.
 * Refer to [MDN][mdn-http-headers], or the [IANA][iana-http-headers] and also
 * in the [IETF HTTP Working Group's specs][ietf-http]
 *
 * [mdn-http-headers]: https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers
 * [iana-http-headers]: https://www.iana.org/assignments/message-headers/message-headers.xhtml
 * [ietf-http]: https://httpwg.org/specs/
 */
export type PublicHttpHeaderKeys =
  | 'Authorization'
  | 'Accept'
  | 'Content-Type'
  | 'Cookie'
  | 'Set-Cookie'
  | 'Host'
  | 'Referer'
  | 'User-Agent'
  | 'From'

/**
 * Private HTTP Header Keys
 *
 * See also {@link PublicHttpHeaderKeys}
 *
 * X-Request-Id:
 *   For distributed tracing purposes, Koa may pass along an UUID unique to the current request/response cycle.
 *
 * @public
 */
export type PrivateHttpHeaderKeys = 'X-Request-Id' | string

/**
 * HTTP Content-Type/Accept
 *
 * There are many MIME Types, just limiting set of popular ones.
 *
 * https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Type
 * https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types
 */
export type MimeType = 'application/json' | 'application/x-www-form-urlencoded'

/**
 * All possible HTTP Headers keys
 * Including any other strings, if we need in the future.
 *
 * @public
 */
export type HttpHeaderKeys = PublicHttpHeaderKeys | PrivateHttpHeaderKeys

/**
 * This abstraction is used by the mixin pattern.
 * It describes the "static side" of a class.
 *
 * This has been copy-pasted from microsoft/rushstack api-extractor-model
 * https://github.com/microsoft/rushstack/blob/a2a8b044/apps/api-extractor-model/src/mixins/Mixin.ts
 *
 * @public
 */
export type PropertiesOf<T> = { [K in keyof T]: T[K] }

/**
 * Make HttpHeaders be a completely optional list of HTTP headers.
 *
 * @public
 */
export type HttpHeaders = Record<HttpHeaderKeys, MimeType | string> & {}

export class ProxySurface {
  public constructor(
    public readonly method: HttpRequestMethod,
    public readonly path: string,
  ) {}
}

export type EndpointPropertyReadOnly = true
export type EndpointPropertyReadWrite = false
export type EndpointProperty =
  | EndpointPropertyReadOnly
  | EndpointPropertyReadWrite

/**
 * What properties Must be found on an Endpoint
 *
 * Each key represent a property for use either in an URL, or
 * as a Request HTTP Header.
 *
 * If a key is present, it MUST be present to make the Endpoint
 * to be usable.
 *
 * The value is a boolean, is a toggle to
 */
export type EndpointPropertiesMustHaves = Record<
  HttpHeaderKeys,
  EndpointProperty
>

/**
 * What parameters to use for an Endpoint.
 *
 * An Endpoint is a method, a path and optionally some
 * other URL properties such as query parameters.
 *
 * An Endpoint can be setup for different purposes and
 * have predetermined HTTP Request headers.
 *
 * @public
 */
export interface DataSourceEndpointInterface {
  /**
   * HTTP Request headers to use for that data source.
   * See OutgoingHttpHeaders, in Node.js 'http' package.
   *
   * This will be merged on top of the DataSource's headers.
   *
   * ```http
   * Accept: text/yaml
   * X-Foo: BAAR
   * ```
   *
   * Assuming DataSource would have "Host: secret.example.org.local:9999",
   * DataSource#addEndpoint() should merge its headers
   * (including Host in example here) to the Endpoint's
   *
   * Resulting as:
   *
   * ```http
   * Host: secret.example.org.local:9999
   * Accept: text/yaml
   * X-Foo: BAAR
   * ```
   *
   * Should match OutgoingHttpHeaders, in Node.js 'http' package.
   */
  headers?: HttpHeaders

  /**
   * HTTP Verb/Method name
   * e.g. "GET"
   *
   * Valid verbs (incomplete list):
   * - GET
   * - HEAD
   * - POST
   *
   * Bookmarks:
   * - https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods
   */
  readonly method: HttpRequestMethod

  /**
   * The URL's path, Anything after the hostname:port, before ?
   *
   * May contain an [URITemplate][uri-template]
   *
   * [uri-template]: http://www.rfc-editor.org/info/rfc6570
   */
  readonly path: string
}

/**
 * Should implement same properties as ClientRequestArgs from '@types/node' package
 *
 * @public
 */
export interface DataSourceOriginInterface {
  protocol: string

  host: string

  hostname: string

  port: number

  headers: HttpHeaders
}

/**
 * Any data shape.
 */
export type JsonObject = any

export type StringsHashMap = Record<string, string> & {}

export interface RequestConfigLogMessageFormatInterface {
  method: HttpRequestMethod
  url: string
}

/**
 * A Data Source.
 *
 * In other words, a server's IP address, port and names,
 * and a pre-configured list of HTTP calls we can make.
 *
 * @public
 */
export type EndpointsDictionary = Record<
  string,
  DataSourceEndpointInterface
> & {}

/**
 * A Data Source.
 *
 * In other words, a server's IP address, port and names,
 * and a pre-configured list of HTTP calls we can make.
 *
 * @public
 */
export interface DataSourceInterface extends DataSourceOriginInterface {
  endpoints: EndpointsDictionary
  headers: HttpHeaders
  hostname: string
  ip?: string
  port: number
  protocol: string
}

export type EmptyDataSourceConstructorOptions = Partial<DataSourceInterface>
export interface PortOnlyDataSourceConstructorOptions
  extends Partial<DataSourceInterface> {
  ip: string
}
export interface HostnameOnlyDataSourceConstructorOptions
  extends Partial<DataSourceInterface> {
  hostname: string
}
export interface HostnamePortDataSourceConstructorOptions
  extends Partial<DataSourceInterface> {
  hostname: string
  port: number
}

export type DataSourceConstructorOptions =
  | EmptyDataSourceConstructorOptions
  | HostnameOnlyDataSourceConstructorOptions
  | HostnamePortDataSourceConstructorOptions
