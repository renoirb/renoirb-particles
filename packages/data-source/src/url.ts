import { URL } from 'url'

// @ts-ignore
import urlTemplate from 'url-template'

export interface UrlTemplateObject {
  template: string
}

/**
 * What properties to plug into URITemplate processing
 * Must only be strings hash-map
 */
export type UriTemplateDataRecords = Record<string, string>

export type UrlSearchParamsRecords = Record<string, number | string | boolean>

export interface UrlTemplateInterface {
  /**
   * URL template string to use as template to apply URITemplate transformations to using {@link UrlTemplateInterface#expand} method
   */
  readonly template: string
  toObject(): UrlTemplateObject
  expand(
    uriTemplateData: UriTemplateDataRecords,
    searchParams: UrlSearchParamsRecords,
  ): void
}

export const setUrlSearchParamsHelper = (
  url: URL | string,
  data: UrlSearchParamsRecords = {},
): URL => {
  const copy = new URL(String(url))
  for (const [key, value] of Object.entries(data)) {
    const v = String(value)
    if (copy.searchParams.has(key)) {
      copy.searchParams.delete(key)
      copy.searchParams.append(key, v)
    } else {
      copy.searchParams.append(key, v)
    }
  }
  copy.searchParams.sort()

  return copy
}

export class UrlTemplateHelper implements UrlTemplateInterface {
  readonly template: string
  constructor(template: string | UrlTemplateObject = 'http://localhost/') {
    const value: string =
      typeof template === 'string' ? template : template.template
    if (typeof value !== 'string') {
      const message = `Invalid template input "${template}`
      throw new Error(message)
    }
    this.template = value
    Object.defineProperty(this, 'template', {
      enumerable: true,
      value,
      writable: false,
    })
  }

  toObject(): UrlTemplateObject {
    const out: UrlTemplateObject = {
      template: this.template,
    }
    return out
  }

  expand(
    uriTemplateData: UriTemplateDataRecords = {},
    searchParams: UrlSearchParamsRecords = {},
  ): URL {
    const parsed = urlTemplate.parse(this.template)
    const formatted = parsed.expand(uriTemplateData)
    const url = setUrlSearchParamsHelper(formatted, searchParams)
    return url
  }
}
