/**
 * Handle necessary steps for preparing an HTTP Request to another endpoint.
 *
 * Notice the RequestConfigInterface should be compatible to any Node.js HTTP Request User-Agent.
 * Initial implementation was using [Axios][axios], but we should be able to use also other clients such as [GOT][got]
 *
 * [axios]: https://github.com/axios/axios
 * [axios-request-config]: https://github.com/axios/axios#request-config
 * [got]: https://www.npmjs.com/package/got
 */

import { URL, URLSearchParams } from 'url'

import { isValidLookingHttpHeaderPair } from './helpers'
import { DataSource } from './data-source'
import { Endpoint } from './endpoint'
import {
  DataSourceEndpointInterface,
  HttpHeaders,
  HttpRequestMethod,
  JsonObject,
  StringsHashMap,
  RequestConfigLogMessageFormatInterface,
} from './types'
import { setUrlSearchParamsHelper } from './url'

// @ts-ignore
import urlTemplate from 'url-template'

/**
 * ReadOnly copy of typical Axios or other HTTP client's configuration
 * Ideally created using {@link DataSourceEndpointRequestHelper#prepare}
 *
 * The URL in this object is separated in 3 parts:
 * - baseURL for the Request's protocol://host:port part
 * - path for the Request's part between the 3rd slash, right before the optionnal port number
 * - params as a URLSearchParams hash map we can convert into a string.
 *
 * Bookmarks:
 * - https://developer.mozilla.org/en-US/docs/Learn/Common_questions/What_is_a_URL
 */
export interface RequestConfigInterface {
  readonly headers: HttpHeaders

  /**
   * HTTP Verb/Method name
   *
   * Valid verbs (incomplete list):
   * - GET
   * - HEAD
   * - POST
   *
   * Bookmarks:
   * - https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods
   */
  readonly method: HttpRequestMethod

  /**
   * The protocol, host, port that is used.
   */
  readonly baseURL: string

  /**
   * URL property is URLEncoded, useful for HTTP client, if you want full string for logging or debugging, see above
   *
   * See related {@link RequestConfigInterface#decodedURL} for decodeURIComponent variant.
   *
   * Notice the URL does not necessarily contain the params as ?...
   * (This is called either URL Query, or search params.)
   *
   * That isn't implemented here just yet.
   * Instead, we'll leverage Axios' way of doing it where
   * we have to give a [URLSearchParams][1] hash map at the params property.
   *
   * Refer to:
   * - https://github.com/sindresorhus/got#searchparams
   * - https://github.com/axios/axios#request-config
   *
   * To get URL, you can also convert RequestConfigInterface to a string
   *
   * @example
   * ```js
   * const requestConfig = new RequestConfig({
   *   method: 'GET',
   *   path: '/foo/bar'
   * })
   * String(requestConfig) // GET http://localhost/foo/bar
   * ``̀
   *
   * Related:
   * - https://github.com/DefinitelyTyped/DefinitelyTyped/issues/34960
   */
  readonly url: string

  /**
   * The URL's path, Anything after the hostname:port, before ?
   *
   * May contain an [URITemplate][uri-template]
   *
   * [uri-template]: http://www.rfc-editor.org/info/rfc6570
   */
  readonly path: string

  /**
   * The URL's Search params, what we put after ?, separated by amperstands &
   *
   * In Axios HTTP Client, this gets converted into [Query String][query-string]
   *
   * [query-string]: https://en.wikipedia.org/wiki/Query_string
   */
  readonly params: URLSearchParams

  /**
   * Axios request Timeout
   */
  readonly timeout?: number

  /**
   * The payload to send for this request
   */
  readonly data?: JsonObject
}

export interface MutableRequestConfigInterface extends RequestConfigInterface {
  /**
   * URL decoded version, of the URL. For logging and human-friendlyness.
   */
  readonly decodedURL: string

  /**
   * Free-form data holder.
   * Anything goes, we aren't typing this just (yet?).
   */
  setData(data: any): void

  /**
   * Optionally add properties to the params hash-map
   * so we can append them as query string.
   * See notes in {@link RequestConfigInterface.params}
   *
   * If we change from Axios to another client, we might need to change
   * the params into another name.
   *
   * See Sindre Horus' HTTP Client (instead of Axios),
   * for URL Query, he uses a different name
   * https://github.com/sindresorhus/got#searchparams
   */
  addURLSearchParam(key: string, value: null | number | string | boolean): void

  /**
   * Add a last-minute Request Header
   * That can be useful if the endpoint also requires context sensitive headers, for example when
   * we can't make adding that header before.
   */
  addHeader(header: string, value: string): void

  export(): RequestConfigInterface

  toLogMessageHashMap(): RequestConfigLogMessageFormatInterface

  getUrl(): URL
}

/**
 * Just before issuing request by sending the RequestConfig to the HTTP client
 * for usage, let's make sure we have all we need.
 */
export class MutableRequestConfig implements MutableRequestConfigInterface {
  headers: HttpHeaders

  readonly method: HttpRequestMethod

  readonly baseURL: string

  /**
   * The URL object we will mutate
   */
  // @ts-ignore
  private _url: URL

  /**
   * The string version of the URL, the full representation
   * {@link RequestConfig#_url}
   */
  url: string

  readonly path: string

  readonly data?: JsonObject

  timeout?: number = 0

  /**
   * Once we've made sure our request is ready to be issued,
   * use this class to convert into Axios' HTTP client request helper.
   *
   * If an Exception had been thrown at constructor,
   * refer to {@link DataSourceEndpointRequestHelper#getExpandedPath}
   */
  constructor(ds: DataSourceEndpointRequestHelper) {
    let path = '/'
    if ('getExpandedPath' in ds) {
      // Not sure it is a good idea to silently avoid
      // making DataSourceEndpointRequestHelper checking for us.
      // But just in case we want to use this class in other use-cases.
      path = ds.getExpandedPath()
    }
    const baseURL = ds.baseURL || 'http://localhost'
    this.headers = { ...(ds.headers || {}) }
    this.method = ds.method
    this.baseURL = baseURL
    this.path = path
    const url = new URL(baseURL + path)
    url.searchParams.sort()
    this._url = url
    Object.defineProperty(this, '_url', {
      enumerable: false,
      value: url,
      writable: false,
    })
    this.url = url.toString()
  }

  addHeader(header: string, value: string): void {
    if (isValidLookingHttpHeaderPair(header, value)) {
      this.headers = {
        ...this.headers,
        [header]: value,
      }
    }
  }

  addURLSearchParam(
    key: string,
    value: null | number | string | boolean,
  ): void {
    const v = String(value)
    if (this._url.searchParams.has(key)) {
      this._url.searchParams.delete(key)
      this._url.searchParams.append(key, v)
    } else {
      this._url.searchParams.append(key, v)
    }
    this._url.searchParams.sort()
    this.url = this._url.toString()
  }

  setData(data: any): void {
    const value = JSON.parse(JSON.stringify(data))
    Object.defineProperty(this, 'data', {
      enumerable: true,
      value,
      writable: false,
    })
  }

  get params(): URLSearchParams {
    return this._url.searchParams
  }

  /**
   * Since we often use URL Query parameters that are escaped
   * We still want to have them decoded for logging output
   */
  get decodedURL(): string {
    return decodeURIComponent(this.url)
  }

  /**
   * For logging purposes, what infos do we want to share
   */
  toLogMessageHashMap(): RequestConfigLogMessageFormatInterface {
    const out: RequestConfigLogMessageFormatInterface = {
      method: this.method,
      url: this.decodedURL,
    }
    return out
  }

  /**
   * GET http://localhost/foo/bar
   * As if it was an HTTP request over TCP
   * Useful for logging.
   */
  toString(): string {
    return `${this.method} ${this.decodedURL}`
  }

  toJSON(): RequestConfigInterface {
    const out: RequestConfigInterface = {
      headers: { ...this.headers },
      method: this.method,
      baseURL: this.baseURL,
      path: this.path,
      url: this.url,
      params: this._url.searchParams,
      data: this.data,
    }

    if (this.timeout) {
      Object.assign(out, { timeout: this.timeout })
    }

    return out
  }

  getUrl(): URL {
    return setUrlSearchParamsHelper(this._url, {})
  }

  export(): RequestConfigInterface {
    return this.toJSON()
  }
}

/**
 * Create an Axios compatible Request configuration.
 *
 * Declare which internal Backend will be used.
 *
 * To avoid issuing a request with missing parameters in the URL
 * We need a way to make sure we haven't forgotten them.
 *
 * Attach that new endpoint to the data source, give it an alias "foo-bar"
 *
 * @example
 * ```js
 * const dataSource = new DataSource({
 *   hostname: 'secret.example.org',
 *   ip: '172.22.60.6',
 *   port: 8080,
 * })
 * dataSource.addHeader('X-Tenant', 'ACMECORP') // Assuming we need the HTTP Header, for any attached endpoints
 * const endpoint = new Endpoint('POST', '/foo/bar{/FooID*}{?fields,q,limit,page}')
 * endpoint.addHeader('From', 'john.doe@example.org') // Sometimes we need HTTP headers
 * endpoint.addHeader('Content-Type', 'application/json')
 * dataSource.set('foo-bar', endpoint)
 * ```
 *
 * So we can do the following HTTP method
 *
 * ``̀ http
 * POST http://secret.example.org:8080/foo/bar/111-222-333?fields=name&limit=10
 * Content-Type: application/json
 * ```
 *
 * Where we can even use the same
 * ... later, before making an HTTP request, let's prepare the client.
 *
 * ```js
 * const helper = DataSourceEndpointRequestHelper(dataSource, 'foo-bar')
 * helper.setURITemplateData({TenantName: 'ACMECORP'})
 * const requestConfig = helper.prepare()
 * ```
 */
export class DataSourceEndpointRequestHelper
  implements DataSourceEndpointInterface {
  endpoint: Endpoint

  readonly headers: HttpHeaders = {
    // 'Authorization': 'Bearer 111.222.333',
  }

  readonly method: HttpRequestMethod = 'GET'

  /**
   * The protocol, host, port that is used.
   * {@link RequestConfigInterface#baseURL}
   */
  readonly baseURL: string = 'http://localhost'

  /**
   * URL is the concatenation of baseURL and path.
   * {@link RequestConfigInterface#url}
   */
  url: string = 'http://localhost/'

  /**
   * Path is the part of the URL right after the port number.
   * {@link RequestConfigInterface#path}
   */
  path: string = '/'

  readonly uriTemplateData: StringsHashMap = {}

  get hasURITemplateData(): boolean {
    const assertion =
      this.uriTemplateData && Object.keys(this.uriTemplateData).length > 0
    return assertion === true
  }

  get hasURITemplateParts(): boolean {
    const test = /\{\w+\}/g.test(this.path)
    return test === true
  }

  setURITemplateData(data: any): void {
    const dto = { ...this.headers }
    Object.assign(dto, { ...(data || {}) })
    const value = Object.freeze(dto)
    Object.defineProperty(this, 'uriTemplateData', {
      enumerable: true,
      value,
      writable: false,
    })
  }
  /**
   * The goal of this method is either to get the expanded path from an URITemplate.
   * OR to use it as validation by throwing an Exception should it be invalid.
   *
   * The objective is to avoid issuing HTTP requests with missing placeholders
   * that gets emptied because of missing key, we want to catch those errors early.
   * And avoid getting HTTP Requests with mandatory URL Path parts, and/or Query parameters
   */
  getExpandedPath(): string {
    const path = this.endpoint.path
    const uriTemplateData = this.uriTemplateData || {}
    if (this.hasURITemplateParts && this.hasURITemplateData === false) {
      const message = `URL "${path}" contains URITemplate, but we do not have uriTemplateData property set, did you use DataSourceEndpointRequestHelper#setURITemplateData()?`
      throw new Error(message)
    }
    const parsedTemplatedPath = urlTemplate.parse(path)
    const expandedPath = parsedTemplatedPath.expand(uriTemplateData)
    return expandedPath
  }

  constructor(origin: DataSource, alias: string, headers?: HttpHeaders) {
    if (!origin.has(alias)) {
      const message = `Endpoint "${alias}" not configured on this origin`
      throw new Error(message)
    }
    const endpoint = origin.get(alias)
    this.endpoint = endpoint

    Object.defineProperty(this, 'baseURL', {
      enumerable: true,
      value: origin.baseURL,
      writable: false,
    })

    const maybeHeaders = {}
    if (headers && Object.keys(headers).length > 0) {
      Object.assign(maybeHeaders, { ...headers })
    }
    Object.assign(this.headers, {
      ...origin.headers,
      ...endpoint.headers,
      ...maybeHeaders,
    })

    Object.defineProperty(this, 'method', {
      enumerable: true,
      value: endpoint.method,
      writable: false,
    })

    this.path = endpoint.path
    this.url = origin.baseURL + endpoint.path
  }

  /**
   * Use this method to tell Node.js' HTTP Client how to make an HTTP request
   *
   * If an exception has been thrown, refer to {@link DataSourceEndpointRequestHelper#getExpandedPath}
   */
  toRequestConfig(): MutableRequestConfigInterface {
    return this.prepare()
  }

  prepare(): MutableRequestConfigInterface {
    return new MutableRequestConfig(this)
  }
}

export const requestConfig = (
  origin: DataSource,
  alias: string,
): DataSourceEndpointRequestHelper => {
  return new DataSourceEndpointRequestHelper(origin, alias)
}
