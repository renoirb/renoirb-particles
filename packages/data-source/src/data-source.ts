import { Endpoint } from './endpoint'
import {
  DataSourceConstructorOptions,
  DataSourceInterface,
  EndpointsDictionary,
  HttpHeaders,
} from './types'
import {
  getProperty,
  hashMapHasOnlyStringValues,
  isValidLookingHttpHeaderPair,
  mustBeLoopbackIfLocalhost,
} from './helpers'

/**
 * Data Source.
 *
 * A service that understands HTTP on which we can have multiple
 * baseURLs ("Endpoint").
 *
 * @public
 * @author Renoir Boulanger <contribs@renoirboulanger.com>
 */
export class DataSource implements DataSourceInterface {
  private _toggleHostHeader: boolean = false

  isLocalHost(): boolean {
    const ip = this.ip ? this.ip : null
    const hostname = this.hostname ? this.hostname : 'localhost'
    const ipIsLocalhost =
      typeof ip === 'string' && String(ip).startsWith('127.')
    const hostnameIsLocalhost =
      typeof hostname === 'string' &&
      String(hostname).toLowerCase().startsWith('localhost')

    return ipIsLocalhost || hostnameIsLocalhost
  }

  get host(): string {
    const protocol = this.protocol ? this.protocol : null
    const hostname = this.hostname ? this.hostname : 'localhost'
    const port = this.port ? this.port : null

    let hostString = hostname

    if (protocol === 'http' && port !== 80) {
      hostString += ':' + port
    }
    if (protocol === 'https' && port !== 443) {
      hostString += ':' + port
    }

    return hostString
  }

  set host(_: string) {
    // Setter. We make it No-Op (do no operations, i.e. nothing)
    // NoOp, because ip and hostname are going to regenerate this anyway
  }

  endpoints: EndpointsDictionary = {}

  private _headers: HttpHeaders = {}

  get headers(): HttpHeaders {
    const headers = { ...this._headers }
    if (this._toggleHostHeader) {
      const host = this.host
      Object.assign(headers, { Host: host })
    }
    return headers
  }

  hostname: string = 'localhost'

  ip?: string

  port: number = 80
  protocol: string = 'http'

  constructor(args: DataSourceConstructorOptions = {}) {
    const opts = args ? { ...args } : {}
    // If we have no hostname at all, set localhost
    // We will later on validate if we have an IP, if it is in class for loopback (127.)
    // If there is an IP, and no hostname, the localhost hostname will be used, but should
    // throw an error if the IP is not loopback when hostname is localhost.
    // Which means if one wants to set an IP address, he should be forced to set a name too
    if (!Reflect.has(opts, 'hostname')) {
      opts.hostname = 'localhost'
    }
    const hasOption = (prop: string): boolean =>
      Object.keys(opts).includes(prop) === true
    const mustHaveOneTrue = [hasOption('ip'), hasOption('hostname')]
    // @ts-ignore
    if (mustHaveOneTrue.includes(true) === false) {
      // If we have at least one true missing
      const message = `Missing required options, we need at least one of the following keys: ip, hostname. We received: ${Object.keys(
        opts || {},
      ).join(',')}, ${JSON.stringify(opts || {})}`
      throw new Error(message)
    }
    mustBeLoopbackIfLocalhost(opts || {})
    const that = this
    // ECMAScript 5 Property descriptor
    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/defineProperty
    const defineProperty = (propName: string, descriptor: PropertyDescriptor) =>
      Object.defineProperty(that, propName, descriptor)
    Object.keys(opts).forEach((prop) => {
      if (prop in opts) {
        if (prop !== 'headers') {
          // @ts-ignore
          const value = opts[prop]
          const descr: PropertyDescriptor = {
            configurable: false,
            value,
          }
          defineProperty(prop, descr)
        }
      }
    })
    const headers: HttpHeaders = {}
    if ('headers' in opts && hashMapHasOnlyStringValues(opts.headers || {})) {
      Object.assign(headers, { ...opts.headers })
    }
    this._headers = headers
  }

  toString(): string {
    return this.baseURL
  }

  get baseURL(): string {
    const parts: string[] = []

    const protocol = this.protocol ? this.protocol : 'http'
    const hostname = this.hostname ? this.hostname : 'localhost'
    const ip = this.ip ? this.ip : null
    const port = this.port ? this.port : null

    parts.push(protocol)
    parts.push('://')

    let host = hostname
    if (this._toggleHostHeader && typeof ip === 'string') {
      host = ip
    }

    if (protocol === 'http' && port !== 80) {
      host += ':' + port
    }
    if (protocol === 'https' && port !== 443) {
      host += ':' + port
    }

    parts.push(host)
    return parts.join('')
  }

  toggleUseIpAndAddHostHeader(): void {
    const ip = this.ip ? this.ip : null
    if (typeof ip === 'string') {
      this._toggleHostHeader = true
    } else {
      const message = `We cannot use IP based because we are missing an IP address`
      throw new Error(message)
    }
  }

  addHeader(header: string, value: string): void {
    if (isValidLookingHttpHeaderPair(header, value)) {
      this._headers = {
        ...this._headers,
        [header]: value,
      }
    }
  }

  set(alias: string, endpoint: Endpoint): void {
    this.endpoints[alias] = endpoint
  }

  has(alias: string) {
    return alias in this.endpoints
  }

  get(alias: string): Endpoint {
    const endpoint = getProperty(this.endpoints, alias)
    if (!endpoint) {
      const list = Object.keys(this.endpoints).join(', ')
      const message = `Endpoint ${alias} not found in ${list}`
      throw new Error(message)
    }

    const headers = { ...this.headers, ...endpoint.headers }
    const copy = Object.assign(
      Object.create(Object.getPrototypeOf(endpoint)),
      endpoint,
    )
    copy.headers = headers

    return copy
  }

  public toJSON(): DataSourceInterface {
    const endpoints = JSON.parse(JSON.stringify(this.endpoints))

    const headers = { ...this.headers }

    const out: DataSourceInterface = {
      endpoints,
      headers,
      host: this.host,
      hostname: this.hostname,
      port: this.port,
      protocol: this.protocol,
    }
    if (this.ip) {
      out.ip = this.ip
    }

    return out
  }
}
