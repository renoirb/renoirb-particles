import { URLSearchParams } from 'url'

import { DataSourceInterface, HttpHeaderKeys, HttpHeaders } from './types'

export const slugifyPath = (p: string): string => {
  let out = String(p).toLowerCase()
  out = out.replace(/^\//, '')
  out = out.replace(/\/+/g, '/')
  out = out.replace(/\/$/, '')
  out = out.replace(/\//g, '-')
  return out
}

// @ts-ignore
export const hasPropIn = (prop: string, obj: any): boolean =>
  Object.keys(obj).includes(prop) === true

// @ts-ignore
export const getObjectKeys = <O>(o: O) => Object.keys(o) as (keyof O)[]

// export type Arguments<T> = T extends (...args: infer A) => any ? A : never
// export type Return<T> = T extends (...args: any[]) => infer R ? R : never
// <T extends keyof Job = keyof Job>(part: Pick<Job, T>)

export const getProperty = <T, K extends keyof T>(
  o: T,
  propertyName: K,
): T[K] | undefined => o[propertyName]

/**
 * Imagine we have a HashMap of HTTP Headers.
 *
 * Let's say somewhere in the runtime, we have an HTTP Header already pre-configured for "content-type",
 * and later down the runtime, we add another "Content-Type".
 * We'll end up having two times the same header because of case sensitiveness.
 * That is fine, but might create bugs.
 *
 * This is an utility that helps figuring out if from an headers HashMap <H>, if we tell a header name
 * (e.g. "content-TyPe"), if the HashMap already has one matching that name, in any other possible casInG,
 * to return it to us.
 * Or return what we've just given.
 *
 * ```js
 * let headers = {
 *   "Host": "example.org",
 *   "Content-Type": "text/html"
 * }
 *
 * appropriateHttpHeaderPicker(headers, 'content-TyPe') // 'Content-Type'
 * appropriateHttpHeaderPicker(headers, 'X-Forwarded-for') // 'X-Forwarded-for'
 * ```
 *
 * Bookmarks, in case the TypeScript notation looks too much like black magic:
 * - https://artsy.github.io/blog/2018/11/21/conditional-types-in-typescript/
 * - https://egghead.io/lessons/typescript-query-properties-with-keyof-and-lookup-types-in-typescript
 * - https://blog.logrocket.com/when-to-use-never-and-unknown-in-typescript-5e4d6c5799ad/
 * - https://medium.com/shyftplan-techblog/typescript-advanced-types-199ff1f3e3e8
 *
 * @public
 */
export const appropriateHttpHeaderPicker = <
  H extends HttpHeaders,
  K extends keyof H & HttpHeaderKeys
>(
  headers: H,
  headerName: K,
) =>
  getObjectKeys(headers).filter((k) => {
    return String(k).toLowerCase() === String(headerName).toLowerCase()
      ? k
      : false
  })[0] || headerName

export const hasAlreadyHttpHeader = <
  H extends HttpHeaders,
  K extends keyof H & HttpHeaderKeys
>(
  headers: H,
  headerName: K,
) => {
  const name = appropriateHttpHeaderPicker(headers, headerName)
  return name in headers
}

export const isValidLookingHttpHeaderPair = (
  name: string,
  value: string,
): boolean =>
  typeof name === 'string' &&
  /[a-z0-9\-]/i.test(name) &&
  typeof value === 'string'

export const hashMapHasOnlyStringValues = <T extends {}>(obj: T): boolean =>
  obj &&
  Object.entries<T>(obj)
    .map(([, v]) => typeof v === 'string')
    .includes(false) === false

/**
 * Take a HashMap `{key: "value"}`, make it an URL Search string
 *
 * Docs:
 * - https://url.spec.whatwg.org/#interface-urlsearchparams
 * - https://developer.mozilla.org/en-US/docs/Web/API/URLSearchParams
 *
 * @example
 * const kv = { username: 'jdoe', hl: 'fr,en' }
 * stringifyParams(kv)
 * // > "username=jdoe&hl=fr%2Cen"
 * stringifyParams(kv, false)
 * // > "username=jdoe&hl=fr,en"
 */
export const stringifyParams = <T>(
  hashMap: T,
  encodeUriComponents: boolean = true,
): string => {
  const params: URLSearchParams = new URLSearchParams()
  const entries = Object.entries(hashMap).filter(
    // @ts-ignore
    ([, v]) => typeof v === 'string',
  )
  // @ts-ignore
  for (const [name, value] of entries) {
    const n =
      encodeUriComponents === true
        ? encodeURIComponent(String(name))
        : String(name)
    const v =
      encodeUriComponents === true
        ? encodeURIComponent(String(value))
        : String(value)
    params.append(n, v)
  }
  const query: string[] = []
  // @ts-ignore
  for (const [key, value] of params.entries()) {
    query.push([key, value].join('='))
  }

  return query.join('&')
}

export const hasOptionsAndIsLoopback = (
  opts: Partial<DataSourceInterface>,
): boolean =>
  'ip' in opts &&
  typeof opts.ip === 'string' &&
  opts.ip.startsWith('127') === true

export const mightHaveOptionsHostnameAsLocalhost = (
  opts: Partial<DataSourceInterface>,
): boolean => {
  const hasNoHostname = 'hostname' in opts === false
  const hasHostnameAndIsLocalhost =
    'hostname' in opts &&
    typeof opts.hostname === 'string' &&
    opts.hostname.toLowerCase() === 'localhost'
  const inEitherCase = hasNoHostname || hasHostnameAndIsLocalhost
  return inEitherCase
}

export const mustBeLoopbackIfLocalhost = (
  opts: Partial<DataSourceInterface>,
): boolean => {
  const shouldBeLocalhost = mightHaveOptionsHostnameAsLocalhost(opts)
  const isLoopback = hasOptionsAndIsLoopback(opts)
  if (shouldBeLocalhost && !isLoopback && 'ip' in opts) {
    const message = `When no hostname, or hostname localhost, and there is an IP, it MUST be a loopback but with IP staring by 127. We received: ${JSON.stringify(
      opts,
    )}`
    throw new Error(message)
  }

  return shouldBeLocalhost && isLoopback
}
