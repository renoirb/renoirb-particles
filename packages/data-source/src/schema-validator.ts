import { SchemaValidator as BaseSchemaValidator } from '@renoirb/jsonschema-aware-loader'

export class SchemaValidator extends BaseSchemaValidator {
  static readonly SCHEMA: string = 'schemas/data-source-origins.schema.json'
  constructor() {
    super(__dirname, SchemaValidator.SCHEMA)
  }
}
