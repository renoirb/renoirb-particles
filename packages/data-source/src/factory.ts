import { DataSource } from './data-source'
import { Endpoint } from './endpoint'
import { HttpRequestMethod, DataSourceConstructorOptions } from './types'
import { SchemaValidator } from './schema-validator'

/**
 * Factory helper to setup a tree of available Endpoints to make HTTP calls to.
 *
 * We pass a configuration JSON string, it loads that JSON, makes sure the data is valid
 * then binds together an "DataSource" (i.e. an IP address, port, protocol) and adds
 * "Endpoints" (i.e. GET /endpoint) with alias names.
 * Using this factory, we can make the project importing that library to only have to
 * load the JSON as a string, and pass it to this helper.
 *
 * @author Renoir Boulanger <contribs@renoirboulanger.com>
 */
export const factory = (
  jsonString: string,
  opts: DataSourceConstructorOptions = {},
): DataSource => {
  // Throw as early as possible, so that we don't get a data-source
  // with corrupted or bound to fail configurations.
  const sv = new SchemaValidator()
  let jsonStringToValidate = jsonString
  if (Object.keys(opts).length > 0) {
    // We want to support both possibilities of just validating JSON string already loaded
    // And adding values that can be added on at runtime.
    const attempt = JSON.parse(jsonString)
    const merged = Object.assign(attempt, opts)
    jsonStringToValidate = JSON.stringify(merged)
  }
  // Load and throw ASAP
  const validated = sv.loadFromString(jsonStringToValidate)
  const {
    endpoints = {},
    ...rest
  } = validated.toJSON() as DataSourceConstructorOptions

  // ... Then if it worked, carry-on
  const dataSource = new DataSource(rest)
  // @ts-ignore
  for (const [alias, descriptor] of Object.entries(endpoints)) {
    if (descriptor) {
      const { method = 'GET', path = '/', headers = {} } = descriptor
      const endpoint = new Endpoint(method as HttpRequestMethod, path, headers)
      dataSource.set(alias, endpoint)
    }
  }

  return dataSource
}
