import {
  DataSourceEndpointInterface,
  HttpHeaderKeys,
  HttpHeaders,
  HttpRequestMethod,
} from './types'
import {
  appropriateHttpHeaderPicker,
  hasAlreadyHttpHeader,
  isValidLookingHttpHeaderPair,
} from './helpers'
import { DataSourceRuntimeError } from './errors'

/**
 * Data Source Endpoint.
 *
 * Say we want to have a place where we have pre-configured HTTP Calls such as
 *
 *     GET /Backends/BackendMicroServiceName/v1/ControllerName/ACMECORP/foo?limit=10 HTTP/1.1
 *     Content-Type: application/vnd.api+json
 *     Accept: text/yaml
 *     Authorization: Bearer 111.222.333
 *     NonStandardHeader: aaaaaaa-b-ccccccc
 *
 * Imagine "Backends" is a known URL namespace for storing many other microservices.
 * "BackendMicroServiceName" is one of them, and there is a "v1" Controller "ControllerName".
 * The route needs to have some context-sensive namespace, say a client name "ACMECORP".
 *
 * In that respect, our Data Source would have an Endpoint defined for "/Backends/BackendMicroServiceName"
 *
 * What HTTP call we can make, with what HTTP headers, etc.
 *
 * Imagine an HTTP service may be able to return more than one content-type, just by changing
 * the Accept header.
 * While we may have a default of free-form JSON, sometimes even without
 * any schemas (e.g. JSONSchema), we might want to take the same endpoint and later on
 * change the header.
 * We would need to rewrite the Accept HTTP header.
 *
 * HTTP headers are normally (not always!) only once per request or response.
 * While we're implementing a HashMap to avoid name collision, we may end up with
 * two equivalent headers written differently (e.g. "Content-type: text/html" and "content-type: text/html"
 * Normalizing headers, headers that are case-sensitive, or that can't be overwritten once they're set
 * are handled in {@link HttpHeadersNormalizer}
 *
 * An Endpoint is a predefined HTTP call, from which we can get a copy, plug-in parameters,
 * make sure everything is set before using it.
 *
 * Which will avoid us common mistakes such as ending up with URL that would not be found due to missing parts,
 * maybe with HTTP request headers that would make the service return a client error.
 *
 * Also, that we can try-catch before making the request and return meaningful feedback.
 *
 * Endpoint should:
 * - Support URL path to be an [URITemplates][uri-templates] (i.e. /hello/{name} => /hello/jdoe)
 * - Make sure if URITemplates, that we ARE passing-in values before issuing request
 * - Can add/replace HTTP headers
 * - Can tell which HTTP headers are must-haves
 *
 * [uri-templates]: https://datatracker.ietf.org/doc/rfc6570/
 *
 * @public
 * @author Renoir Boulanger <contribs@renoirboulanger.com>
 */
export class Endpoint implements DataSourceEndpointInterface {
  headers: HttpHeaders = {}

  method: HttpRequestMethod = 'GET'

  path: string = '/'

  constructor(
    method: HttpRequestMethod,
    path: string,
    headers: HttpHeaders = {},
  ) {
    const hasContentType = hasAlreadyHttpHeader(headers, 'content-type')
    const isPost = /^post$/i.test(method)
    if (isPost && !hasContentType) {
      // In order to enforce/avoid runtime errors, let's make sure POST endpoints ALWAYS
      // tells what is the content-type
      const message = `Missing required Content-Type HTTP Request header, POST request types must have it declared explicitly to avoid runtime errors.`
      throw new DataSourceRuntimeError(
        'ENDPOINT_POST_MUST_HAVE_CONTENTTYPE',
        message,
      )
    }

    this.method = method
    this.path = path
    this.headers = {
      ...headers,
    } as HttpHeaders
  }

  addHeader(header: HttpHeaderKeys, value: string): void {
    if (isValidLookingHttpHeaderPair(header, value)) {
      const headerKey = appropriateHttpHeaderPicker(this.headers, header)
      this.headers = {
        ...this.headers,
        [headerKey]: value,
      }
    }
  }
}
