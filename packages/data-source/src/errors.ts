/**
 * Encapsulates information about an error
 *
 * ---
 * Inspiration: https://github.com/microsoft/rushstack/blob/4d18946/apps/rush-lib/src/logic/taskRunner/TaskError.ts#L7
 */
export class DataSourceRuntimeError extends Error {
  /**
   * Error Type Discriminant.
   * This should eventually become a known set
   */
  public readonly type: string

  public constructor(type: string, message: string) {
    super(message)
    this.type = type
  }

  public get message(): string {
    return `[${this.type}] '${super.message}'`
  }

  public toString(): string {
    return this.message
  }
}
