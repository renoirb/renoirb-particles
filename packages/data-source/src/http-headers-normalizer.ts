import { HttpHeaders, HttpHeaderKeys } from './types'

/**
 * Normalize HTTP Headers.
 *
 * Most(!) HTTP headers are case insensitive, others aren't.
 * Sometimes we might want to replace an HTTP header key with another.
 * What if we have a header that can exist only once, but exist
 * more than once, with different casing.
 *
 * This helper is for that purpose.
 *
 * Thank you middyjs/middy
 * https://github.com/middyjs/middy/blob/a3e2ad8/src/middlewares/httpHeaderNormalizer.js
 * And watson/http-headers
 * https://github.com/watson/http-headers/blob/dd9fceb/index.js
 *
 * This is a refactor based on middy's HttpHeaderNormalizer middleware, but not bound to
 * anything. Just manipulation helpers.
 *
 * [http-headers]: https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers
 *
 * @author Renoir Boulanger <contribs@renoirboulanger.com>
 */

export interface HttpHeadersNormalizerOptions {
  exceptionsList?: string[]
}

export interface HttpHeadersMapContainer {
  headers: HttpHeaders
}

export const httpHeadersNormalizer = (
  opts: Partial<HttpHeadersNormalizerOptions> = {},
) => {
  const exceptionsList: HttpHeaderKeys[] = [
    /**
     * Case SensItiVe headers.
     */
    'ALPN',
    'C-PEP',
    'C-PEP-Info',
    'CalDAV-Timezones',
    'Content-ID',
    'Content-MD5',
    'DASL',
    'DAV',
    'DNT',
    'ETag',
    'GetProfile',
    'HTTP2-Settings',
    'Last-Event-ID',
    'MIME-Version',
    'Optional-WWW-Authenticate',
    'Sec-WebSocket-Accept',
    'Sec-WebSocket-Extensions',
    'Sec-WebSocket-Key',
    'Sec-WebSocket-Protocol',
    'Sec-WebSocket-Version',
    'SLUG',
    'TCN',
    'TE',
    'TTL',
    'WWW-Authenticate',
    'X-ATT-DeviceId',
    'X-DNSPrefetch-Control',
    'X-UIDH',

    /**
     * {@link PrivateHttpHeaderKeys}
     */
    'X-Request-Id',
  ]

  const options: HttpHeadersNormalizerOptions = Object.assign({}, opts || {})

  if ('exceptionsList' in options && Array.isArray(options.exceptionsList)) {
    // @ts-ignore
    exceptionsList.push(...(options.exceptionsList || []))
  }

  const exceptions: Record<string, string> = exceptionsList.reduce(
    (acc: Record<string, string>, curr: string) => {
      acc[curr.toLowerCase()] = curr
      return acc
    },
    {},
  )

  const normalizeHeaderKey = (key: string): string => {
    if (exceptions[key.toLowerCase()]) {
      return exceptions[key.toLowerCase()]
    }

    return key
      .split('-')
      .map(
        (text) => text.charAt(0).toUpperCase() + text.substr(1).toLowerCase(),
      )
      .join('-')
  }

  return function normalize(
    input: Partial<HttpHeadersMapContainer>,
  ): HttpHeaders | null {
    const headers: HttpHeaders = Object.create(null)
    if (input && 'headers' in input) {
      Object.keys(input.headers as HttpHeaders).forEach((key) => {
        if (input.headers && key in input.headers) {
          headers[normalizeHeaderKey(key)] = input.headers[key]
        }
      })

      return headers
    }

    return null
  }
}
