# Change Log - @renoirb/data-source

This log was last generated on Tue, 24 Nov 2020 09:11:31 GMT and should not be manually modified.

## 0.3.3
Tue, 24 Nov 2020 09:11:31 GMT

### Patches

- Node.js exports must start by dot slash

## 0.3.2
Tue, 24 Nov 2020 07:40:40 GMT

### Patches

- README

## 0.3.1
Mon, 23 Nov 2020 07:47:01 GMT

### Patches

- Upgrade @types/node

## 0.3.0
Wed, 15 Jul 2020 02:59:19 GMT

### Minor changes

- Packaging and bundling normalization and deps update

## 0.2.8
Fri, 05 Jun 2020 02:41:05 GMT

### Patches

- Attempt at publishing to see if import works

## 0.2.7
Fri, 05 Jun 2020 02:35:11 GMT

### Patches

- Attempt at publishing to see if import works

## 0.2.6
Fri, 05 Jun 2020 02:24:16 GMT

### Patches

- Attempt at publishing to see if import works

## 0.2.5
Thu, 04 Jun 2020 22:34:20 GMT

### Patches

- Attempt at publishing to see if import works

## 0.2.4
Thu, 04 Jun 2020 14:48:59 GMT

### Patches

- Broken package json exports

## 0.2.3
Thu, 04 Jun 2020 14:17:17 GMT

*Version update only*

## 0.2.2
Thu, 04 Jun 2020 13:55:57 GMT

### Patches

- Forgot to make peerDependency jsonschema-aware-loader

## 0.2.1
Thu, 04 Jun 2020 00:51:48 GMT

### Patches

- Dependencies update

## 0.2.0
Wed, 03 Jun 2020 23:37:39 GMT

### Minor changes

- New package @renoirb/data-source

