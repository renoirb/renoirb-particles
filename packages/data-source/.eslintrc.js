/**
 * Boilerplate ESLint config, adjust to your requirements
 * See: https://www.npmjs.com/package/@renoirb/conventions-use-eslint
 */
const base = require('@renoirb/conventions-use-eslint')

/**
 * @type {import('@types/eslint').Linter.Config}
 */
const main = {
  ...base,
  parserOptions: {
    tsconfigRootDir: process.cwd(),
    project: './tsconfig.json',
    ...(base.parserOptions || {}),
  },
  rules: {
    ...base.rules,
    // TODO: Make this list smaller, not bigger
    '@rushstack/no-null': 'off',
    '@typescript-eslint/explicit-function-return-type': 'off',
    '@typescript-eslint/member-ordering': 'off',
    '@typescript-eslint/naming-convention': 'off',
    '@typescript-eslint/no-explicit-any': 'off',
    '@typescript-eslint/no-parameter-properties': 'off',
    '@typescript-eslint/typedef': 'off',
  },
}

module.exports = main
