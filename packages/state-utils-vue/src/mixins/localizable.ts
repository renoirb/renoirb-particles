/* tslint:disable:no-any no-shadowed-variable no-invalid-this typedef object-literal-sort-keys */

import { TranslatorMixin } from './translator'

/**
 * Vue Component Mixin related to i18n (Internationalization)
 * and l10n (Localization).
 */

/**
 * Quick object check - this is primarily used to tell
 * Objects from primitive values when we know the value
 * is a JSON-compliant type.
 *
 * Copy-Pasta from Vue.js transpiled source (!).
 *
 * https://github.com/vuejs/vue/blob/2.6/dist/vue.runtime.esm.js
 */
// @ts-ignore
// tslint:disable-next-line:typedef
export function isObject(obj) {
  return obj !== null && typeof obj === 'object'
}

/**
 * Badge props
 *
 * @mixin
 */
export const LocalizableMixin = {
  mixins: [TranslatorMixin],
  props: {
    numberFormatOptions: {
      type: Object,
      default: null,
    },
    timeZone: {
      type: String,
      default: 'America/New_York',
    },
    locale: {
      type: String,
      default: 'en-CA',
      // @ts-ignore
      validator(value) {
        const isAtLeastFiveChars = value.length > 4
        const hasDash = value[2] === '-'
        const langCode = value.split('-')[0].toLowerCase()
        const onlyTwo = langCode.length === 2
        // TODO: Use instead Intl's API for this.
        return isAtLeastFiveChars && hasDash && onlyTwo
      },
    },
  },
  computed: {
    // @ts-ignore
    normalizedNumberFormatOptions() {
      // @ts-ignore
      const isAnObject = isObject(this.numberFormatOptions)
      // @ts-ignore
      const isNotNull = this.numberFormatOptions !== null
      if (isAnObject && isNotNull) {
        // @ts-ignore
        const options = this.numberFormatOptions
        // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/NumberFormat
        // https://www.ecma-international.org/ecma-402/2.0/#sec-InitializeNumberFormat
        const hasStyle = Reflect.has(options, 'style')
        const hasCurrency = Reflect.has(options, 'currency')
        const hasLocaleMatcher = Reflect.has(options, 'localeMatcher')
        const hasCurrencyDisplay = Reflect.has(options, 'currencyDisplay')
        const hasMaximumSignificantDigits = Reflect.has(
          options,
          'maximumSignificantDigits',
        )

        const testRunOutcome =
          hasStyle ||
          hasCurrency ||
          hasLocaleMatcher ||
          hasCurrencyDisplay ||
          hasMaximumSignificantDigits

        if (testRunOutcome) {
          return { ...options }
        }
      }

      return null
    },
  },
}
