export const TranslatorMixin = {
  methods: {
    /**
     * Attempt, gracefully, at translating a string.
     *
     * @todo Use TranslatorMixin instead.
     *
     * @link https://kazupon.github.io/vue-i18n/en/api.html#vue-injected-methods
     * @param {string} text Text to translate
     */
    // tslint:disable:typedef
    $translator(label: string = '...'): string {
      // const hasTranslator = Reflect.has(this, '$i18n') && Reflect.has(this.$i18n, 't')
      let translated = `${label}`
      try {
        // @ts-ignore
        // tslint:disable:no-invalid-this
        const attempt = this.$t ? this.$t(label) : label
        translated = attempt
      } catch (e) {
        // Nothing to do
      }

      return translated
    },
  },
}
