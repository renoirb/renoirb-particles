/* tslint:disable:object-literal-sort-keys */

export type DateTimeVariantTypes =
  | 'json'
  | 'utc'
  | 'iso'
  | 'numeric'
  | 'short'
  | 'shorter'
  | 'long'
  | 'time'

export type DateTimeVariantsValueTypes =
  | DateTimeFormatUsingMethodNameStringType
  | Intl.DateTimeFormatOptions

export type DateTimeFormatUsingMethodNameStringType =
  | 'toJSON'
  | 'toUTCString'
  | 'toISOString'

export interface IVariantsHash {
  [name: string]: DateTimeVariantsValueTypes
}
/**
 * Store some predefined DateTime variants.
 *
 * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date/toLocaleString
 * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/DateTimeFormat
 * https://www.iana.org/time-zones
 * https://timezonedb.com/
 * https://github.com/umpirsky/locale-list
 * https://github.com/bavardage/TypeScript/blob/538978e3be704d759a568aea36270f6859c9d95d/src/lib/intl.d.ts
 *
 * Let's not forget; https://xkcd.com/1179/
 */
export class DateTimeVariants {
  public readonly variants: IVariantsHash = {}

  constructor(
    public readonly defaultVariant: DateTimeVariantTypes = 'shorter',
  ) {
    this.defaultVariant = defaultVariant

    const numeric: Intl.DateTimeFormatOptions = {
      day: 'numeric',
      month: 'numeric',
      year: 'numeric',
    }
    const short: Intl.DateTimeFormatOptions = {
      day: 'numeric',
      month: 'numeric',
      year: 'numeric',
      hour: 'numeric',
      minute: 'numeric',
      second: 'numeric',
      hour12: false,
    }
    const shorter: Intl.DateTimeFormatOptions = {
      day: 'numeric',
      month: 'long',
      year: 'numeric',
    }
    const long: Intl.DateTimeFormatOptions = {
      day: 'numeric',
      month: 'long',
      year: 'numeric',
      weekday: 'long',
    }
    const time: Intl.DateTimeFormatOptions = {
      hour: 'numeric',
      minute: 'numeric',
      second: 'numeric',
      hour12: false,
    }
    const full: Intl.DateTimeFormatOptions = {
      day: 'numeric',
      month: 'long',
      year: 'numeric',
      weekday: 'long',
      hour: 'numeric',
      minute: 'numeric',
      second: 'numeric',
      hour12: false,
      timeZoneName: 'long',
    }

    this.variants = {
      full,
      iso: 'toISOString',
      json: 'toJSON',
      long,
      numeric,
      short,
      shorter,
      time,
      utc: 'toUTCString',
    }
  }

  public has(name: string): boolean {
    return Reflect.has(this.variants, name)
  }

  public get(name: string): DateTimeVariantsValueTypes {
    const pick = this.has(name) ? name : this.defaultVariant
    return this.variants[pick]
  }

  get variantNames(): string[] {
    return Object.keys(this.variants)
  }
}
