export const mustHaves = (keys: string[] = []) => {
  return (subject: any): boolean => {
    const mustBeFalse = keys
      .map((w) => Reflect.ownKeys(subject).includes(w))
      .includes(false)
    return mustBeFalse === false
  }
}
