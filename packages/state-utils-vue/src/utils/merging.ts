// @ts-nocheck
/* tslint:disable */

/**
 * Recursively merge any objects INTO target.
 * Notice IT IS a traditional function, not an arrow one.
 * That is because WE WANT to leverage JavaScript "arguments" feature
 * of traditional functions.
 * Mesning that any other arguments, except the first ("target"), will be
 * MERGED INTO target.
 *
 * https://github.com/ElemeFE/element/blob/1.x/src/utils/merge.js
 */
// tslint:disable:typedef
// @ts-ignore
export function recursivelyMerge(target) {
  // @ts-ignore
  for (let i = 1, j = arguments.length; i < j; i++) {
    const source = arguments[i] || {}
    for (const prop in source) {
      if (source.hasOwnProperty(prop)) {
        const value = source[prop]
        if (value !== undefined) {
          target[prop] = value
        }
      }
    }
  }

  return target
}

export const objectAssign = (target: object = {}) => (
  source: object = {},
  omitProperties: string[] = [],
) => {
  Object.getOwnPropertyNames(source).forEach((propertyName) => {
    const descriptor:
      | PropertyDescriptor
      | undefined = Reflect.getOwnPropertyDescriptor(source, propertyName)
    const isNotToOmmit: boolean = omitProperties.includes(propertyName) !== true
    if (typeof descriptor !== 'undefined' && isNotToOmmit) {
      Object.defineProperty(target, propertyName, descriptor)
    }
  })
}
