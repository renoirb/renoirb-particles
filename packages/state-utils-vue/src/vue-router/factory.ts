export interface IRouteDescriptor {
  /**
   * Name of route.
   * Should be a slug (i.e. lowercased, with only letters, numbers, dash. No space, underscore, nor slash)
   * Difference with "slug" property is that one is vue-router abstract name
   *
   * For example:
   * We might have Nuxt with a file as `pages/users/_id/index.vue`
   * Which would give a vue-router name of `users-id`, that is the return value.
   *
   * @default "index"
   */
  name: string
  /**
   * URL Parameters, we would add them as URL Query parameters.
   *
   * @example
   * // For the following URL Query `?limit=3&q=Status_$eq_$Disabled`
   * const params = {limit: 3, q: 'Status_$eq_$Disabled'}
   */
  params: { [key: string]: string | number }
  /**
   * URL Path
   * @default "/"
   */
  path: string
  /**
   * Slug to use for that given URL path.
   *
   * Similar to sibling "route" property, it is a normalized string.
   *
   * For example (continued from `name`):
   * For an URL to go to `/users/abc-123` when running Nuxt, we might want an identifier for that route
   * to [use as a "key"][vue-key].
   *
   * The slug value would be `users-abc-123`
   */
  slug?: string
}

export type Complete<T> = {
  [P in keyof Required<T>]: Pick<T, P> extends Required<Pick<T, P>>
    ? T[P]
    : T[P] | undefined
}

export interface ICompleteRouteDescriptor extends Complete<IRouteDescriptor> {}

/**
 * Extract from a Vue Router Route Object a normalized data object.
 *
 * Assuming we have a route defined as `/request/:id/assets/:asset,
 * matching Nuxt's Page in client/pages/request/_id/assets/_asset/index.vue.
 * And we have a request coming as /request/111/assets/foo
 *
 * Notice that its because Nuxt guesses what Vue Router Route configuration to use
 * because Nuxt will  makes guesses based on path use based on.
 *
 * That is, because Nuxt has convention that file in `pages/request/_id/assets/_asset/index.vue`
 * would provide a $route object with following properties.
 *
 * ```json
 * {
 *   "name": "request-id-assets-asset",
 *   "meta": {},
 *   "path": "/request/111/assets/foo",
 *   "hash": "",
 *   "query": {},
 *   "params": {
 *     "asset": "foo",
 *     "id": "111",
 *   },
 *   "fullPath": "/request/111/assets/foo"
 * }
 * ```
 *
 * Nuxt API can tell us by doing:
 *
 *     const resolved = pickFromNuxtRouterRouteObject($nuxt.$route)
 *
 *
 * @param {Object} $route - a Vue Router Route object
 */
const pickFromNuxtRouterRouteObject = (
  $route: Partial<IRouteDescriptor> = {},
): IRouteDescriptor => {
  const { name = 'index', params = {}, path = '/' } = $route || {}

  return {
    name,
    params,
    path,
  }
}

/**
 * Page logic to bind Vuex views module.
 *
 * e.g. Routes configured as following will be translated as;
 *
 * | Route                   | Slug                   | Name                    |
 * | /request/123            | request-123            | request-id              |
 * | /request/111/assets/foo | request-111-assets-foo | request-id-assets-asset |
 *
 * @param {Object} $route - a Vue Router Route object
 */
export const getNuxtRouterName = (
  $route: Partial<IRouteDescriptor> = {},
): ICompleteRouteDescriptor => {
  const { name, path, params = {} } = pickFromNuxtRouterRouteObject($route)

  const slug = path.replace(/^\//, '').replace(/-/g, '').replace(/\//g, '-')

  return {
    name,
    params,
    path,
    slug,
  }
}
