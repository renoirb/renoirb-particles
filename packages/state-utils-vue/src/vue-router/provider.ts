/**
 * Vue Router's RouteConfig parts specific to routing and Sidebar navigation.
 *
 * See globals.d.ts at the root of the project to see how we're exporting.
 *
 * https://router.vuejs.org/
 * https://github.com/vuejs/vue-router
 */
export interface IVueRouterRoute /* extends RouteConfig */ {
  /**
   * vue-router's path
   */
  path: string

  /**
   * vue-router's name
   */
  name?: string

  /**
   * Hide from the Sidebar if ̀`false`
   */
  hidden?: boolean

  meta?: IVueRouterRouteMeta

  /**
   * Fully-Qualified path to redirect to
   */
  redirect?: string

  /**
   * Sub-Navigation.
   */
  children?: IVueRouterRoute[]
}

export interface IVueRouterRouteMeta {
  /**
   * If set, and is the same as the parent's path property,
   * the sidebar will highlight that particular item.
   */
  activeMenu?: string

  /**
   * If false, the item will hidden in breadcrumb
   * default: true
   */
  breadcrumb?: boolean

  /**
   * Name of icon to use in Sidebar
   */
  icon?: string

  /**
   * The page roles (you can set multiple roles).
   * Not used yet, but could be the place we use to filter off
   * which items to display in a menu.
   */
  roles?: string[]

  /**
   * Text to display in the menu
   */
  title: string
}

export class VueRouterRouteMeta implements IVueRouterRouteMeta {
  activeMenu?: string
  breadcrumb: boolean = true
  icon?: string
  constructor(public readonly title: string, icon?: string) {
    if (icon) {
      this.icon = icon
    }
  }
  /**
   * Based on the parent, what would be the the activeMenu path to use.
   *
   * @param parent so we can pick values from it
   *
   * BEWARE: DO NOT create self-referencing loops. We just pick scalar values we need (i.e. a number, string.)
   * Please, do not affect state
   */
  setActiveMenuFromParent(parent: IVueRouterRoute): void {
    const { path = '/', name = 'index' } = parent
    const nameParts: string[] = name.split('-')
    if (nameParts.length > 1) {
      const lastPart = nameParts[nameParts.length - 1]
      const activeMenu = path
        .replace(lastPart, '')
        .replace(/\/\//g, '/')
        .replace(/\/$/, '')
      this.activeMenu = activeMenu
    }
  }
}

/**
 * Utility class to create a tree of routes in the same shape of
 * vue-router.
 *
 * The objective is to allow us serving dynamically routes that are
 * compatible with vue-router, and prevent changing Nuxt.js
 * and how it handles reading each page's meta data
 *
 * We either had to find a way in Nuxt to read from
 * https://github.com/nuxt/nuxt.js/issues/1687#issuecomment-331870619
 * https://github.com/nuxt/nuxt.js/issues/3751#issuecomment-413818318
 * https://github.com/nuxt/nuxt.js/blob/dev/examples/routes-meta/middleware/theme.js
 */
export class VueRouterRoute implements IVueRouterRoute {
  name: string
  hidden: boolean = false
  meta?: IVueRouterRouteMeta
  children?: IVueRouterRoute[]
  constructor(
    public readonly path: string = '/',
    title?: string,
    icon?: string,
  ) {
    const name = String(path)
      .replace(/^\//, '')
      .replace(/\/$/, '')
      .replace(/\//g, '-')
      .toLowerCase()
    this.name = name === '' ? 'index' : name
    if (title) {
      const meta = new VueRouterRouteMeta(title, icon)
      meta.setActiveMenuFromParent(this)
      this.meta = meta
    }
  }
  /**
   * Factory utility to create a new VueRouterRoute instance as a children.
   */
  addChildren(path: string, title?: string, icon?: string): VueRouterRoute {
    const child = new VueRouterRoute(path, title, icon)
    if (!this.children) {
      this.children = []
    }
    this.children.push(child)

    return child
  }
}
