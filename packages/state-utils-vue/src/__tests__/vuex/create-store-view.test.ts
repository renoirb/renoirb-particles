import { createStoreView } from '../../'

// @ts-ignore
describe('createStoreView', () => {
  test('Factory first argument is a store name, yet is not visible because we will explicitly import it', () => {
    const foo = createStoreView()
    expect(foo).toHaveProperty('state', {
      rowIdCurrent: '',
      rowIdKey: 'id',
      rows: [],
    })
  })

  test('Factory second argument allows to tell which field acts as "id".', () => {
    const subject = createStoreView('Id')
    expect(subject).toHaveProperty('state', {
      rowIdCurrent: '',
      rowIdKey: 'Id',
      rows: [],
    })
  })

  test('We can use ADD_ROW, and REMOVE_ROW without havign to tell the "id" field.', () => {
    const subject = createStoreView('Id')
    // Mimicking how Vuex works.
    const state = subject.state

    const Id = 'abc-123'
    const row = { Id, type: 'Foo', completionState: 100 }

    expect(() => {
      subject.state.rowIdKey = Number.POSITIVE_INFINITY
    }).toThrow(/Cannot assign to read only property 'rowIdKey'/)

    // Mimicking Vuex mutations
    subject.mutations.ADD_ROW(state, row)
    // Validating state
    expect(subject).toHaveProperty('state.rowIdKey', 'Id')
    expect(subject).toHaveProperty('state.rows', [row])
    expect(subject).toHaveProperty('state.rows.0.completionState', 100)
    subject.mutations.REMOVE_ROW(state, Id)
    expect(subject).toMatchSnapshot()
  })

  test('ADD_ROW can effectively merge', () => {
    const subject = createStoreView('Id')
    const state = subject.state

    const rows = []
    rows.push({ Id: 'abc-123', type: 'Foo', completionState: 100 })
    rows.push({ Id: 'dev-456', type: 'Bar', completionState: 20 })
    subject.mutations.SET_ROWS(state, rows)
    expect(subject.state.rows).toMatchSnapshot()

    const updatedRow = {
      Id: 'dev-456',
      completionState: -1,
      CurrentStatusCode: 19,
    }
    subject.mutations.ADD_ROW(state, updatedRow)
    expect(subject.state.rows).toMatchSnapshot()
  })

  test('Factory third argument allows to extend.', () => {
    const options = {
      getters: {
        // @ts-ignore
        findByCompletionState(state, completionState = 100) {
          const matches = state.rows.filter(
            // @ts-ignore
            (row) => row.completionState === completionState,
          )
          return matches
        },
      },
    }

    const subject = createStoreView('Id', options)
    const state = subject.state

    const rows = []
    rows.push({
      Id: 'abc-123',
      type: 'Foo',
      completionState: 100,
      CurrentStatusCode: 19,
    })
    rows.push({
      Id: 'dev-456',
      type: 'Bar',
      completionState: -1,
      CurrentStatusCode: 17,
    })
    // Mimicking Vuex mutations. Also testing SET_ROWS mutation.
    subject.mutations.SET_ROWS(state, rows)
    expect(subject).toHaveProperty('state.rows', rows)

    let matches = subject.getters.findByCompletionState(state, 100)
    expect(matches).toMatchObject([
      {
        completionState: 100,
        CurrentStatusCode: 19,
        Id: 'abc-123',
        type: 'Foo',
      },
    ])
    expect(matches).toMatchSnapshot()

    matches = subject.getters.findByCompletionState(
      state,
      Number.NEGATIVE_INFINITY,
    )
    expect(matches).toMatchObject([])
  })

  test('hasRow getter', () => {
    const subject = createStoreView('Id')
    const state = subject.state

    const rows = []
    rows.push({ Id: 'abc-123', type: 'Foo', completionState: 100 })
    rows.push({ Id: 'dev-456', type: 'Bar', completionState: -1 })
    subject.mutations.SET_ROWS(state, rows)

    expect(subject.getters.hasRow(state)('abc-123')).toBe(true)
    expect(subject.getters.hasRow(state)(2)).toBe(false)
  })

  test('currentRow getter', () => {
    const subject = createStoreView('MyDataStoreIdKey')
    const state = subject.state
    const MyDataStoreIdKey = 'abc-123'

    const rows = []
    rows.push({ MyDataStoreIdKey, type: 'Foo', count: 100 })
    rows.push({ MyDataStoreIdKey: 'dev-456', type: 'Bar', count: 0 })
    subject.mutations.SET_ROWS(state, rows)
    subject.mutations.SET_CURRENT_ROW(state, MyDataStoreIdKey)
    expect(subject.getters.currentRow(state)).toHaveProperty(
      'MyDataStoreIdKey',
      MyDataStoreIdKey,
    )
    expect(subject).toHaveProperty('state.rowIdCurrent', MyDataStoreIdKey)
    expect(subject).toMatchSnapshot()

    // Ensuring that REMOVE_ROW also cleans up rowIdCurrent too.
    subject.mutations.REMOVE_ROW(state, MyDataStoreIdKey)
    expect(subject).toHaveProperty('state.rowIdCurrent', '')
  })
})
