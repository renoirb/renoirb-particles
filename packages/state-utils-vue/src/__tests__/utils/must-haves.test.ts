import { mustHaves } from '../../'

describe('mustHaves', () => {
  test('When non-existing members', () => {
    const subject = mustHaves(['firstName', 'lastName', 'age'])
    expect(
      subject({
        gender: 'm',
        0: [2, 3, 4],
      }),
    ).toBe(false)
    expect(
      subject({
        gender: () => Number.NaN,
        [Symbol(3)]: [1, 2, 3],
      }),
    ).toBe(false)
  })
  test('When some are missing', () => {
    const subject = mustHaves(['firstName', 'lastName', 'age'])
    expect(
      subject({
        firstname: 'Multi', // notice, Case MaTteRs!
        lastName: 'Vac',
      }),
    ).toBe(false)
  })
})
