/* tslint:disable:object-literal-sort-keys */

import { getNuxtRouterName } from '../../'

test('getNuxtRouterName', () => {
  const fixture = {
    name: 'user-id-friend-fid',
    meta: {},
    path: '/user/24532/friend/36723',
    hash: '',
    query: {},
    params: {
      fid: '36723',
      id: '24532',
    },
    fullPath: '/user/24532/friend/36723',
  }
  const subject = getNuxtRouterName(fixture)
  expect(subject).toMatchObject({
    name: 'user-id-friend-fid',
    path: '/user/24532/friend/36723',
    slug: 'user-24532-friend-36723',
    params: {
      fid: '36723',
      id: '24532',
    },
  })
})
