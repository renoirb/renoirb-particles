/* eslint-env jest */

import { DateTimeVariants, LocalizableMixin } from '../../'

describe('l10n/date-time-variants', () => {
  test('Typical use', () => {
    const defaultVariant = 'json'
    expect(DateTimeVariants).toBeDefined()
    const dv = new DateTimeVariants(defaultVariant)
    expect(dv).toMatchSnapshot()
    expect(dv.defaultVariant).toBe(defaultVariant)
  })
})

describe('mixins/localizable', () => {
  test('accessibility', () => {
    expect(LocalizableMixin).toBeDefined()
    expect(Object.keys(LocalizableMixin)).toMatchSnapshot()
  })
})
