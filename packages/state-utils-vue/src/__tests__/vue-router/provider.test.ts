import { VueRouterRoute } from '../..'

describe('VueRouterRoute', () => {
  test('constructor', () => {
    const subject = new VueRouterRoute('/', 'Home', 'smile')
    const request = subject.addChildren('/request', 'lbl_Request')
    request.addChildren('/request/outstanding', 'Must watch request patterns')
    request.addChildren('/request/detail', 'detail')
    expect(subject).toMatchSnapshot()
  })
})
