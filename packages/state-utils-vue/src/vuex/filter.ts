import { ActionTree, MutationTree, ActionContext } from 'vuex'

export interface IStoreStateRoot {}

export interface IFilterDefinition {
  /**
   * @alias viewContextNamespace
   */
  readonly namespace: string
  readonly filter: string
}

export interface IStoreStateFilter {
  filters: IFilterDefinition[]
}

export const types = {
  FILTER: 'SET_FILTER',
}

export const namespaced = true

export const name = 'filter'

/**
 * https://github.com/cheeaun/node-hnapi/wiki/API-Documentation
 */
export const state = (): IStoreStateFilter => {
  const out: IStoreStateFilter = {
    filters: [
      /* {namespace: 'foo', filter: 'Foo_$eq_$'} */
    ],
  }

  return out
}

export const actions: ActionTree<IStoreStateFilter, IStoreStateRoot> = {
  change(
    actionContext: ActionContext<IStoreStateFilter, IStoreStateRoot>,
    definition: IFilterDefinition,
  ) {
    const { namespace = '', filter = '' } = definition
    const isSame = actionContext.getters.equals(namespace)(filter)
    if (!isSame) {
      actionContext.commit(types.FILTER, definition)
    }
  },
}

/**
 * UNFINISHED
 * https://egghead.io/lessons/vue-js-access-state-by-using-vuex-getter-functions-with-typescript
 */
export const getters: any = {
  // @ts-ignore
  has(getterContextState: any) {
    return (viewContextNamespace: string) => {
      const filtered = getterContextState.filters.filter(
        (i: IFilterDefinition) => i.namespace === viewContextNamespace,
      )
      return filtered.length === 1
    }
  },
  // @ts-ignore
  for(getterContextState: any) {
    return (viewContextNamespace: string) => {
      const hasFilter = getterContextState.getters.has(viewContextNamespace)
      let out = ''
      if (hasFilter) {
        const filtered = getterContextState.filters.filter(
          (i: IFilterDefinition) => i.namespace === viewContextNamespace,
        )
        out = filtered[0].filter
      }
      return out
    }
  },
  // @ts-ignore
  equals(getterContextState) {
    return (viewContextNamespace: string) => (q: string) => {
      const hasFilter = getterContextState.getters.has(viewContextNamespace)
      let currentFilter = ''
      if (hasFilter) {
        const filtered = getterContextState.filters.filter(
          (i: IFilterDefinition) => i.namespace === viewContextNamespace,
        )
        currentFilter = filtered[0].filter
      }
      return currentFilter === q
    }
  },
}

export const mutations: MutationTree<IStoreStateFilter> = {
  [types.FILTER](
    // @ts-ignore
    mutationStateContext: any,
    payload: IFilterDefinition,
  ) {
    const { namespace = '', filter = '' } = payload
    if (namespace === '') {
      return
    }
    const filterIndex = mutationStateContext.filters.findIndex(
      (i: IFilterDefinition) => i.namespace == namespace,
    )
    if (filterIndex >= 0) {
      mutationStateContext.filters[filterIndex] = {
        ...mutationStateContext.filters[filterIndex],
        namespace,
        filter,
      }
    } else {
      mutationStateContext.filters.push({
        namespace,
        filter,
      })
    }
  },
}
