// https://github.com/ElemeFE/element/blob/v2.4.11/src/utils/util.js
// @ts-ignore
const arrayFindIndex = (arr, predicate) => {
  if (typeof predicate !== 'function') {
    const message = `
            Second argument MUST BE a predicate type compounded function.
            First time we call the predicate, we declare which field we want to filter on,
            and it return another function that will do the comparison for each item of the collection
            based on the field previously declared.
          `
      .replace(/[\n\s]+/g, ' ')
      .trim()
    throw new Error(message)
  }
  for (let i = 0; i !== arr.length; ++i) {
    if (predicate(arr[i])) {
      return i
    }
  }
  return -1
}

export const createStoreViewHelpers = {
  arrayFindIndex,
}

/**
 * Assuming a Vuex store has only one collection of items
 * that we want to visualize only one of them "currencly selected"
 * and we want to add,remove,update from the collection.
 *
 * But we're never sure what would be the primary key.
 *
 * @param primaryKey
 * @param options
 */
export const createStoreView = (primaryKey = 'id', options = {}) => {
  // tslint:disable-next-line:no-parameter-reassignment
  options = options || {}
  // @ts-ignore
  const actions = options.actions || {}
  // @ts-ignore
  const gettersRest = options.getters || {}
  // @ts-ignore
  const mutationsRest = options.mutations || {}
  // @ts-ignore
  const stateRest = options.state || {}

  // @ts-ignore
  const findByPrimaryKeyPredicate = (id) => (item) => item[primaryKey] === id

  const store = {
    namespaced: true,
    // tslint:disable-next-line:object-literal-sort-keys
    actions: {
      // @ts-ignore
      // tslint:disable-next-line:typedef
      async setSelectedRow({ commit, getters, dispatch }, id = '') {
        if (String(id).length < 1) {
          const message = `Invalid argument ${id}`
          throw new Error(message)
        }
        const hasRow = getters.hasRow(id)
        if (!hasRow) {
          await dispatch('hydrateRow', id)
        }
        // eslint-disable-next-line
        // console.log('store/request actions setSelectedRow', { id, hasRow })
        commit('SET_CURRENT_ROW', id)
      },
      ...actions,
    },
    getters: {
      // @ts-ignore
      // tslint:disable-next-line:typedef
      currentRow(state) {
        const id = state.rowIdCurrent
        const idx = arrayFindIndex(state.rows, findByPrimaryKeyPredicate(id))
        const hasIndex = idx >= 0
        if (hasIndex) {
          return state.rows[idx]
        }

        return {}
      },
      // @ts-ignore
      // tslint:disable-next-line:typedef
      hasRow(state) {
        const rows = state.rows
        // @ts-ignore
        return (id) => {
          const idx = arrayFindIndex(rows, findByPrimaryKeyPredicate(id))
          const hasIndex = idx >= 0
          return hasIndex
        }
      },
      ...gettersRest,
    },
    mutations: {
      // @ts-ignore
      // tslint:disable-next-line:typedef
      SET_ROWS(state, rows = []) {
        state.rows = rows
      },
      // @ts-ignore
      // tslint:disable-next-line:typedef
      SET_CURRENT_ROW(state, id = null) {
        if (id === null) {
          state.rowIdCurrent = ''
          return
        }
        const idx = arrayFindIndex(state.rows, findByPrimaryKeyPredicate(id))
        const hasIndex = idx >= 0
        if (hasIndex) {
          state.rowIdCurrent = id
        }
      },
      // @ts-ignore
      // tslint:disable-next-line:typedef
      ADD_ROW(state, row) {
        const id = row[primaryKey]
        const idx = arrayFindIndex(state.rows, findByPrimaryKeyPredicate(id))
        const hasIndex = idx >= 0
        if (!hasIndex) {
          state.rows.push(row)
        } else {
          // https://vuex.vuejs.org/guide/mutations.html#mutations-follow-vue-s-reactivity-rules
          // Maybe-issue: If row has a non scalar value (i.e. array, object), we might have an issue. FIXME, maybe? @todo
          const currentRow = state.rows[idx]
          state.rows[idx] = { ...currentRow, ...row }
        }
      },
      // @ts-ignore
      // tslint:disable-next-line:typedef
      REMOVE_ROW(state, id) {
        const idx = arrayFindIndex(state.rows, findByPrimaryKeyPredicate(id))
        const hasIndex = idx >= 0
        const isCurrent = id === state.rowIdCurrent
        if (isCurrent) {
          state.rowIdCurrent = ''
        }
        if (hasIndex) {
          state.rows.splice(idx, 1)
        }
      },
      ...mutationsRest,
    },
    state: {
      rowIdKey: primaryKey,
      rowIdCurrent: '',
      rows: [],
      ...stateRest,
    },
  }

  // Make sure rowIdKey becomes read-only
  Object.defineProperty(store.state, 'rowIdKey', {
    writable: false,
    value: primaryKey,
  })

  return store
}
