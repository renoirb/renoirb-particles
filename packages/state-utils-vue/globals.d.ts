/// <references path="dist/index.d.ts" />
/// <reference types="vue-router" />

/**
 * An "ambient" types descriptor.
 *
 * Import typings from another module so we're not forced to require
 *
 * UNFINISHED.
 *
 * https://stackoverflow.com/a/51114250
 * https://www.credera.com/blog/technology-solutions/typescript-adding-custom-type-definitions-for-existing-libraries/
 * https://basarat.gitbooks.io/typescript/docs/types/ambient/d.ts.html
 * https://github.com/microsoft/TypeScript/issues/2568
 */
declare module '@renoirb/state-utils-vue' {
  import { RouteConfig, Route } from 'vue-router'
  export interface IVueRouterRoute extends RouteConfig {}
  export interface IRouteDescriptor extends Route {}
  export function pickFromNuxtRouterRouteObject(
    $route: Partial<RouteConfig>,
  ): IRouteDescriptor
}
