const base = require('@renoirb/conventions-use-eslint')

module.exports = {
  ...base,
  rules: {
    ...base.rules,
    // See comments in @renoirb/tools-bundling-helpers .eslintrc.js
    '@typescript-eslint/typedef': 'off',
    '@typescript-eslint/ban-ts-ignore': 'off',
    '@typescript-eslint/explicit-function-return-type': 'off',
    '@typescript-eslint/no-empty-interface': 'off',
    '@typescript-eslint/no-explicit-any': 'off',
    '@typescript-eslint/no-inferrable-types': 'off',
    '@typescript-eslint/triple-slash-reference': 'off',
    'prefer-rest-params': 'off',
  },
}
