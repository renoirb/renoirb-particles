[repo-url]: https://gitlab.com/renoirb/renoirb-particles/-/tree/v1.x/packages/state-utils-vue
[npmjs-badge]: https://img.shields.io/npm/v/%40renoirb%2Fstate-utils-vue?style=flat-square&logo=appveyor&label=npm&logo=npm 'NPMjs.com Package Badge'
[npmjs-url]: https://www.npmjs.com/package/%40renoirb%2Fstate-utils-vue
[size-badge]: https://img.shields.io/bundlephobia/min/%40renoirb%2Fstate-utils-vue?style=flat-square
[deps-status]: https://img.shields.io/librariesio/release/npm/%40renoirb%2Fstate-utils-vue?style=flat-square&logo=appveyor&logo=dependabot

# [**@renoirb/state-utils-vue**][repo-url]

Vue.js utilities, (Vuex) store factory, et al.

| Version                          | Size                           | Dependencies                                                      |
| -------------------------------- | ------------------------------ | ----------------------------------------------------------------- |
| [![npm][npmjs-badge]][npmjs-url] | ![npm bundle size][size-badge] | ![Libraries.io dependency status for latest release][deps-status] |
