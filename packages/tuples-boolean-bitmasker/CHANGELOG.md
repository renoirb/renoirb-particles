# Change Log - tuples-boolean-bitmasker

This log was last generated on Tue, 24 Nov 2020 09:11:31 GMT and should not be manually modified.

## 1.0.2
Tue, 24 Nov 2020 09:11:31 GMT

### Patches

- Node.js exports must start by dot slash

## 1.0.1
Mon, 23 Nov 2020 07:47:01 GMT

### Patches

- Upgrade @types/node

## 1.0.0
Wed, 15 Jul 2020 02:59:19 GMT

### Breaking changes

- Packaging and bundling normalization and deps update

