/* eslint-disable no-bitwise */

/**
 * Bit Wise Operator
 *
 * - '&' AND: Stands for "logical AND". It's an operation that returns 1 if both operands are true
 * - '|' OR: If either the left side, or the right side has a bit, then at least one of both sides has a match
 * - '^' XOR: If we want to determine if ONLY ONE side has a bit
 */
export type BitWiseOperator = '&' | '|' | '^'

export interface IBitMasker {
  readonly blocks: ReadonlyArray<boolean>

  /**
   * Check if current IBitMasker is included into subject.
   * It uses '&' AND Bitwise operator and checks if current IBitMasker is still present.
   */
  includes(subject: IBitMasker | number): boolean

  /**
   * What Bitwise operation to make and return a number that is the result of the operation.
   *
   * @param {(IBitMasker|number)} subject — Either number or IBitMasker to work with
   * @param {('&'|'|'|'^')} operator — What Bitwise operation to make
   */
  operate(subject: IBitMasker | number, operator: BitWiseOperator): number

  toNumber(): number

  toString(): string

  toJSON(): boolean[]
}

/**
 * Bit Mask.
 *
 * Provided we have a known list of boolean values in an array,
 * we can use them as a filtering mechanism.
 *
 * It's another way of using a binary representation, and
 * see if they match.
 *
 * @public
 * @author Renoir Boulanger <contribs@renoirboulanger.com>
 */
export class BitMasker implements IBitMasker {
  readonly blocks: ReadonlyArray<boolean> = []

  constructor(blocks: boolean[], limiter?: IBitMasker) {
    if (!Array.isArray(blocks)) {
      const message = `
        Invalid argument, input must be an array of booleans
      `
        .replace(/[\n\s]+/g, ' ')
        .trim()
      throw new Error(message)
    }
    const filtered = blocks.map((i) => typeof i === 'boolean').filter(Boolean)
    if (filtered.includes(false) || filtered.length !== blocks.length) {
      const message = `
        Invalid argument, input must be an array of booleans
        and some members seem to be of invalid type or with empty items
      `
        .replace(/[\n\s]+/g, ' ')
        .trim()
      throw new Error(message)
    }
    this.blocks = Object.freeze([...blocks])
    if (limiter) {
      const limiterMask = limiter.toNumber()
      const thisNumber = this.toNumber()
      const test = limiterMask & thisNumber
      // If Is not within the mask
      if (test < 1) {
        const lms0b = limiterMask.toString(2)
        const l0b = thisNumber.toString(2)
        const l0bsum = test.toString(2)
        const message = `
          Invalid argument, BitMasker range must be within range.
          Current mask is "0b${lms0b} & 0b${l0b}" is "0b${l0bsum}" and should be greater than 0b0.
        `
          .replace(/[\n\s]+/g, ' ')
          .trim()
        throw new Error(message)
      }
    }
    Object.freeze(this)
  }

  includes(subject: IBitMasker | number): boolean {
    const mask = this._toNumberMask(subject)
    const comparison = this.operate(subject, '&')
    return mask === comparison
  }

  operate(
    subject: IBitMasker | number,
    operator: BitWiseOperator = '&',
  ): number {
    const mask = this._toNumberMask(subject)
    const local = this.toNumber()
    // We dynamically coerce both numbers and compare them using bitwise operators
    // and see if the comparsion yields a number that is higher than 0
    // When 0 means the mask has not found it.
    switch (operator) {
      case '&':
        return local & mask
        break

      case '|':
        return local | mask
        break

      case '^':
        return local ^ mask
        break

      default:
        const _exhaustiveSwitchCase: never = operator
        return _exhaustiveSwitchCase
        break
    }
  }

  private _toNumberMask(subject: IBitMasker | number): number {
    let mask: number
    if (typeof subject === 'number') {
      mask = subject
    } else if ('toNumber' in subject && 'blocks' in subject) {
      mask = subject.toNumber()
    } else {
      const message = `
        Invalid argument, input must either be a number, or a BitMasker object
      `
        .replace(/[\n\s]+/g, ' ')
        .trim()
      throw new Error(message)
    }
    return mask
  }

  toNumber(): number {
    return parseInt(this.toString(), 2)
  }

  toString(): string {
    const stringified: string = this.blocks
      .map((i) => (i === true ? '1' : '0'))
      .join('')
    return stringified
  }

  static fromString(input: string, limiter?: IBitMasker): IBitMasker {
    const blocks = input.split('')
    const sanity = blocks.map((i) => ['0', '1'].includes(i))
    if (sanity.includes(false)) {
      const message = `
        Invalid argument, fromNumber only accepts strings of 1 and 0's
        we received "${input}"
      `
        .replace(/[\n\s]+/g, ' ')
        .trim()
      throw new Error(message)
    }
    const args = blocks.map((i) => (i === '1' ? true : false))
    return new this(args, limiter)
  }

  static fromNumber(input: number | string, limiter?: IBitMasker): IBitMasker {
    const inputTypeCastedAsInt = +input
    if (Number.isNaN(inputTypeCastedAsInt)) {
      const message = `
        Invalid argument, input is not a number,
        or a string with a number "${input}"
      `
        .replace(/[\n\s]+/g, ' ')
        .trim()
      throw new Error(message)
    }
    const stringified: string = inputTypeCastedAsInt.toString(2)
    const blocks: boolean[] = stringified.split('').map((x) => x === '1')
    // OR;
    // (inputTypeCastedAsInt >>> 0).toString(2)

    return new this(blocks, limiter)
  }

  [Symbol.toPrimitive](hint: 'default'): string
  [Symbol.toPrimitive](hint: 'string'): string
  [Symbol.toPrimitive](hint: 'number'): number

  /**
   * Converts a Date object to a string or number.
   *
   * Notes: This is ECMAScript's "well known" protocol.
   * See https://exploringjs.com/deep-js/ch_type-coercion.html#example-coercion-algorithms
   *
   * @param hint The strings "number", "string", or "default" to specify what primitive to return.
   *
   * @returns A number if 'hint' was "number", a string if 'hint' was "string" or "default".
   */
  [Symbol.toPrimitive](hint: string): string | number {
    if (/number/.test(hint)) {
      return this.toNumber()
    } else {
      return this.toString()
    }
  }

  get [Symbol.toStringTag](): string {
    const symbol = this.toString()
    return `BitMasker(${symbol})`
  }

  toJSON(): boolean[] {
    return [...this.blocks]
  }
}
