/* eslint-disable no-bitwise */
/* eslint-disable @rushstack/no-null */

import { BitMasker } from '../bit-mask'

describe('BitMasker', () => {
  test('constructor argument only accepts array of booleans', () => {
    // @ts-ignore
    expect(() => new BitMasker([1, 2, 3])).toThrow()
    // @ts-ignore
    expect(() => new BitMasker(['1', '2', '3'])).toThrow()
    // @ts-ignore
    expect(() => new BitMasker([null, null, null])).toThrow()
    /* eslint-disable no-sparse-arrays */
    // @ts-ignore
    expect(() => new BitMasker([, ,])).toThrow()
    // @ts-ignore
    expect(() => new BitMasker()).toThrow()
    // @ts-ignore
    expect(() => new BitMasker('?')).toThrow()
    // @ts-ignore
    expect(() => new BitMasker(1)).toThrow()
  })

  /**
   * Bit masking basics sample.
   *
   * How can we dynamically convert an array of binaries
   * into a data structure to check if flags applies using
   * bit masking techniques.
   *
   * Imagine we have 3 features flags (can be many more, same pattern can apply):
   * - WITH_FLAG_A (0b001)
   * - WITH_FLAG_B (0b010)
   * - WITH_FLAG_C (0b100)
   *
   * Assuming we have a configuration that could tell us what applies:
   *
   * const enabledFlags = WITH_FLAG_B | WITH_FLAG_C; // (0b110)
   */
  const WITH_FLAG_A = 0b001 // Binary representation
  const WITH_FLAG_A_DEC = 1 // Decimal representation
  // const WITH_FLAG_A_ARRAY = [false, false, true] // Array of boolean representation

  const WITH_FLAG_B = 0b010
  const WITH_FLAG_B_DEC = 2
  // const WITH_FLAG_B_ARRAY = [false, true, false]

  const WITH_FLAG_C = 0b100
  const WITH_FLAG_C_DEC = 4
  // const WITH_FLAG_C_ARRAY = [true, false, false]

  test('Fixture sanity check', () => {
    // Making sure that flags are equivalents o the same numbers
    // Because the purpose of BitMasker is to allow converting array
    // of booleans into them
    expect(WITH_FLAG_A).toBe(WITH_FLAG_A_DEC)
    expect(WITH_FLAG_B).toBe(WITH_FLAG_B_DEC)
    expect(WITH_FLAG_C).toBe(WITH_FLAG_C_DEC)
  })

  test('Usage surface sanity check', () => {
    // WITH_FLAG      A     B     C
    const AS_TUPLE = [true, true, false]
    const AS_BITMASK = WITH_FLAG_B | WITH_FLAG_C
    const subject = new BitMasker([...AS_TUPLE])
    expect(subject).toMatchSnapshot()
    expect(subject.toNumber()).toBe(AS_BITMASK)
    expect(subject.toString()).toBe('110')
    expect(subject.toJSON()).toMatchObject([...AS_TUPLE])
    expect(JSON.parse(JSON.stringify(subject))).toMatchObject([...AS_TUPLE])
    // well-known ECMAScript 2015 Symbol.toStringTag
    expect(subject[Symbol.toStringTag]).toBe('BitMasker(110)')
  })

  test('fromNumber', () => {
    const subject = BitMasker.fromNumber(WITH_FLAG_B | WITH_FLAG_C)
    expect(subject.toString()).toBe('110')
    expect(subject.toNumber()).toBe(WITH_FLAG_B | WITH_FLAG_C)
  })

  test('fromString', () => {
    const subject = BitMasker.fromString('101')
    expect(subject.toString()).toBe('101')
    expect(subject.toNumber()).toBe(WITH_FLAG_A | WITH_FLAG_C)
  })

  test('includes', () => {
    const subject = BitMasker.fromString('101')
    expect(subject.toString()).toBe('101')
    expect(subject.toNumber()).toBe(WITH_FLAG_A | WITH_FLAG_C)

    // Comparsion mechanics
    const mask = subject.toNumber()
    // & operator tells the decimal value or 0
    // We have WITH_FLAG_A
    expect(mask & WITH_FLAG_A).toBe(1)
    // We have WITH_FLAG_C
    expect(mask & WITH_FLAG_C).toBe(4)
    expect(subject.includes(WITH_FLAG_C)).toBe(true)

    // Bit Mask with only WITH_FLAG_C
    const anotherMask = new BitMasker([false, false, true])
    expect(subject.includes(anotherMask)).toBe(true)

    // But, since mask DO NOT have WITH_FLAG_B
    expect(mask & WITH_FLAG_B).toBe(0)
    expect(subject.includes(WITH_FLAG_B)).toBe(false)
  })

  test('operate OR', () => {
    const ALPHA = '11010000' // 208
    const BRAVO = '10101010'
    const ALPHA_AND_BRAVO = '11111010'
    // (0b11010000 | 0b10101010 >>> 0).toString(2)
    // '11111010'
    const alpha = BitMasker.fromString(ALPHA)
    expect(alpha.toString()).toBe(ALPHA)
    expect(alpha.toNumber()).toBe(208)
    expect(alpha.toJSON()).toMatchObject([
      true,
      true,
      false,
      true,
      false,
      false,
      false,
      false,
    ])
    const bravo = BitMasker.fromString(BRAVO)
    expect(bravo.toString()).toBe(BRAVO)
    const subject = alpha.operate(bravo, '|').toString(2)
    expect(subject).toBe(ALPHA_AND_BRAVO)
  })

  test('limiter', () => {
    const limiter = BitMasker.fromString('111')
    const subject = BitMasker.fromString('100', limiter)
    expect(subject.toString()).toBe('100')
    // @ts-ignore
    expect(() => {
      BitMasker.fromString('1000', limiter)
    }).toThrow(/Invalid argument, BitMasker range must be within range/)
  })

  test('README: Example 1', () => {
    const capabilityFoo = BitMasker.fromString('100') // 0b100
    const userCapabilities = new BitMasker([true, true, false]) // 0b110

    // Manually, we'd have to use bitwise operator &
    // Non zero means there is a collision.
    expect(0b100 & 0b110).toBe(4) // 4
    expect(userCapabilities.includes(capabilityFoo)).toBe(true) // true
  })
})
