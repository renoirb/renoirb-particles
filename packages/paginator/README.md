[repo-url]: https://gitlab.com/renoirb/renoirb-particles/-/tree/v1.x/packages/paginator
[npmjs-badge]: https://img.shields.io/npm/v/%40renoirb%2Fpaginator?style=flat-square&logo=appveyor&label=npm&logo=npm 'NPMjs.com Package Badge'
[npmjs-url]: https://www.npmjs.com/package/%40renoirb%2Fpaginator
[size-badge]: https://img.shields.io/bundlephobia/min/%40renoirb%2Fpaginator?style=flat-square
[deps-status]: https://img.shields.io/librariesio/release/npm/%40renoirb%2Fpaginator?style=flat-square&logo=appveyor&logo=dependabot

# [**@renoirb/paginator**][repo-url]

For a large collection of objects, break down into N items long, and allow me to retrieve a copy of only items (or the applicable indices).

| Version                          | Size                           | Dependencies                                                      |
| -------------------------------- | ------------------------------ | ----------------------------------------------------------------- |
| [![npm][npmjs-badge]][npmjs-url] | ![npm bundle size][size-badge] | ![Libraries.io dependency status for latest release][deps-status] |
