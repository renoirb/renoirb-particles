import type {
  IPaginatorError,
  IPaginatorFn,
  IBasePaginatorState,
  IPaginatorState,
} from './types'
import { toPaginatorError, toPaginatorIndices, toPaginatorItems } from './utils'

/**
 * Search result paginator factory.
 *
 * https://www.typescriptlang.org/docs/handbook/generics.html
 * https://mariusschulz.com/blog/typing-functions-in-typescript#function-type-literals
 * https://vincent.billey.me/pure-javascript-immutable-array/
 * https://dev.to/gcanti/getting-started-with-fp-ts-either-vs-validation-5eja
 * https://medium.com/@dhruvrajvanshi/making-exceptions-type-safe-in-typescript-c4d200ee78e9
 *
 * rows: the array of elements to be paginated
 * pageSize:  the maximum size of each individual page (last page may be smaller)
 *
 * @author Renoir Boulanger <contribs@renoirboulanger.com>
 */
export const paginatorFactory = <T>(
  rows: T[],
  max: number = 10,
): IPaginatorFn<T> => {
  const items: T[] = []
  let pageSize: number
  let pages = 0
  let total = 0
  let maximum = 0
  if (!Array.isArray(rows)) {
    throw new Error('The provided array was invalid')
  }
  if (rows.length < 1) {
    throw new Error('The provided array was empty')
  }
  if (!Number.isInteger(max)) {
    throw new Error('Max pages argument MUST be an integer')
  }
  if (max < 1) {
    pageSize = 10
  } else {
    pageSize = max
  }
  items.push(...rows)
  total = rows.length
  maximum = pageSize > total ? total : pageSize
  pages = Math.ceil(total / maximum)
  return function paginator(
    page: number = 1,
    indices: boolean = false,
  ): IPaginatorState<T> | IPaginatorError {
    let error: string | false = false
    if (!Number.isInteger(page)) {
      throw new Error('page argument MUST be an integer')
    }
    const state: IBasePaginatorState = {
      page,
      pageSize,
      pages,
      total,
    }
    if (pageSize < 1) {
      error = 'The page size provided was less than one'
    }
    if (page > pages) {
      error = `The page number provided (${page}) is larger than the maximum number of pages (${pages})`
    }
    if (page < 1) {
      error = `The page number provided (${page}) cannot be under 1`
    }
    if (error) {
      return toPaginatorError(state, error)
    }
    const startIndex = (page - 1) * maximum
    const endIndex = startIndex + maximum
    if (indices === true) {
      const all = Array.from({ length: total }, (_, i) => i)
      const rows = all.slice(startIndex, endIndex)
      return toPaginatorIndices(state, rows)
    } else {
      const rows = [...items.slice(startIndex, endIndex)]
      return toPaginatorItems(state, rows)
    }
  }
}
