/* eslint-env jest */

import Mock from 'mockjs'
import { paginatorFactory } from '../factory'
import type { IPaginatorItems } from '../types'

/**
 * An hypotethical User entity
 */
export interface User {
  id: number
  name?: string
}

const data = Mock.mock({
  'users|15': [
    {
      'id|+1': 0,
      name: '@NAME',
    },
  ],
}) as { users: User[] }

describe('Factory', () => {
  let u: User[]
  let page: number

  beforeEach(() => {
    /* tslint:disable-next-line:no-shadowed-variable */
    u = data.users.map((u) => ({ ...u }))
  })

  it('Supports typical use-case', () => {
    // users fixture has 15 elements
    // should return the first page (of 3)
    // page increment starts at 1
    page = 1
    const max: number = 5
    const paginator = paginatorFactory<User>(u, max)
    const currentPage = paginator(page) as IPaginatorItems<User>
    expect(currentPage).toHaveProperty('page', page)
    const { items = [] } = currentPage as IPaginatorItems<User>
    expect(items.map((e) => e.id)).toMatchObject([0, 1, 2, 3, 4])

    const total = u.length
    const pages = total / max
    expect(currentPage).toHaveProperty('items')
    expect(currentPage).toHaveProperty('pages', pages)
    expect(currentPage).toHaveProperty('total', total)

    expect(currentPage.items.length).toEqual(5)
    expect(currentPage.items[0].id).toEqual(0)
    expect(currentPage.items[0].name).toEqual(data.users[0].name)
    expect(currentPage.items[4].id).toEqual(4)
    expect(currentPage.items[4].name).toEqual(data.users[4].name)

    expect(currentPage.page).toEqual(1)
    expect(currentPage.pages).toEqual(3)
    expect(currentPage.total).toEqual(15)

    const pageTwo = paginator(2) as IPaginatorItems<User>

    expect(pageTwo.items[0].id).toEqual(5)
    expect(pageTwo.items[0].name).toEqual(data.users[5].name)
    expect(pageTwo.items[1].id).toEqual(6)
    expect(pageTwo.items[1].name).toEqual(data.users[6].name)

    expect(pageTwo).toHaveProperty('page', 2)
    expect(pageTwo).toHaveProperty('pages', pages)
    expect(pageTwo).toHaveProperty('total', total)
    expect(pageTwo.items.map((e) => e.id)).toMatchObject([5, 6, 7, 8, 9])

    const [sixth] = pageTwo.items
    expect(sixth).toMatchObject(u[5])
    expect(sixth).toMatchObject({ id: 5 })
  })

  it('Should not have an error property when no errors', () => {
    page = 1
    const max: number = 5
    const paginator = paginatorFactory<User>(u, max)
    const currentPage = paginator(page) as IPaginatorItems<User>
    expect(currentPage).not.toHaveProperty('error')
  })

  it('Should have an error property and an empty items array when errors', () => {
    page = 111
    const max: number = 5
    const paginator = paginatorFactory<User>(u, max)
    const currentPage = paginator(page)
    expect(currentPage).toHaveProperty('error')
    expect(currentPage).toHaveProperty('items', [])
  })

  it('Is possible to get the first page by asking index 1', () => {
    page = 1
    const max: number = 5
    const paginator = paginatorFactory<User>(u, max)
    const currentPage = paginator(page) as IPaginatorItems<User>
    expect(currentPage.items[0].id).toEqual(0)
    expect(currentPage.items[0].name).toEqual(data.users[0].name)
  })

  it('Should return an error on invalid page, when 0', () => {
    page = 0
    const max: number = 5
    const paginator = paginatorFactory<User>(u, max)
    const currentPage = paginator(page) as IPaginatorItems<User>

    expect(currentPage.items).toMatchObject([])
    expect(currentPage).toHaveProperty(
      'error',
      'The page number provided (0) cannot be under 1',
    )
  })

  it('Should return an error on invalid page, when -1', () => {
    page = -1
    const max: number = 5
    const paginator = paginatorFactory<User>(u, max)
    const currentPage = paginator(page)
    expect(currentPage).toHaveProperty(
      'error',
      'The page number provided (-1) cannot be under 1',
    )
  })

  it('Should default to page=1 on lack of input', () => {
    const max: number = 5
    const paginator = paginatorFactory<User>(u, max)
    // @ts-ignore
    const currentPage = paginator() as IPaginatorItems<User>

    expect(currentPage.items.length).toEqual(5)
    expect(currentPage.items[0].id).toEqual(0)
    expect(currentPage.items[4].id).toEqual(4)

    expect(currentPage.page).toEqual(1)
    expect(currentPage.pages).toEqual(3)
    expect(currentPage.total).toEqual(15)
  })

  it('Should contain the full collection when the maximum is the same length as the collection', () => {
    page = 1
    const max: number = 15
    const paginator = paginatorFactory<User>(u, max)
    const currentPage = paginator(page) as IPaginatorItems<User>

    expect(currentPage.items.length).toEqual(u.length)
    expect(currentPage.items[0].id).toEqual(0)
    expect(currentPage.items[14].id).toEqual(14)

    expect(currentPage.page).toEqual(1)
    expect(currentPage.pages).toEqual(1)
    expect(currentPage.total).toEqual(15)
  })

  it('Should contain only remaining elements on the last page when max does not divide evenly into total', () => {
    page = 2
    const max: number = 12
    const paginator = paginatorFactory<User>(u, max)
    const currentPage = paginator(page) as IPaginatorItems<User>

    expect(currentPage.items.length).toEqual(3)
    expect(currentPage.items[0].id).toEqual(12)
    expect(currentPage.items[2].id).toEqual(14)

    expect(currentPage.page).toEqual(2)
    expect(currentPage.pages).toEqual(2)
    expect(currentPage.total).toEqual(15)
    expect(currentPage).not.toHaveProperty('error')
  })

  it('Defaults to 10 max when no second argument provided', () => {
    page = 1
    const paginator = paginatorFactory<User>(u)
    const currentPage = paginator(page)
    expect(currentPage).toHaveProperty('pageSize', 10)
    expect(currentPage.page).toEqual(1)
    expect(currentPage.pages).toEqual(2)
    expect(currentPage.total).toEqual(15)
    expect(currentPage).not.toHaveProperty('error')
  })

  it('Should return an error if collection is null', () => {
    expect(() => {
      page = 1
      const max: number = 5
      // @ts-ignore
      paginatorFactory<User>(null, max)
    }).toThrowError('The provided array was invalid')
  })

  it('Should return an error if  max is null', () => {
    expect(() => {
      page = 1
      // @ts-ignore
      const paginator = paginatorFactory<User>(u, null)
      paginator(page)
    }).toThrowError('Max pages argument MUST be an integer')
  })

  it('Shluld return an error when page is null', () => {
    expect(() => {
      // @ts-ignore
      page = null
      const max: number = 5
      const paginator = paginatorFactory<User>(u, max)
      paginator(page)
    }).toThrowError('page argument MUST be an integer')
  })

  it('Should return an error when max is not an integer', () => {
    expect(() => {
      page = 1
      const max: number = 1.5
      const paginator = paginatorFactory<User>(u, max)
      paginator(page)
    }).toThrowError('Max pages argument MUST be an integer')
  })

  it('Should return an error when page is not an integer', () => {
    expect(() => {
      page = 1.5
      const max: number = 5
      const paginator = paginatorFactory<User>(u, max)
      paginator(page)
    }).toThrowError('page argument MUST be an integer')
  })

})
