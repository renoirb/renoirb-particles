import type {
  IPaginatorState,
  IPaginatorIndices,
  IPaginatorItems,
  IPaginatorError,
  IBasePaginatorState,
} from './types'

export const toPaginatorError = (
  input: IBasePaginatorState,
  error: string,
): IPaginatorError => {
  return {
    discriminant: 'error',
    ...input,
    items: [],
    error,
  } as IPaginatorError
}

export const assertsPaginatorState = <T>(
  input: any,
): asserts input is IPaginatorState<T> => {
  if ('error' in input || 'items' in input === false) {
    const message = `It is not a IPaginatorState: ` + input.error
    throw new TypeError(message)
  }
}

export const toPaginatorItems = <T>(
  input: IBasePaginatorState,
  items: T[],
): IPaginatorItems<T> => {
  return {
    discriminant: 'items',
    ...input,
    items,
  } as IPaginatorItems<T>
}

export const toPaginatorIndices = (
  input: IBasePaginatorState,
  items: number[],
): IPaginatorIndices => {
  return {
    discriminant: 'indices',
    ...input,
    items,
  } as IPaginatorIndices
}

export const isPaginatorItemsTypeNarrower = <T>(
  input: IPaginatorState<T>,
): input is IPaginatorItems<T> => {
  return (
    'items' in input &&
    Array.isArray(input.items) &&
    input.discriminant === 'items'
  )
}

export const assertsPaginatorItems = <T>(
  input: any,
): asserts input is IPaginatorItems<T> => {
  const test = isPaginatorItemsTypeNarrower(input as any)
  if (!test) {
    const message = `Unexpected input, it is not a IPaginatorItems`
    throw new TypeError(message)
  }
}

export const isPaginatorIndicesTypeNarrower = <T>(
  input: IPaginatorState<T>,
): input is IPaginatorIndices => {
  return (
    'items' in input &&
    Array.isArray(input.items) &&
    input.discriminant === 'indices'
  )
}

export const assertsPaginatorIndices = (
  input: any,
): asserts input is IPaginatorIndices => {
  const test = isPaginatorIndicesTypeNarrower(input as any)
  if (!test) {
    const message = `Unexpected input, it is not a IPaginatorIndices`
    throw new TypeError(message)
  }
}
