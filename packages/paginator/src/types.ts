/**
 * This code has been written back in 2018, and has run in production in a Vue.js app.
 *
 * See also:
 * - https://gist.github.com/renoirb/5d048d3b95f340e20fa0454d4c446b6a
 * - https://stackblitz.com/edit/202009-ngx-translate-w-pluralization-and-gender?file=src%2Fapp%2Fpaginator.ts
 *
 * @author Renoir Boulanger <contribs@renoirboulanger.com>
 */

/**
 * Current Paginator representation, for the current page.
 *
 * Matches [Element UI's el-pagination component attributes][1]
 *
 * [1]: https://element.eleme.io/#/en-US/component/pagination#attributes
 */
export interface IBasePaginatorState {
  /**
   * Current page number (indexed at 1).
   */
  page: number
  /**
   * Item count of each page, supports the .sync modifier
   * Should be the same value as the factory second argument
   */
  pageSize: number
  /**
   * How many pages in total.
   *
   * > pages = items.length / pageSize
   *
   */
  pages: number
  /**
   * The total number of elements
   */
  total: number
}

export interface IPaginatorError extends IBasePaginatorState {
  readonly discriminant: 'error'
  /**
   * If any errors returned. If there were no errors, will be empty
   * Should there be an error, this property should exist
   */
  error: string
  items: []
}

export interface IPaginatorItems<T> extends IBasePaginatorState {
  readonly discriminant: 'items'
  /**
   * The array of all elements in the returned page.
   *
   * Should be an immutable copy, and the paginator state will only
   * return the items matching that search item results "window".
   */
  items: T[]
}

export interface IPaginatorIndices extends IBasePaginatorState {
  readonly discriminant: 'indices'
  /**
   * Alternatively you can get the collection indices and ignore items array
   */
  items: number[]
}

export type IPaginatorState<T> = IPaginatorItems<T> | IPaginatorIndices

/**
 * Query what is the applicable Paginator for a given page
 */
export type IPaginatorFn<T> = (
  page: number,
  indices?: boolean,
) => IPaginatorState<T> | IPaginatorError

export interface IPaginatorDiscriminator<T> {
  readonly discriminant: 'indices' | 'items'
  items: T[] | number[]
}

export interface IPaginatorContext {
  pageSize: number
  total: number
  error?: string
}
