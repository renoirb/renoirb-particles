# Change Log - @renoirb/paginator

This log was last generated on Tue, 24 Nov 2020 09:11:31 GMT and should not be manually modified.

## 0.1.2
Tue, 24 Nov 2020 09:11:31 GMT

### Patches

- Node.js exports must start by dot slash

## 0.1.1
Mon, 23 Nov 2020 07:47:01 GMT

### Patches

- Upgrade @types/node

## 0.1.0
Mon, 23 Nov 2020 05:32:29 GMT

### Minor changes

- New package paginator

