// import { Basics } from '@renoirb/jsonresume-model'

export const BasicsComponent = {
  functional: true,
  props: {
    basics: {
      type: Object,
      default: () => ({
        name: 'John Doe',
        label: 'Software Developer',
      }),
    },
  },
  render(h, ctx) {
    return h('div', {}, [
      h('h1', {}, [
        h('span', {}, [ctx.props.basics.name, h('br')]),
        h('small', {}, [ctx.props.basics.label]),
      ]),
    ])
  },
}

export const AppRoot = {
  functional: true,
  props: {
    resume: Object,
    default: () => ({
      basics: {},
    }),
  },
  render(h, ctx) {
    return h('div', {}, [h(BasicsComponent, { props: ctx.props })])
  },
}

export default AppRoot
