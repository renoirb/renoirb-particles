# Change Log - @renoirb/koa-middleware-utils

This log was last generated on Thu, 14 Jan 2021 02:22:52 GMT and should not be manually modified.

## 0.1.3
Thu, 14 Jan 2021 02:22:52 GMT

### Patches

- had error in recovery-form middleware

## 0.1.2
Tue, 24 Nov 2020 09:11:31 GMT

### Patches

- Node.js exports must start by dot slash

## 0.1.1
Mon, 23 Nov 2020 07:47:01 GMT

### Patches

- Upgrade @types/node
- Missing exports

## 0.1.0
Mon, 23 Nov 2020 05:32:29 GMT

### Minor changes

- New package koa-middleware-utils

