import { Context } from 'koa'
import { createMockContext } from '@shopify/jest-koa-mocks'
import {
  hasSessionCookieMiddleware as middleware,
  IHasSessionCookieMiddlewareOptions,
} from '../../middleware/has-session-cookie'

const statePropValidator = (ctx: Context) =>
  ctx.state && ctx.state.hasSession && ctx.state.hasSession === true

describe('has-session-cookie middleware', () => {
  test('Happy path', async () => {
    const state = {}
    const cookies = {
      'app:sess': '1111111.22222222222.33333--1',
    }
    const ctx = createMockContext({ cookies, state, throw: jest.fn() })
    const next = jest.fn()
    const subject = middleware({ cookieName: 'app:sess' })
    await subject(ctx, next)
    expect(ctx.status).not.toBe(401)
    expect(ctx.state).toHaveProperty('hasSessionCookie', {
      cookieName: 'app:sess',
      cookieValue: cookies['app:sess'],
      propValidatorOutcome: null,
    })
    expect(ctx.throw).not.toBeCalled()
    expect(next).toBeCalled()
  })
  test('Happy path with statePropValidator', async () => {
    const state = { foo: { bar: { bazz: 1 } } }
    const cookies = {
      tkn: '1111111.22222222222.33333',
    }
    const ctx = createMockContext({ cookies, state, throw: jest.fn() })
    const next = jest.fn()
    const subject = middleware({
      cookieName: 'tkn',
      statePropValidator: (ctx: Context) => ctx.state.foo.bar.bazz === 1,
    })
    await subject(ctx, next)
    expect(ctx.status).not.toBe(401)
    expect(ctx.state).toHaveProperty('hasSessionCookie', {
      cookieName: 'tkn',
      cookieValue: cookies.tkn,
      propValidatorOutcome: true,
    })
    expect(ctx.throw).not.toBeCalled()
    expect(next).toBeCalled()
  })
  test('Forbid when statePropValidator returned false', async () => {
    const cookies = {
      tkn: '1111111.22222222222.33333',
    }
    const state = { hasSession: false }
    const ctx = createMockContext({ cookies, state, throw: jest.fn() })
    const next = jest.fn()
    const subject = middleware({
      cookieName: 'tkn',
      statePropValidator,
    })
    await subject(ctx, next)
    expect(ctx.status).toBe(401)
    expect(ctx.throw).toBeCalled()
    expect(next).toBeCalled()
  })
  test('Forbid when statePropValidator returned is anything but true', async () => {
    const cookies = {
      tkn: '1111111.22222222222.33333',
    }
    const state = {
      hasSession: Math.PI /* should be true, but just in case */,
    }
    const ctx = createMockContext({ cookies, state, throw: jest.fn() })
    const next = jest.fn()
    const subject = middleware(({
      cookieName: 'tkn',
      statePropValidator: () => Math.PI,
    } as any) as IHasSessionCookieMiddlewareOptions)
    await subject(ctx, next)
    expect(ctx.state).toHaveProperty('hasSessionCookie', {
      cookieName: 'tkn',
      cookieValue: cookies.tkn,
      propValidatorOutcome: null /** Because statePropValidator is not returning boolean */,
    })
    expect(ctx.status).toBe(401)
    expect(ctx.throw).toBeCalled()
    expect(next).toBeCalled()
  })
  test('No arguments defaults to allow all', async () => {
    const cookies = {}
    const state = {}
    const ctx = createMockContext({ cookies, state, throw: jest.fn() })
    const next = jest.fn()
    const subject = middleware({})
    await subject(ctx, next)
    expect(ctx.status).not.toBe(401)
    expect(ctx.throw).not.toBeCalled()
    expect(next).toBeCalled()
  })
})
