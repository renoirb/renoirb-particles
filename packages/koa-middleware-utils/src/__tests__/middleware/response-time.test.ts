import Koa from 'koa'

// @ts-ignore
import { createMockContext } from '@shopify/jest-koa-mocks'
// @ts-ignore
import request from 'supertest'

import {
  ResponseTime,
  responseTimeMiddleware as middleware,
} from '../../middleware/response-time'
import { sleep } from '../../utils'

const matchMiliRegEx = /^\d[\d\.]+\sms$/
const matchNanoRegEx = /^\d[\d\.]+\sns$/

describe('ResponseTime', () => {
  test('constructor', async () => {
    const subject = new ResponseTime()
    await sleep(2)
    subject.mark()
    const formatted = String(subject)
    expect(subject.abbr).toBe('ns')
    expect(subject.resolution).toBe('nanoseconds')
    expect(formatted).toMatch(matchNanoRegEx)
  })
  test('constructor: invalid argument at runtime', async () => {
    const fallback = ResponseTime.FALLBACK
    // @ts-ignore
    // An invalid argument. But runtime might not check.
    const subject = new ResponseTime(true)
    await sleep(2)
    subject.mark()
    expect(subject.resolution).toBe(fallback)
    expect(fallback).toBe('nanoseconds')
    expect(String(subject)).toMatch(matchNanoRegEx)
  })
  test('constructor: nanoseconds at constructor', async () => {
    const resolution = 'milliseconds'
    const subject = new ResponseTime(resolution)
    await sleep(2)
    subject.mark()
    const formatted = String(subject)
    expect(subject.abbr).toBe('ms')
    expect(subject.resolution).toBe(resolution)
    expect(formatted).toMatch(matchMiliRegEx)
    expect(subject.resolution).toBe(resolution)
  })
  test('getTime', async () => {
    const subject = new ResponseTime()
    await sleep(2)
    const time = subject.getTime()
    await sleep(2)
    const time2 = subject.getTime()
    expect(time).toBeGreaterThan(1)
    expect(time).toBe(time2)
    expect(String(subject)).toMatch(matchNanoRegEx)
  })
  test('toString', async () => {
    const subject = new ResponseTime()
    await sleep(2)
    expect(String(subject)).toMatch(matchNanoRegEx)
  })
  test('setResolution', async () => {
    const subject = new ResponseTime()
    expect(subject.resolution).toBe('nanoseconds')
    await sleep(2)
    subject.setResolution('milliseconds')
    expect(subject.resolution).toBe('milliseconds')
    expect(String(subject)).toMatch(matchMiliRegEx)
  })
})

describe('ResponseTime middleware', () => {
  /**
   * Example of how to inspect middleware without integrating with Koa.
   *
   * This example might be too contrived,
   * but that'll be useful to start from when we'll need to inspect.
   *
   * Look at the next example where we'll use only built-in Jest utilities.
   *
   * https://github.com/Shopify/quilt/tree/master/packages/jest-koa-mocks
   * https://github.com/Shopify/quilt/blob/master/packages/jest-koa-mocks/src/create-mock-context/test/create-mock-context.test.ts#L156
   */
  test('Using Shopify/quilt jest-koa-mocks', async () => {
    const state = { responseTime: '' }
    const ctx = createMockContext({ state })
    const next = jest.fn()
    const subject = middleware({ resolution: 'milliseconds' })
    await subject(ctx, next)
    const response = ctx.response
    const headers = response.header
    expect(
      Object.getOwnPropertyNames(headers).includes('x-response-time'),
    ).toBe(true)
    expect(headers['x-response-time']).toMatch(matchMiliRegEx)
    expect(next).toBeCalled()
  })

  /**
   * Use only built-in Jest.
   *
   * An example of how to mock methods and directly inspect.
   *
   * https://github.com/koajs/koa/blob/master/docs/api/response.md#responsesetfields
   */
  test('Using built-in Jest mocks', async () => {
    const ctx = { set: jest.fn(), state: {} }
    const next = jest.fn()
    const subject = middleware({ resolution: 'milliseconds' })
    // @ts-ignore
    await subject(ctx, next)

    /**
     * ```
     * const ctx = {
     *   "set":  { // [MockFunction]
     *     "calls": [
     *       [
     *         "X-Response-Time",
     *         "0.077 ms",
     *       ],
     *     ],
     *     "results": [
     *       {
     *         "isThrow": false,
     *         "value": undefined,
     *       },
     *     ],
     *   },
     * }
     * ```
     */
    expect(ctx.set).toHaveBeenCalledTimes(1)
    // Access ctx.set MockFunction contents.
    const calls = ctx.set.mock.calls
    const [headerName = null, value = null] = calls[0] || []

    expect(headerName).toBe('X-Response-Time')
    const state = ctx.state
    expect(state).toHaveProperty('responseTime')
    const rt = Reflect.get(state, 'responseTime') || null
    expect(value).toBe(rt)
    expect(next).toBeCalled()
  })
})

describe('SLOW: ResponseTime middleware within Koa', () => {
  // @ts-ignore
  let app

  beforeAll(async () => {
    app = new Koa()
    // @ts-ignore
    app.use(middleware())
  })

  /**
   * Slow integration test example.
   */
  test('In integration with Koa', async (done) => {
    // @ts-ignore
    app.use(async (ctx, next) => {
      await sleep(2)
      ctx.body = 'Hello World'
      ctx.status = 200
      await next()
    })

    /**
     * superagent response object
     *
     * ```js
     * let response = await request(app.callback())
     * ```
     *
     * Should look like this
     *
     * ```js
     * response = {
     *   header: {
     *     connection: 'close',
     *     'content-length': '9',
     *     'content-type': 'text/plain; charset=utf-8',
     *     date: 'Wed, 23 Jan 2019 17:38:19 GMT',
     *     'x-response-time': '1.548 ms',
     *   },
     *   req: {
     *     data: undefined,
     *     headers: {
     *       'user-agent': 'node-superagent/3.8.3',
     *     },
     *     method: 'GET',
     *     url: 'http://127.0.0.1:60051/',
     *   },
     *   status: 200,
     *   text: 'Hello World',
     * }
     * ```
     */
    // @ts-ignore
    await request(app.callback())
      .get('/')
      .expect('x-response-time', matchMiliRegEx)

    // This is useful otherwise Jest will tell that we left a reference opened.
    // Also prevents TCPSERVERWRAP error when running Jest with Asychronous code.
    // https://github.com/facebook/jest/issues/6907
    await done()
  })
})
