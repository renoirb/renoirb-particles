import { createMockContext } from '@shopify/jest-koa-mocks'
import { recoveryFormMiddleware as middleware } from '../../middleware/recovery-form'

describe('session-recovery  middleware', () => {
  test('Changing baseApi option', async () => {
    const token = '111.222.333'
    const url = '/foo/bar?jwt=' + token
    const ctx = createMockContext({ url })
    const subject = middleware({
      baseApi: '/whatever',
      recoveryPath: '/foo/bar/auth/session/recovery',
    })
    // @ts-ignore
    await subject(ctx)
    expect(ctx.body).toMatch(
      /action="\/whatever\/foo\/bar\/auth\/session\/recovery"/g,
    )
    const matcher = new RegExp(
      'value="' + token.replace('.', '\\.') + '" name=jwt',
      'g',
    )
    expect(ctx.body).toMatch(matcher)
    expect(ctx.body).toMatchSnapshot()
  })

  test('Default options', async () => {
    const token = '111.222.444'
    const url = '/foo/bar?jwt=' + token
    const ctx = createMockContext({ url })
    // @ts-ignore
    const subject = middleware()
    // @ts-ignore
    await subject(ctx)
    expect(ctx.body).toMatch(/action="\/bff\/auth\/session"/g)
    const matcher = new RegExp(
      // ... HTML ... <input type=text value="111.222.444" name=jwt /> ... HTML ...
      'value="' + token.replace('.', '\\.') + '" name=jwt',
      'g',
    )
    expect(ctx.body).toMatch(matcher)
    expect(ctx.body).toMatchSnapshot()
  })

  test('Redirect option', async () => {
    const url = '/foo'
    const redirect = '/hello-world'
    const ctx = createMockContext({ url })
    const subject = middleware({ redirect })
    // @ts-ignore
    await subject(ctx)
    const matcher = new RegExp(
      'value="' + encodeURIComponent(redirect) + '" name=redirect',
      'g',
    )
    expect(ctx.body).toMatch(matcher)
    expect(ctx.body).toMatchSnapshot()
  })
})
