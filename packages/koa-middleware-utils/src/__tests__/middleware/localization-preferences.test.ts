import { Context } from 'koa'
import { createMockContext } from '@shopify/jest-koa-mocks'
import { localizationPreferencesMiddleware as middleware } from '../../middleware/localization-preferences'
import type { ILocalizationPreferencesState } from '../../middleware/localization-preferences'

describe('Localization Preferences middleware', () => {
  test('Happy path', async () => {
    const expectedTz = 'America/Montreal'
    const expectedLocale = 'fr-CA'
    const state = {}
    const ctx = createMockContext({ state, throw: jest.fn() })
    const next = jest.fn()
    const subject = middleware({
      fallbackTz: expectedTz,
      fallbackLocale: expectedLocale,
    })
    await subject(ctx, next)
    expect(ctx.state).toHaveProperty('localizationPreferences', {
      tz: expectedTz,
      locale: expectedLocale,
    } as ILocalizationPreferencesState)
    expect(ctx.throw).not.toBeCalled()
    expect(next).toBeCalled()
  })

  test('Happy path when providing statePropGetTz and statePropGetLocale returning valid', async () => {
    const expectedTz = 'America/Montreal'
    const expectedLocale = 'fr-CA'
    const state = {}
    const ctx = createMockContext({ state, throw: jest.fn() })
    const next = jest.fn()
    const subject = middleware({
      fallbackTz: 'Europe/Lisbon',
      fallbackLocale: 'pt-PT',
      statePropGetTz: (_: Context) => expectedTz,
      statePropGetLocale: (_: Context) => expectedLocale,
    })
    await subject(ctx, next)
    expect(ctx.state).toHaveProperty('localizationPreferences', {
      tz: expectedTz,
      locale: expectedLocale,
    } as ILocalizationPreferencesState)
  })

  test('When statePropGetTz and statePropGetLocale returns invalid values', async () => {
    const expectedTz = 'America/Montreal'
    const expectedLocale = 'fr-CA'
    const state = {}
    const ctx = createMockContext({ state, throw: jest.fn() })
    const next = jest.fn()
    const subject = middleware({
      fallbackTz: expectedTz,
      fallbackLocale: expectedLocale,
      statePropGetTz: (_: Context) => '⊂(▀¯▀⊂)',
      statePropGetLocale: (_: Context) => 'ᕦ( ̿ ﹏ ̿ )ᕤ',
    })
    await subject(ctx, next)
    expect(ctx.state).toHaveProperty('localizationPreferences', {
      tz: expectedTz,
      locale: expectedLocale,
    } as ILocalizationPreferencesState)
  })

  test('When no arguments defaults to Montréal in English', async () => {
    const expectedTz = 'America/Montreal'
    const expectedLocale = 'en-CA'
    const state = {}
    const ctx = createMockContext({ state, throw: jest.fn() })
    const next = jest.fn()
    const subject = middleware()
    await subject(ctx, next)
    expect(ctx.state).toHaveProperty('localizationPreferences', {
      tz: expectedTz,
      locale: expectedLocale,
    } as ILocalizationPreferencesState)
    expect(ctx.throw).not.toBeCalled()
    expect(next).toBeCalled()
  })
})
