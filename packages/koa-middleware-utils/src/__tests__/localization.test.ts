import { Locale } from '../localization'

describe('Locale', () => {
  const assertions = [
    ['fr-CA', true, { langCode: 'fr', country: 'CA' }],
    ['fr-FR', true, { langCode: 'fr', country: 'FR' }],
    ['en-CA', true, { langCode: 'en', country: 'CA' }],
    ['de-DE', true, { langCode: 'de', country: 'DE' }],
    ['pt-BR', true, { langCode: 'pt', country: 'BR' }],
    ['pt-PT', true, { langCode: 'pt', country: 'PT' }],
    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Locale
    // Japanese from Japan, but we ignore the rest of the string, for now.
    [
      'ja-Jpan-JP-u-ca-japanese-hc-h12',
      true,
      { langCode: 'ja', country: 'JP' },
    ],
    ['', false, { langCode: '', country: '' }],
    ['hello-world-speak-mine-plz', false, { langCode: '', country: '' }],
  ]

  for (const [input, expected, dto] of assertions) {
    test(`"${input}" should ${expected ? 'Pass' : 'Fail'}`, () => {
      const subject = new Locale(input)
      expect(subject.isValid).toBe(expected)
      expect(subject).toMatchObject(dto)
    })
  }

  test('Exceptions at constructor time', () => {
    expect(() => new Locale(234)).toThrow()
    expect(() => new Locale([])).toThrow()
    expect(() => new Locale(true)).toThrow()
  })

  test('Asking for a fallback at constructor time', () => {
    expect(new Locale('', 'en-ca')).toMatchObject({
      langCode: 'en',
      country: 'CA',
    })
    expect(new Locale('3asfdlj', 'ja-Jp')).toMatchObject({
      langCode: 'ja',
      country: 'JP',
    })
  })
})
