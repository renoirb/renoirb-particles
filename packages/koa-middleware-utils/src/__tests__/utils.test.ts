import { createSessionState, createMockPasswordValidator } from '../utils'
import { ISessionSubject } from '../types'

interface IMyLocalSessionSubject extends ISessionSubject {
  likesChocolate: boolean
  email: string
}

describe('utils', () => {
  describe('createSessionState', () => {
    test('Non authenticated returns Anonymous', () => {
      const subject = createSessionState(false)
      expect(subject).toHaveProperty('authenticated', false)
      expect(subject).toHaveProperty('subject.displayName', 'Anonymous')
    })
    test('Non authenticated subject can be superseeded from outside', () => {
      const subjectData: IMyLocalSessionSubject = {
        displayName: 'Anonymous John',
        likesChocolate: true,
        email: 'root@example.org',
      }
      const subject = createSessionState(false, subjectData)
      expect(subject).toHaveProperty('authenticated', false)
      expect(subject).toHaveProperty(
        'subject.displayName',
        subjectData.displayName,
      )
    })
    test('Authenticated subject can be superseeded from outside', () => {
      const subjectData: IMyLocalSessionSubject = {
        displayName: 'John Doe',
        likesChocolate: false,
        email: 'john.doe@example.org',
      }
      const subject = createSessionState(false, subjectData)
      expect(subject).toHaveProperty('authenticated', false)
      expect(subject).toHaveProperty(
        'subject.displayName',
        subjectData.displayName,
      )
    })
  })

  describe('createMockPasswordValidator', () => {
    test('Happy-Path', () => {
      const validator = createMockPasswordValidator('admin', 'admin')
      expect(() => validator('', '')).toThrow()
      expect(() => validator('admin', 'wrong password')).toThrow()
      expect(() => validator('wrong user', 'admin')).toThrow()
      expect(validator('admin', 'admin')).toBe(true)
    })
  })
})
