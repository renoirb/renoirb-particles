import { strict as assert } from 'assert'
import type {
  ISessionState,
  ICookieOptions,
  ISessionSubject,
  IAuthenticationValidator,
} from './types'

export const assertsIsObject = (input: any): boolean => {
  assert.equal(
    typeof input === 'object' ? input !== null : typeof input === 'function',
    true,
    'Must be an object',
  )
  return true
}

export const sessionCookieCreateConfig = (maxAge?: number): ICookieOptions => {
  const options: ICookieOptions = {
    signed: true,
    httpOnly: true,
    path: '/api',
  }
  if (maxAge && typeof maxAge === 'number') {
    // For an hour after creation, e.g.
    // new Date(Date.now() + 3600000);
    options.maxAge = maxAge
  }
  return options
}

export const createSessionState = <T extends {} = {}>(
  authenticated: boolean,
  maybeSubject?: T & ISessionSubject,
): ISessionState<T & ISessionSubject> => {
  let subject: T & ISessionSubject
  if (!maybeSubject) {
    subject = { displayName: 'Anonymous' } as T & ISessionSubject
  } else {
    assertsIsObject(maybeSubject)
    assert.equal(
      Reflect.has(maybeSubject, 'displayName'),
      true,
      'Property displayName is missing',
    )
    assert.equal(
      typeof Reflect.get(maybeSubject, 'displayName') === 'string',
      true,
      'Property displayName must be a string',
    )
    subject = maybeSubject
  }
  const state: ISessionState<T & ISessionSubject> = { authenticated, subject }
  return state
}

export const createMockPasswordValidator = (
  username: string,
  password: string,
): IAuthenticationValidator => (
  usernameInput: string = '',
  passwordInput: string = '',
): boolean => {
  assert.equal(password === passwordInput, true)
  assert.equal(username === usernameInput, true)
  // If nothing throwhs thus far, good job!
  return true
}

function _sleep(ms: number) {
  return new Promise((resolve) => setTimeout(resolve, ms))
}

export async function sleep(ms: number) {
  await _sleep(ms)
}
