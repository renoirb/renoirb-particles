import { Context, Middleware } from 'koa'
import { strict as assert } from 'assert'
import { assertsIsObject } from '../utils'
import { Locale, isTimeZone } from '../localization'

export const DEFAULT_TZ = 'America/Montreal'

export const statePropGetTzDefaultFn = (_: Context): typeof DEFAULT_TZ =>
  DEFAULT_TZ

export const DEFAULT_LOCALE = 'en-CA'

export const statePropGetLocaleDefaultFn = (
  _: Context,
): typeof DEFAULT_LOCALE => DEFAULT_LOCALE

export interface ILocalizationPreferencesMiddlewareOptions {
  fallbackTz: string
  fallbackLocale: string
  statePropGetTz?: (ctx: Context) => string | typeof DEFAULT_TZ
  statePropGetLocale?: (ctx: Context) => string | typeof DEFAULT_LOCALE
}

export interface ILocalizationPreferencesState {
  tz: string | typeof DEFAULT_TZ
  locale: string | typeof DEFAULT_LOCALE
}

function factory(
  args?: Partial<ILocalizationPreferencesMiddlewareOptions>,
): Middleware {
  if (args) {
    assertsIsObject(args)
  }
  const options: ILocalizationPreferencesMiddlewareOptions = {
    fallbackTz: DEFAULT_TZ,
    fallbackLocale: DEFAULT_LOCALE,
    ...(args || {}),
  }

  assert.equal(isTimeZone(options.fallbackTz), true, 'Invalid Time Zone')
  const validatingLocale = new Locale(options.fallbackLocale)
  assert.equal(validatingLocale.isValid, true, 'Invalid locale language-tag')

  return async (
    ctx: Context,
    // {@link node_modules/@types/koa/index.d.ts}
    // @ts-ignore
    next,
  ) => {
    await next()

    const tzGetOutcome =
      typeof options.statePropGetTz === 'function'
        ? options.statePropGetTz.call(undefined, ctx)
        : null

    const tz: string | typeof DEFAULT_TZ =
      typeof tzGetOutcome === 'string' && isTimeZone(tzGetOutcome)
        ? tzGetOutcome
        : options.fallbackTz

    const localeGetOutcome =
      typeof options.statePropGetLocale === 'function'
        ? options.statePropGetLocale.call(undefined, ctx)
        : options.fallbackLocale

    let locale: string | typeof DEFAULT_LOCALE = options.fallbackLocale
    if (typeof localeGetOutcome === 'string') {
      const maybe = new Locale(localeGetOutcome)
      if (maybe.isValid) {
        locale = String(maybe)
      }
    }

    ctx.state.localizationPreferences = {
      tz,
      locale,
    } as ILocalizationPreferencesState
  }
}

export const localizationPreferencesMiddleware: (
  options?: Partial<ILocalizationPreferencesMiddlewareOptions>,
) => Middleware = factory
