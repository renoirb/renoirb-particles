import { Context, Middleware } from 'koa'
import { assertsIsObject } from '../utils'

export interface IHasSessionCookieMiddlewareOptions {
  cookieName: string

  /**
   * Function closure to drill into Koa context and ensure a property check
   */
  statePropValidator?: (ctx: Context) => boolean
}

export interface IHasSessionCookieState {
  propValidatorOutcome: boolean | null
  cookieName: string
  cookieValue: string | null
}

function factory(
  args?: Partial<IHasSessionCookieMiddlewareOptions>,
): Middleware {
  if (args) {
    assertsIsObject(args)
  }
  const options: IHasSessionCookieMiddlewareOptions = {
    cookieName: '',
    ...(args || {}),
  }

  const { cookieName, statePropValidator } = options

  return async (
    ctx: Context,
    // {@link node_modules/@types/koa/index.d.ts}
    // @ts-ignore
    next,
  ) => {
    // Allow all when an empty string
    if (cookieName === '') {
      await next()
      return
    }

    const cookieValue = ctx.cookies.get(cookieName)
    const hasCookieHeaderValue: string = cookieValue ? 'true' : 'false'
    ctx.set('X-Has-Cookie', hasCookieHeaderValue)

    ctx.state.hasSessionCookie = {
      cookieValue,
      cookieName,
      propValidatorOutcome: null,
    } as IHasSessionCookieState

    await next()

    const propValidatorOutcome =
      typeof statePropValidator === 'function'
        ? statePropValidator.call(undefined, ctx)
        : null
    ctx.state.hasSessionCookie.propValidatorOutcome =
      typeof propValidatorOutcome !== 'boolean' ? null : propValidatorOutcome
    const hasCookieData = ctx.state.hasSessionCookie.cookieValue !== ''

    if (
      !hasCookieData ||
      (typeof statePropValidator === 'function' &&
        ctx.state.hasSessionCookie.propValidatorOutcome !== true)
    ) {
      ctx.status = 401
      ctx.throw(401, 'Unauthorized')
    }
  }
}

export const hasSessionCookieMiddleware: (
  options?: Partial<IHasSessionCookieMiddlewareOptions>,
) => Middleware = factory
