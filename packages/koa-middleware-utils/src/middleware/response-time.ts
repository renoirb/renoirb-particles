import { Context, Middleware } from 'koa'

export type HighResolutionTimeTuple = [number, number]

export type TimeResolution = 'seconds' | 'nanoseconds' | 'milliseconds'

export interface IResponseTimeMiddlewareOptions {
  resolution?: TimeResolution
}

export class ResponseTime {
  static FALLBACK: TimeResolution = 'nanoseconds'

  abbr: string = 'ns'

  private begin: HighResolutionTimeTuple
  private delta: HighResolutionTimeTuple = [0, 0]

  constructor(public resolution: TimeResolution = 'nanoseconds') {
    this.begin = process.hrtime()
    this.setResolution(resolution)
  }

  setResolution(resolution: TimeResolution = 'nanoseconds'): void {
    const resolutions = {
      milliseconds: 'ms',
      nanoseconds: 'ns',
      seconds: 's',
    }

    const isValidResolution = Object.keys(resolutions).includes(resolution)
    const res = isValidResolution ? resolution : ResponseTime.FALLBACK

    this.resolution = res
    this.abbr = resolutions[res]
  }

  mark(): void {
    // if (!!this.delta[1] && this.delta[1] === 0) { // Uncomment to see breaking test #DemoTesting
    if (this.delta[1] === 0) {
      this.delta = process.hrtime(this.begin)
    }
  }

  getTime(): number {
    this.mark()
    const nanoseconds: number = this.delta[0] * 1e9 + this.delta[1]
    const milliseconds: number = nanoseconds / 1e6
    const seconds: number = nanoseconds / 1e9

    const available = {
      milliseconds,
      nanoseconds,
      seconds,
    }

    return available[this.resolution] || nanoseconds
  }

  toString(): string {
    const abbr = this.abbr
    const time = this.getTime()

    return `${time} ${abbr}`
  }
}

function factory(options?: IResponseTimeMiddlewareOptions | void): Middleware {
  // https://github.com/koajs/koa/blob/master/docs/guide.md#middleware-options
  const { resolution = 'milliseconds' } = options || {}

  // @ts-ignore
  return async (ctx: Context, next) => {
    const rt = new ResponseTime(resolution)
    await next()
    const responseTimeString = rt.toString()
    ctx.set('X-Response-Time', responseTimeString)
    ctx.state.responseTime = responseTimeString
  }
}

/**
 * API ResponseTime calculator and middleware.
 *
 * Bookmarks:
 * - [koa-metrics][koa-metrics]: A Koa middleware written in isolation.
 * - [koa-sewing-kit][koa-sewing-kit]: A Server-Side to organize and serve JavaScript/CSS
 *
 * [koa-metrics]: https://github.com/Shopify/quilt/tree/master/packages/koa-metrics
 * [koa-sewing-kit]: https://github.com/Shopify/quilt/tree/master/packages/sewing-kit-koa
 */
export const responseTimeMiddleware: (
  options: IResponseTimeMiddlewareOptions,
) => Middleware = factory
