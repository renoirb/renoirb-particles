import { strict as assert } from 'assert'
import { Context, Middleware } from 'koa'
import { assertsIsObject } from '../utils'

export interface IRecoveryFormMiddlewareOptions {
  /**
   * Submit button text content
   */
  textContent: string

  /**
   * Base URL to use to prepend to recoveryPath.
   *
   * Assuming we expose the Web Portal (e.g. Nuxt) as https://example.org/portal and
   * we call our API as "bff" and is exposed under it, we'd have https://example.org/portal/bff
   *
   * e.g. ''
   */
  baseApi: string

  /**
   * URL Path to use for this middleware (without after baseApi)
   *
   * This property is appended after baseApi
   *
   * e.g. /auth/session
   */
  recoveryPath: string

  /**
   * URL to redirect to when successful.
   *
   * e.g. ''
   */
  redirect: string
}

function factory(args?: Partial<IRecoveryFormMiddlewareOptions>): Middleware {
  if (args) {
    assertsIsObject(args)
  }
  const options = {
    baseApi: '/bff',
    recoveryPath: '/auth/session',
    textContent: 'Submit',
    redirect: '',
    ...(args || {}),
  } as IRecoveryFormMiddlewareOptions
  assert.equal(
    Object.keys(options).includes('baseApi'),
    true,
    'Missing property baseApi',
  )
  assert.equal(
    (Reflect.get(options, 'baseApi') || '').startsWith('/'),
    true,
    'Property baseApi must start by /',
  )
  assert.equal(
    (Reflect.get(options, 'baseApi') || '').endsWith('/'),
    false,
    'Property baseApi must not end by /',
  )
  assert.equal(
    Object.keys(options).includes('recoveryPath'),
    true,
    'Missing property recoveryPath',
  )
  assert.equal(
    (Reflect.get(options, 'recoveryPath') || '').startsWith('/'),
    true,
    'Property recoveryPath must start by /',
  )
  assert.equal(
    (Reflect.get(options, 'recoveryPath') || '').endsWith('/'),
    false,
    'Property recoveryPath must not end by /',
  )

  return async (ctx: Context) => {
    // Do not display form or use this if we're not on the route we need
    if (options.recoveryPath !== ctx.path.replace(options.baseApi, '')) {
      return
    }

    let href = ctx.href
    const queryJwt = Reflect.has(ctx.query, 'jwt') ? ctx.query.jwt : false

    try {
      if (typeof options.baseApi === 'string') {
        href = options.baseApi
      }
      if (typeof options.recoveryPath === 'string') {
        href += options.recoveryPath
      }
    } catch (e) {
      // Nothing to do.
    }

    let jwt = ''
    if (queryJwt !== false) {
      jwt = queryJwt
    }

    let redirectInput = ''
    if (typeof options.redirect === 'string' && options.redirect !== '') {
      const cleansedRedirect = encodeURIComponent(options.redirect)
      redirectInput += `<input type=hidden value="${cleansedRedirect}" name=redirect />`
    }

    const body = `
    <form action="${href}" method=POST name=recovery-form class=is-inline>
      <button type="submit">${options.textContent}</button>
      <input type=text value="${jwt}" name=jwt class="is-hidden invisible hidden" />${redirectInput}
    </form>
    `
      .replace(/[\n\s]+/g, ' ')
      .trim()

    // Allow anybody to call this URL via AJAX
    ctx.set('Access-Control-Allow-Origin', '*')
    ctx.set('Cache-Control', 'no-cache')
    ctx.type = 'text/html'
    ctx.status = 200
    ctx.body = body
  }
}

export const recoveryFormMiddleware: (
  options?: Partial<IRecoveryFormMiddlewareOptions>,
) => Middleware = factory
