import type { ILocale } from './types'

export const toLocale = (value: any): ILocale | null => {
  if (typeof value === 'string') {
    const extracted = Locale.RE.exec(value)
    if (Array.isArray(extracted) && extracted.length > 2) {
      const [, langCode = '', country = ''] = extracted
      const out: ILocale = {
        langCode,
        country,
      }
      return out
    }
  }
  return null
}

export class Locale implements ILocale {
  readonly langCode: string = ''
  readonly country: string = ''

  static RE: RegExp = /^([a-z]{2})-([A-Z]{2})/i

  constructor(input: any, fallback?: string) {
    if (typeof input !== 'string') {
      throw new TypeError(
        `Invalid constructor argument, input MUST be a string`,
      )
    }
    let parsed = toLocale(input)
    if (parsed === null && typeof fallback === 'string') {
      parsed = toLocale(fallback)
    }
    if (parsed) {
      const { langCode = '', country = '' } = parsed
      this.langCode = String(langCode).toLowerCase()
      Object.freeze(this.langCode)
      this.country = String(country).toUpperCase()
      Object.freeze(this.country)
    }
  }

  get isValid(): boolean {
    const { langCode = '', country = '' } = this
    return langCode !== '' && country !== ''
  }

  toString(): string {
    const { langCode = '', country = '' } = this
    if (langCode === '' || country === '') {
      return ''
    }
    return `${langCode}-${country}`
  }
}

export const isTimeZone = (timeZone = 'America/Montreal'): boolean => {
  let outcome = false
  try {
    const resolved = Intl.DateTimeFormat(undefined, {
      timeZone,
    }).resolvedOptions()
    outcome = !!resolved.timeZone
  } catch (e) {
    outcome = false
  }

  return outcome
}
