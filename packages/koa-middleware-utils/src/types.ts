export interface ISessionSubject {
  displayName: string
}

export interface ISessionState<T extends ISessionSubject = ISessionSubject> {
  subject: T
  authenticated: boolean
}

export interface ICookieOptions {
  signed?: boolean
  httpOnly?: boolean
  path?: string
  maxAge?: number | string
}

export type IAuthenticationValidator = (
  username: string,
  passwordInput: string,
) => boolean

/**
 * Locale
 *
 * Is normally at least two two letter codes separated by a dash following IETF's [language tag][language-tag]:
 * - First slot is language (e.g. French, "fr")
 * - Second is country code (e.g. Canada, "CA")
 *
 * Refer to [IETF specification about Language-Tag][language-tag] and [BCP47][ietf-bcp47]
 *
 * Bookmarks:
 * - https://en.wikipedia.org/wiki/IETF_language_tag
 * - https://www.unicode.org/reports/tr35/#Language_Tag_to_Locale_Identifier
 * - https://github.com/bavardage/TypeScript/blob/538978e3be704d759a568aea36270f6859c9d95d/src/lib/intl.d.ts
 * - https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/DateTimeFormat
 * - https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Locale
 * - https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date/toLocaleString
 * - https://github.com/umpirsky/locale-list
 * - https://kazupon.github.io/vue-i18n/guide/number.html#number-localization
 *
 * [ietf-bcp47]: https://tools.ietf.org/html/bcp47
 * [language-tag]: https://tools.ietf.org/html/rfc1766#section-2
 */
export interface ILocale {
  langCode: string
  country: string
}
