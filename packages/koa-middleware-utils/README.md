[repo-url]: https://gitlab.com/renoirb/renoirb-particles/-/tree/v1.x/packages/koa-middleware-utils
[npmjs-badge]: https://img.shields.io/npm/v/%40renoirb%2Fkoa-middleware-utils?style=flat-square&logo=appveyor&label=npm&logo=npm 'NPMjs.com Package Badge'
[npmjs-url]: https://www.npmjs.com/package/%40renoirb%2Fkoa-middleware-utils
[size-badge]: https://img.shields.io/bundlephobia/min/%40renoirb%2Fkoa-middleware-utils?style=flat-square
[deps-status]: https://img.shields.io/librariesio/release/npm/%40renoirb%2Fkoa-middleware-utils?style=flat-square&logo=appveyor&logo=dependabot

# [**@renoirb/koa-middleware-utils**][repo-url]

Koa middleware utilities.

| Version                          | Size                           | Dependencies                                                      |
| -------------------------------- | ------------------------------ | ----------------------------------------------------------------- |
| [![npm][npmjs-badge]][npmjs-url] | ![npm bundle size][size-badge] | ![Libraries.io dependency status for latest release][deps-status] |
