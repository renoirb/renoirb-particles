import type { IDay, IShift } from './types'

export class Day implements IDay {
  shifts: IShift[] = []
  constructor(public label?: string) {}
}
