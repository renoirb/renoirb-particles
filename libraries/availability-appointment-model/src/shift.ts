import { strict as assert } from 'assert'
import type { IShift } from './types'
import { DateTime } from 'luxon'

export class Shift implements IShift {
  // TODO Fix Babel/Rollup/Bili/TypeScript to support ECMAScript Private Fields
  // https://www.typescriptlang.org/docs/handbook/classes.html#ecmascript-private-fields
  // #begin: DateTime
  private _begin: DateTime
  // #end: DateTime
  private _end: DateTime

  get begin(): string {
    return this._begin.toLocaleString({
      hour: '2-digit',
      minute: '2-digit',
      hour12: false,
    })
  }
  set begin(value: string) {
    const dto = DateTime.fromFormat(value, 'HH:mm')
    this._begin = dto
  }

  get end(): string {
    return this._end.toLocaleString({
      hour: '2-digit',
      minute: '2-digit',
      hour12: false,
    })
  }
  set end(value: string) {
    const dto = DateTime.fromFormat(value, 'HH:mm')
    this._end = dto
  }

  constructor(begin: string, end: string, public label?: string) {
    assert.equal(
      typeof begin === 'string',
      true,
      'Input must be an hour notation 00:00 in 24-hour format',
    )
    assert.equal(
      typeof end === 'string',
      true,
      'Input must be an hour notation 00:00 in 24-hour format',
    )
    this._begin = DateTime.fromFormat(begin, 'HH:mm')
    this._end = DateTime.fromFormat(end, 'HH:mm')
    assert.equal(this._begin < this._end, true, `The time range is invalid`)
  }

  toJSON(): IShift {
    const out: IShift = {
      begin: this.begin,
      end: this.end,
      label: this.label,
    }
    return out
  }
}
