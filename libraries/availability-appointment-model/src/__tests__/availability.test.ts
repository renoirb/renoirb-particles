import type { IAvailability } from '../types'
import { AvailabilitySchema } from '../availability-schema'
import { Availability } from '../availability'
import { Day } from '../day'
import { Shift } from '../shift'
import { safeLoad } from 'js-yaml'

const FIXTURE = `
availabilities:
- tz: America/Montreal
  label: Summer schedule
  days:
    - label: Monday
      shifts:
        - begin: '13:00'
          end: '19:00'
          label: 'Lazy monday'
    - label: Tuesday
      shifts:
        - begin: '09:00'
          end: '12:00'
        - begin: '13:00'
          end: '18:00'
    - label: Wednesday
      shifts:
        - begin: '09:00'
          end: '12:00'
        - begin: '13:00'
          end: '18:00'
    - label: Thursday
      shifts:
        - begin: '09:00'
          end: '12:00'
        - begin: '13:00'
          end: '18:00'
    - shifts:
        - begin: '09:00'
          end: '12:00'

`
// ^ try running tests when removing "end" or other required properties

describe('availability', () => {
  const validator = new AvailabilitySchema()
  test('Happy-Path', () => {
    const data = safeLoad(FIXTURE)
    /**
     * Tedious?
     * - https://www.npmjs.com/package/serializr
     * - https://www.npmjs.com/package/zod
     *
     * See also:
     * - Tackling TypeScript – Dr. Axel Rauschmayer, 24 Validating external data
     */
    validator.validateObject(data)
    const { availabilities = [] } = data as { availabilities: IAvailability[] }
    for (const a of availabilities) {
      const subject = new Availability(
        a.tz || '༼つಠ益ಠ༽つ ─=≡ΣO))',
        a.label || 'Renoir’s week',
      )
      const { days = [] } = a
      for (const d of days) {
        const day = new Day(d.label)
        const { shifts = [] } = d
        for (const s of shifts) {
          const shift = new Shift(s.begin || '00:00', s.end || '00:00', s.label)
          day.shifts.push(shift)
        }
        subject.days.push(day)
      }
      expect(subject).toMatchSnapshot()
    }
  })

  test('fromObject', () => {
    const data = safeLoad(FIXTURE)
    const subject = Availability.fromObject(data)
    expect(subject).toMatchSnapshot()
  })
})
