import { Shift } from '../shift'

describe('shift', () => {
  test.each([
    ['09:00', '10:00', false],
    ['09:00', '09:01', false],
    ['09:00', '09:00', true],
    ['09:00', '08:00', true],
    ['༼つಠ益ಠ༽つ ─=≡ΣO))', Number.POSITIVE_INFINITY, true],
    [undefined, { toString: () => '09:00' }, true],
  ] as any[])('begin %p, end %p, throws? %p', (begin, end, shouldThrow) => {
    if (shouldThrow) {
      expect(() => new Shift(begin as string, end as string)).toThrow()
    } else {
      const subject = new Shift(begin as string, end as string)
      expect(subject).toHaveProperty('begin', begin)
      expect(subject).toHaveProperty('end', end)
    }
  })
})
