import { SchemaValidator as BaseSchemaValidator } from '@renoirb/jsonschema-aware-loader'

export class AvailabilitySchema extends BaseSchemaValidator {
  static readonly SCHEMA: string = 'resources/schemas/availability.schema.json'
  constructor() {
    super(__dirname, AvailabilitySchema.SCHEMA)
  }
}
