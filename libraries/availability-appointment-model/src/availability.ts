import { strict as assert } from 'assert'
import { DateTime } from 'luxon'
import { Day } from './day'
import { Shift } from './shift'
import { AvailabilitySchema } from './availability-schema'
import type { IAvailability, IDay, IAvailabilitySchedule } from './types'

const validator = new AvailabilitySchema()

export const isAvailability = (input: any): input is IAvailability => {
  let out = false
  try {
    validator.validateObject(input)
    out = true
  } catch (e) {
    // ...
  }

  return out
}

export const assertsAvailability = (
  input: unknown,
): asserts input is IAvailability => {
  validator.validateObject(input)
}

export class Availability implements IAvailability {
  tz: string
  days: IDay[] = []
  constructor(tz: string, public label?: string) {
    const maybe = DateTime.local().setZone(tz)
    assert.equal(maybe.isValid, true, maybe.invalidReason as string)
    this.tz = maybe.zoneName
  }

  static fromObject(data: any): IAvailability[] {
    const out: IAvailability[] = []
    // @ts-ignore
    assertsAvailability(data)
    const { availabilities = [] } = data as { availabilities: IAvailability[] }
    for (const a of availabilities) {
      const availability = new Availability(data.tz, data.label)
      const { days = [] } = a
      for (const d of days) {
        const day = new Day(d.label)
        const { shifts = [] } = d
        for (const s of shifts) {
          const shift = new Shift(s.begin, s.end, s.label)
          day.shifts.push(shift)
        }
        availability.days.push(day)
      }
      out.push(availability)
    }
    return out
  }
}

export class AvailabilitySchedule extends Availability
  implements IAvailabilitySchedule {
  days: IDay[] = []
  constructor(public beginDate: string, tz: string, public label?: string) {
    super(tz, label)
    // https://moment.github.io/luxon/docs/manual/parsing.html#table-of-tokens
    const maybe = DateTime.fromFormat(beginDate, 'yyyy-MM-dd', { zone: tz })
    assert.equal(maybe.isValid, true, maybe.invalidReason as string)
  }
}
