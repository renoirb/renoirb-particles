/**
 * A time slot
 */
export interface IShift {
  label?: string
  begin: string
  end: string
}

/**
 * Time Slots we can make appointments for that day
 */
export interface IDay {
  label?: string
  shifts: IShift[]
}

/**
 * Aggregate root, a description of shifts time-slots per day.
 */
export interface IAvailability {
  /**
   * The TimeZone in which the schedule is assuming hours are for
   */
  tz: string
  label?: string
  days: IDay[]
}

/**
 * When an availability is in effect.
 * The `beginDate` determines the relative date based on the days index.
 */
export interface IAvailabilitySchedule extends IAvailability {
  beginDate: string
}
