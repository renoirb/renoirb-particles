[repo-url]: https://gitlab.com/renoirb/renoirb-particles/-/tree/v1.x/libraries/availability-appointment-model
[npmjs-badge]: https://img.shields.io/npm/v/%40renoirb%2Favailability-appointment-model?style=flat-square&logo=appveyor&label=npm&logo=npm 'NPMjs.com Package Badge'
[npmjs-url]: https://www.npmjs.com/package/%40renoirb%2Favailability-appointment-model
[size-badge]: https://img.shields.io/bundlephobia/min/%40renoirb%2Favailability-appointment-model?style=flat-square
[deps-status]: https://img.shields.io/librariesio/release/npm/%40renoirb%2Favailability-appointment-model?style=flat-square&logo=appveyor&logo=dependabot

# [**@renoirb/availability-appointment-model**][repo-url]

| Version                          | Size                           | Dependencies                                                      |
| -------------------------------- | ------------------------------ | ----------------------------------------------------------------- |
| [![npm][npmjs-badge]][npmjs-url] | ![npm bundle size][size-badge] | ![Libraries.io dependency status for latest release][deps-status] |

Assuming we want to expose time slots to make appointments, we want a way to describe an **availability** schedule.
An **availability** schedule (e.g. a professional is available for appointments on mondays in the afternoon, etc.) is spanning a number of days (e.g. a week, or any number of days), and can be repeated.
Each **day** of that **availability** schedule (i.e. a collection of days) can have a **shift** (i.e. between 09:00 and 17:00).

With this model, we can have an availability schedule described as like this, say:

```yaml
availabilities:
  - tz: America/Montreal
    label: Summer schedule
    days:
      - label: Monday
        shifts:
          - begin: '13:00'
            end: '19:00'
            label: 'Lazy monday'
      - label: Tuesday
        shifts:
          - begin: '09:00'
            end: '12:00'
          - begin: '13:00'
            end: '18:00'
      - label: Wednesday
        shifts:
          - begin: '09:00'
            end: '12:00'
          - begin: '13:00'
            end: '18:00'
      - label: Thursday
        shifts:
          - begin: '09:00'
            end: '12:00'
          - begin: '13:00'
            end: '18:00'
      - shifts:
          - begin: '09:00'
            end: '12:00'
```

This example is showing a typical white collar 09:00 to 17:00 work week, but this data model should be flexible enough to support more complex use-cases.

```ts
/**
 * A time slot
 */
export interface IShift {
  label?: string
  shifts: IDay[]
}

/**
 * Time Slots we can make appointments for that day
 */
export interface IDay {
  label?: string
  shifts: IDay[]
}

/**
 * Aggregate root, a description of shifts time-slots per day.
 */
export interface IAvailability {
  /**
   * The TimeZone in which the schedule is assuming hours are for
   */
  tz: string
  label?: string
  days: IDay[]
}

/**
 * When an availability is in effect.
 * The `beginDate` determines the relative date based on the days index.
 */
export interface IAvailabilitySchedule extends IAvailability {
  beginDate: Date
}
```

Each schedule are "templates", in a system, we would have to have such a schedule to have a start date, where we'd be able to query if today's date, according to the start date, if we can make an appointment.

The difference between a template and an effective schedule would be with the presence of a `beginDate`.

There are other aspects that can be expanded, but we will continue this another time.
