# Change Log - @renoirb/availability-appointment-model

This log was last generated on Tue, 24 Nov 2020 09:11:31 GMT and should not be manually modified.

## 0.2.1
Tue, 24 Nov 2020 09:11:31 GMT

### Patches

- Node.js exports must start by dot slash

## 0.2.0
Tue, 24 Nov 2020 07:40:40 GMT

### Minor changes

- New package

