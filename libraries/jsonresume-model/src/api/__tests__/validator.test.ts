/* eslint-env jest */

import { Validator } from '..'
import { SchemaValidator } from '@renoirb/jsonschema-aware-loader'

const sampleResumeString =
  '{"basics":{"name":"John Doe","label":"Web Developer"}}'

describe('Validator', () => {
  let sv: SchemaValidator

  beforeEach(() => {
    sv = new Validator()
  })

  it('Extends SchemaValidator', () => {
    expect(sv).toMatchSnapshot()
    const subject = sv.loadFromString(sampleResumeString)
    expect(subject).toMatchSnapshot()
  })

  it('Throws', () => {
    expect(() => {
      sv.loadFromString('{"╰(✿˙ᗜ˙)੭━☆ﾟ.*･｡ﾟ":"Hi, my name is Ralph"}')
    }).toThrowError()
  })
})
