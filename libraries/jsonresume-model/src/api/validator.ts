import { SchemaValidator } from '@renoirb/jsonschema-aware-loader'

export class Validator extends SchemaValidator {
  static SCHEMA: string = 'schemas/jsonresume-1.0.0.json'
  constructor() {
    super(__dirname, Validator.SCHEMA)
  }
}
