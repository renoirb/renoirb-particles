import { JsonObject, JsonSchema } from '@rushstack/node-core-library'
import * as jju from 'jju'

/**
 * Whenever SchemaValidator validated the object, it creates an instance of this.
 *
 * We are wrapping into this object the received string.
 * That is because it is best to always create new objects.
 *
 * @author Renoir Boulanger <contribs@renoirboulanger.com>
 */
export interface JsonObjectSourceInterface {
  readonly source: Buffer
  /**
   * Z-Schema Short Name used to validate that string
   */
  readonly schemaShortName: string
  /**
   * Hide internal source Buffer so we can transfer directly.
   * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/toString
   */
  toString(): string
  /**
   * Support creating an object directly.
   *
   * Not really useful for JSON.stringify(), but can still be used to abstract out making JSON string into object.
   *
   * ```js
   * const o = new JsonObjectSource('{name: "John Doe"}')
   * o.toJSON()
   * ```
   *
   * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/JSON/stringify#toJSON_behavior
   */
  toJSON(): JsonObject
}

export class JsonObjectSource implements JsonObjectSourceInterface {
  readonly source: Buffer
  readonly schemaShortName: string
  constructor(readonly jsonString: string = '', validator: JsonSchema) {
    this.source = Buffer.from(jsonString)
    this.schemaShortName = validator.shortName
  }

  public toString(): string {
    return this.source.toString()
  }

  public toJSON(): JsonObject {
    let deserialized: JsonObject
    try {
      deserialized = jju.parse(this.source.toString())
    } catch (error) {
      throw new Error(`Error loading JSON source: ${error.message}`)
    }

    return deserialized
  }
}
