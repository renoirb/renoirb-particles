import { SchemaValidator } from '../../../../..'

export class Validator extends SchemaValidator {
  // String below should match as if it was an import stament
  // Just so we aren't making assumptions of where we want to store
  // the schemas in relation to that validator class.
  static SCHEMA: string = 'schemas/some-schema-identifier.json'
  constructor() {
    super(__dirname, Validator.SCHEMA)
  }
}
