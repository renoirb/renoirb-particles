// Instead of having a Node.js loader, we'll just keep in test-data as if it was an exported package
// But it isn't. This is just for testing and showing how to use.
import { Validator } from './test-data/example-package-alpha/src'

/**
 * See @rushstack/node-core-library test cases if you want to continue working here
 * https://github.com/microsoft/rushstack/blob/76af57a/libraries/node-core-library/src/test/JsonSchema.test.ts
 */

const sampleResumeString = '{"basics":{"name":"John Doe","age":33}}'

describe('Validator', () => {
  let sv: Validator

  beforeEach(() => {
    sv = new Validator()
  })

  it('Extends SchemaValidator', () => {
    const subject = sv.loadFromString(sampleResumeString)
    expect(sv).toHaveProperty('schemaShortName', 'some-schema-identifier.json')
    expect(subject.source).toBeInstanceOf(Buffer)
    expect(sv).toHaveProperty('isUrl', false)
    expect(subject).toMatchSnapshot()
    expect(subject).toHaveProperty(
      'schemaShortName',
      'some-schema-identifier.json',
    )
    expect(String(subject)).toBe(sampleResumeString)
    expect(JSON.stringify(subject)).toBe(sampleResumeString)
    expect(subject.toJSON()).toMatchSnapshot()
    expect(subject.toJSON()).toMatchObject(JSON.parse(sampleResumeString))
  })

  it('Throws', () => {
    expect(() => {
      sv.loadFromString('{"╰(✿˙ᗜ˙)੭━☆ﾟ.*･｡ﾟ":"Hi, my name is Ralph"}')
    }).toThrowError()
  })
})
