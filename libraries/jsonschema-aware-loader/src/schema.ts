import * as path from 'path'
import * as jju from 'jju'
import {
  JsonFile,
  JsonSchema,
  IJsonSchemaFromFileOptions,
  IJsonSchemaValidateOptions,
} from '@rushstack/node-core-library'
import { JsonObjectSource, JsonObjectSourceInterface } from './source'

/**
 * Represents a JSON-serializable object whose type has not been determined yet.
 *
 * @remarks
 *
 * This type is similar to `any`, except that it communicates that the object is serializable JSON.
 *
 * @public
 *
 * ---
 * Should be the same as @rushstack/node-core-library
 */
export declare type JsonObject = any

export interface SchemaValidatorInterface {
  /**
   * Full path where project files are stored.
   */
  readonly projectFolderBasePath: string
  /**
   * Full path to applicable JSONSchema
   */
  readonly schemaBaseUrl: string
  /**
   * Z-Schema Short Name
   */
  readonly schemaShortName: string
}

/**
 * This represent a JSONSchema Validator for use in other projects.
 *
 * @author Renoir Boulanger <contribs@renoirboulanger.com>
 */
export class SchemaValidator {
  readonly isUrl: boolean
  readonly projectFolderBasePath: string
  readonly schemaBaseUrl: string
  readonly schemaShortName: string
  private _jsonSchemaValidator: JsonSchema

  /**
   * @param relativePath {string} FileSystem path from where to attempt resolving JSONSchema schema definition files
   */
  resolvePath(relativePath = ''): string {
    const projectFolderBasePath = this.projectFolderBasePath
    const joinedPath = path.join(projectFolderBasePath, relativePath)
    const resolved: string = path.resolve(joinedPath)

    return path.normalize(resolved)
  }

  constructor(
    baseUrl = '',
    relativePath = '',
    options?: IJsonSchemaFromFileOptions,
  ) {
    const isUrl = /^http/i.test(baseUrl)
    this.isUrl = isUrl
    const projectFolderBasePath = isUrl
      ? baseUrl
      : path.normalize(path.dirname(baseUrl))
    this.projectFolderBasePath = projectFolderBasePath

    const schemaBaseUrl = isUrl
      ? baseUrl + relativePath
      : this.resolvePath(relativePath)
    this.schemaBaseUrl = schemaBaseUrl

    const _jsonSchemaValidator: JsonSchema = JsonSchema.fromFile(
      schemaBaseUrl,
      options,
    )
    this._jsonSchemaValidator = _jsonSchemaValidator
    this.schemaShortName = _jsonSchemaValidator.shortName
  }

  /**
   * {@link JsonSchema#validateObject}
   */
  public validateObject(
    jsonObject: JsonObject,
    filenameForErrors: string = '',
    options?: IJsonSchemaValidateOptions,
  ): void {
    this._jsonSchemaValidator.validateObject(
      jsonObject,
      filenameForErrors,
      options,
    )
  }

  /**
   * Pretty much the same as @rushstack/node-core-library, but we already have the JSON source string.
   */
  public loadFromString(jsonString: string): JsonObjectSourceInterface {
    let deserialized: JsonObject

    // The following is what happens in JsonFile#loadAndValidate, once the file is loaded.
    // We're just adding the feature of loading from a JSON string too.
    try {
      deserialized = jju.parse(jsonString)
    } catch (error) {
      throw new Error(`Error loading JSON source: ${error.message}`)
    }

    // Not try-catching this, it throws an exception, we'll handle from outsideà
    this.validateObject(
      deserialized,
      '' /** Or an empty string if not applicable */,
    )

    const out = new JsonObjectSource(jsonString, this._jsonSchemaValidator)

    return out
  }

  /**
   * Load from file system JSON file.
   *
   * Thank you [rushstack rush-lib][1] for providing good JSONSchema validation pattern.
   *
   * [1]: https://github.com/microsoft/rushstack/blob/master/apps/rush-lib/src/api/RushConfiguration.ts#L546
   * @param jsonFilePath {string} File to validate before deserializing
   */
  public loadFromFile(relativeJsonFilePath: string): JsonObjectSourceInterface {
    if (this.isUrl) {
      const message = `We can’t load "${relativeJsonFilePath}" from filesystem, the baseUrl is not pointing to a filesystem.`
      throw new Error(message)
    }

    const resolvedFilePath = this.resolvePath(relativeJsonFilePath)
    const deserialized = JsonFile.loadAndValidate(
      resolvedFilePath,
      this._jsonSchemaValidator,
    )

    const out = new JsonObjectSource(
      JsonFile.stringify(deserialized),
      this._jsonSchemaValidator,
    )

    return out
  }

  public toJSON(): JsonObject {
    const out = JsonFile.load(this.schemaBaseUrl)
    return out
  }
}
